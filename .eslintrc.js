module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "CHECK": true,
        "NODE": true,
        "TIM": true,
        "def": true,
        "pass": true,
        "root": true,
        "Self": true,
        "tim": true,
        "_require": true
    },
    "parserOptions": {
        "ecmaVersion": 2020
    },
    "rules": {
        "indent": [
            "off",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-console": "off",
        "no-trailing-spaces": "error",
        "no-unused-vars": [
            "error",
            {
                "args" : "none",
                "varsIgnorePattern" : "^_require$|^TIM$|^pass$|^_$",
            },
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
