/*
| A group of proteans.
*/
'use strict';

def.group = [ 'protean' ];

/*
| Creates the group of timtypes.
*/
def.static.TimTypes =
	function( ...args )
{
	const g = { };
	for( let a of args ) g[ a.$type ] = a;
	return Self.create( 'group:init', g );
};
