/*
| Runs the timcode generator for the tim package itself.
*/
'use strict';

Error.stackTraceLimit = Infinity;
global.CHECK = true;
global.NODE = true;

require( '../root' );
const pkg = tim.register( 'repl', module, 'src/', 'Repl/Start.js' );
pkg.require( 'Repl/Root' ).init( );
