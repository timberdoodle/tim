/*
| Runs the node Read-Eval-Print-Loop for debugging tim.
*/
'use strict';

def.abstract = true;

global.util = require( 'util' );
const repl = require( 'repl' );

const Path = tim.require( 'Path/Self' );
const Formatter = tim.require( 'Format/Self' );
const Parser = tim.require( 'Parser/Self' );
const Shorthand = tim.require( 'Ast/Shorthand' );

/*
| Comfort function, inspects with infinite depth as default.
*/
global.inspect = ( obj ) => global.util.inspect( obj, { depth: null } );

def.static.init =
	function( )
{
	global.Formatter = Formatter;
	global.Parser = Parser;
	global.Path = Path;
	global.Shorthand = Shorthand;
	global.parse = global.$ = Parser.statement;
	global.$block = Shorthand.$block;
	global.$for = Shorthand.$for;
	global.format = global.Formatter.format;
	global.$$ =
		function( arg )
	{
		console.log( Formatter.format( arg ) );
	};
	const rs = repl.start( 'repl> ' );
	rs.setupHistory( '.repl-history', ( ) => { } );
};
