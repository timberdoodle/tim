/*
| A trace.
*/
'use strict';

def.attributes =
{
	_list: { type: [ 'Trace/Step/List' ] },
};

def.json = 'trace';
def.fromJsonArgs = [ 'json', 'plan' ];

const PlanPiece = tim.require( 'Plan/Piece/Self' );
const StepAt = tim.require( 'Trace/Step/At' );
const StepKey = tim.require( 'Trace/Step/Key' );
const StepList = tim.require( 'Trace/Step/List' );
const StepPlain = tim.require( 'Trace/Step/Plain' );
const StepRootAt = tim.require( 'Trace/Step/Root/At' );
const StepRootPlain = tim.require( 'Trace/Step/Root/Plain' );

/*
| Adds a step.
*/
def.proto.add =
	function( name, arg )
{
	const list = this._list;
	const last = list.last;
	const lplan = last.plan;

	let sub = lplan.subs.get( name );
/**/if( CHECK && !sub ) throw new Error( );
	if( typeof( sub  ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( );

	let astep;
	if( sub.at )
	{
		astep = StepAt.create( 'at', arg, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
		astep = StepKey.create( 'key', arg, 'name', name, 'plan', sub );
	}
	else
	{
		astep = StepPlain.create( 'name', name, 'plan', sub );
		if( arg !== undefined ) throw new Error( );
	}
	const t = this.create( '_list', list.append( astep ) );
	tim.aheadValue( t, 'back', this );
	return t;
};

/*
| Adds a step object.
*/
def.proto.addStep =
	function( step )
{
	const last = this.last;
	const lplan = last.plan;
	const name = step.name;

	let sub = lplan.subs.get( name );
/**/if( CHECK && !sub ) throw new Error( );
	if( typeof( sub  ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( );

	let astep;
	let t;
	if( sub.at )
	{
/**/	if( step.timtype !== StepAt ) throw new Error( );
		astep = StepAt.create( 'at', step.at, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
/**/	if( step.timtype !== StepKey ) throw new Error( );
		astep = StepKey.create( 'key', step.key, 'name', name, 'plan', sub );
	}
	else
	{
/**/	if( step.timtype !== StepPlain ) throw new Error( );
		astep = StepPlain.create( 'name', name, 'plan', sub );
	}
	t = this.create( '_list', this._list.append( astep ) );
	tim.aheadValue( t, 'back', this );
	return t;
};

/*
| Turns the trace into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	const list = this._list;
	let str = 'trace: ';
	let first = true;
	for( let step of list )
	{
		if( !first ) str += '->'; else first = false;
		str += step.asStringStep;
	}
	return str;
};

/*
| Returns a trace with a step back.
*/
def.lazy.back =
	function( )
{
	const list = this._list;
	const len = list.length;
	if( len <= 1 ) throw new Error( );
	return this.create( '_list', list.remove( len - 1 ) );
};

/*
| traces from the end backwards until the step with 'name' is found.
|
| ~name: step name to trace.
*/
def.lazyFunc.backward =
	function( name )
{
	let t = this;
	for( ;; )
	{
		if( t.last.name === name ) return t;
		if( t.length === 1 ) return;
		t = t.back;
	}
};

/*
| Removes the root entry from the front of trace.
*/
def.lazy.chop =
	function( )
{
	let t = Self.root( this.get( 1 ).plan );
	for( let a = 2, alen = this.length; a < alen; a++ )
	{
		const step = this.get( a );
		t = t.add( step.name, step.key || step.at );
	}
	return t;
};

/*
| Returns the length this trace has in common with another
| as seen from the beginning.
*/
def.proto.commonLength =
	function( trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( trace.timtype !== Self ) throw new Error( );
/**/}
	const tlist = this._list;
	const olist = trace._list;
	const tlen = this.length;
	const olen = trace.length;
	const clen = Math.min( tlen, olen );
	let a;
	for( a = 0; a < clen; a++ )
	{
		if( tlist.get( a ) !== olist.get( a ) ) break;
	}
	return a;
};

/*
| First step.
*/
def.lazy.first =
	function( )
{
	return this._list.first;
};

/*
| traces from the start forwards until the step with 'name' is found.
|
| ~name: step name to trace.
*/
def.lazyFunc.forward =
	function( name )
{
	const list = this._list;
	for( let a = 0, len = list.length; a < len; a++ )
	{
		if( list.get( a ).name === name ) return this.head( a  + 1 );
	}
};

/*
| Custom json creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( json.$type !== 'trace' ) throw new Error( );
	const jtrace = json.trace;
	if( !Array.isArray( jtrace ) ) throw new Error( );

	let rStep = jtrace[ 0 ];
	let trace;
	let p;

	if( rStep[ 0 ] === '#' )
	{
		p = 2;
		trace = Self.rootAt( plan, jtrace[ 1 ] );
	}
	else
	{
		p = 1;
		trace = Self.root( plan );
	}

	for( let len = jtrace.length; p < len; p++ )
	{
		const name = jtrace[ p ];
		const n = name.charAt( 0 );

		if( n !== '>' && n !== '#' ) trace = trace.add( name );
		else trace = trace.add( name.slice( 1 ), jtrace[ ++p ] );
	}
	return trace;
};

/*
| Grafts a new leaf on a tree.
|
| In case of a root trace returns the leaf.
*/
def.proto.graft =
	function( tree, val )
{
	if( this.length === 1 ) return this.first.graft( tree, val );
	const tback = this.back;
	let sub = tback.pick( tree );
	sub = this.last.graft( sub, val );
	return tback.graft( tree, sub );
};

/*
| Gets a step.
*/
def.proto.get = function( a ) { return this._list.get( a ); };

/*
| Returns true if this trace has 'trace' as parent.
*/
def.proto.hasTrace =
	function( trace )
{
	let t = this;
	for( ;; )
	{
		if( t === trace ) return true;
		if( t.length === 1 ) return false;
		t = t.back;
	}
};

/*
| Returns the front part of the trace
| up to len;
*/
def.lazyFunc.head =
	function( len )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( len ) !== 'number' ) throw new Error( );
/**/	if( len > this.length ) throw new Error( );
/**/}

	let t = this;
	for( let a = this.length; a > len ; a-- ) t = t.back;
	return t;
};

/*
| Custom jsonfy converter.
*/
def.lazyFunc.jsonfy =
	function( space )
{
	let r = '{"$type":"trace","trace":[';
	const list = this._list;
	let first = true;
	for( let a = 0, alen = list.length; a < alen; a++ )
	{
		if( !first ) r+= ','; else first = false;
		r += list.get( a ).jsonfyStep;
	}
	return r + ']}';
};

/*
| Last step.
*/
def.lazy.last =
	function( )
{
	return this._list.last;
};

/*
| Length of the trace.
*/
def.lazy.length =
	function( )
{
	return this._list.length;
};

/*
| Picks the traced leaf.
*/
def.proto.pick =
	function( tree )
{
	for( let step of this._list ) tree = step.pick( tree );
	return tree;
};

/*
| Picks the traced leaf.
*/
def.proto.pickSlice =
	function( tree, start, end )
{
/**/if( CHECK && ( arguments.length < 2 || arguments.length > 3 ) ) throw new Error( );

	const list = this._list;
	const len = list.length;
	if( end === undefined ) end = len;

/**/if( CHECK )
/**/{
/**/	if( start >= end ) throw new Error( );
/**/	if( start > len ) throw new Error( );
/**/	if( end > len ) throw new Error( );
/**/}

	for( let a = start; a < end; a++ )
	{
		const step = list.get( a );
		tree = step.pick( tree );
	}
	return tree;
};

/*
| Starts a new trace root.
|
| ~plan: plan of the trace.
*/
def.staticLazyFunc.root =
	function( plan )
{
/**/if( CHECK && plan.timtype !== PlanPiece ) throw new Error( );

	const name = plan.name;
	return(
		Self.create(
			'_list', StepList.Elements( StepRootPlain.create( 'plan', plan, 'name', name ) )
		)
	);
};

/*
| Starts a new trace root with a root/at step.
|
| ~plan: plan of the trace.
| ~at: the at to start with.
*/
def.static.rootAt =
	function( plan, at )
{
/**/if( CHECK )
/**/{
/**/	if( plan.timtype !== PlanPiece ) throw new Error( );
/**/	if( typeof( at ) !== 'number' ) throw new Error( );
/**/}

	const name = plan.name;
	return(
		Self.create(
			'_list', StepList.Elements( StepRootAt.create( 'at', at, 'plan', plan, 'name', name ) )
		)
	);
};

/*
| Replaces a step.
*/
def.proto.set =
	function( a, name, arg )
{
	const list = this._list;
	let astep = list.get( a );

/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 && arguments.length !== 3 ) throw new Error( );
/**/	if( astep.name !== name ) throw new Error( );
/**/}

	const sub = astep.plan;
	if( sub.at )
	{
		astep = StepAt.create( 'at', arg, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
		astep = StepKey.create( 'key', arg, 'name', name, 'plan', sub );
	}
	else
	{
		astep = StepPlain.create( 'name', name, 'plan', sub );
		if( arg !== undefined ) throw new Error( );
	}
	return this.create( '_list', list.set( a, astep ) );
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK && this._list.length < 1 ) throw new Error( );
};
