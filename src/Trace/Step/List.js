/*
| A list of steps.
*/
'use strict';

def.list =
[
	'Trace/Step/At',
	'Trace/Step/Key',
	'Trace/Step/Plain',
	'Trace/Step/Root/At',
	'Trace/Step/Root/Plain',
];
