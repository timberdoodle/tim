/*
| A root of a trace with a list at.
*/
'use strict';

def.extend = 'Trace/Step/Base';

def.attributes =
{
	// the at value
	at: { type: 'number' },

	// name of the trace
	name: { type: 'string' },

	// plan piece
	plan: { type: 'Plan/Piece/Self' },
};

/*
| The trace step as string (for debugging).
*/
def.lazy.asStringStep =
	function( )
{
	return this.name + '(' + this.at + ')';
};

/*
| Root trace doesn't have a back.
*/
def.proto.back = undefined;

/*
| Gets step at position 'p'
*/
def.proto.get =
	function( p )
{
	if( p === 0 ) return this;
	else throw new Error( );
};

/*
| In case of a root trace returns the leaf.
*/
def.proto.graft = ( tree, leaf ) =>
	leaf;

/*
| Help for custom jsonfy converter.
*/
def.lazy.jsonfyStep =
	function( )
{
	return '"#' + this.name + '",'  + this.at;
};

/*
| Help for custom json converter.
*/
def.lazy.jsonStep =
	function( )
{
	return [ '#' + this.name, this.at ];
};

/*
| Picks the traced leaf.
| In case of a root trace returns the tree.
*/
def.proto.pick = ( tree ) =>
	tree;
