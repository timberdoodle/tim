/*
| A plain root of a trace.
*/
'use strict';

def.extend = 'Trace/Step/Base';

def.attributes =
{
	// name of the trace
	name: { type: 'string' },

	// plan piece
	plan: { type: 'Plan/Piece/Self' },
};

/*
| The trace step as string (for debugging).
*/
def.lazy.asStringStep =
	function( )
{
	return this.name;
};

/*
| Root trace doesn't have a back.
*/
def.proto.back = undefined;

/*
| No default FromJson creator.
*/
def.static.FromJson = false;

/*
| In case of a root trace returns the leaf.
*/
def.proto.graft = ( tree, leaf ) =>
	leaf;

/*
| Help for custom jsonfy converter.
*/
def.lazy.jsonfyStep =
	function( )
{
	return '"' + this.name + '"';
};

/*
| Help for custom json converter.
*/
def.lazy.jsonStep =
	function( )
{
	return [ this.name ];
};

/*
| Picks the traced leaf.
| In case of a root trace returns the tree.
*/
def.proto.pick = ( tree ) =>
	tree;
