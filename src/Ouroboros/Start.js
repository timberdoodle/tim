/*
| Runs the timcode generator for the tim package itself.
*/
'use strict';

( async ( ) => {

global.CHECK = true;
global.NODE = true;

const fs = require( 'fs/promises' );
const vm = require( 'vm' );
const argv = process.argv;
if( argv.length !== 3 )
{
	console.error( 'Usage: ' + argv[ 0 ] + ' ' + argv[ 1 ] + ' [tim.js root dir]' );
	process.exit( -1 );
}

const targetDir = argv[ 2 ];
if( targetDir[ targetDir.length - 1 ] !== '/' )
{
	console.error( argv[ 2 ] + ' does not end with a \'/\'' );
	process.exit( -1 );
}

let myDir;
{
	const ending = 'src/Ouroboros/Start.js';
	const filename = module.filename;
	if( !filename.endsWith( ending ) ) throw new Error( );
	myDir = filename.substr( 0, filename.length - ending.length );
}

if( targetDir === myDir )
{
	console.log( 'local ouroboros build' );
}
else
{
	console.log( 'peer ouroboros build' );
	global.OUROBOROS = true;
}
require( '../root' );
await tim.init( );
require( '../Core/Scaffold' );
const _core = tim._core;

/*
| Scaffold requiretim for tim booting.
*/
const requiretim =
	async ( required ) =>
{
	const truss = _core.addTruss( global.OUROBOROS ? 'ouroboros' : 'tim', required );
	await _core.loadAsync( truss );
	await _core.coupleAsync( truss );
	await tim._prepareAsync( truss );
	return truss.runtime;
};

const Path = await requiretim( 'Path/Self.js' );
const Formatter = await requiretim( 'Format/Self.js' );
const Generate = await requiretim( 'Generate/Self.js' );
const Spec = await requiretim( 'Spec/Self.js' );
const TraceSpec = await requiretim( 'Spec/Trace/Root.js' );
const readOptions = Object.freeze( { encoding : 'utf8' } );

if( global.OUROBOROS )
{
	const dir = Path.FromString( targetDir );
	tim.packages.addPackage( dir, Path.FromString( './src/' ), 'tim', module, false );
}

const srcDir = targetDir + 'src/';
const listing = require( targetDir + 'src/Ouroboros/Listing' );

/*
| Prepares the listings filenames
*/
for( let filename of listing )
{
	const inFilename = srcDir + filename;
	const outFilename = targetDir + 'timcode/' + filename.replace( /\//g, '-' );

	console.log( '  reading ' + inFilename );
	const code = await fs.readFile( inFilename, readOptions ) + '';
	let input = '( function( def, require, Self, module ) {' + code + '\n} )';
	const subtim = { };
	const def =
	{
		lazy: { },
		lazyFunc: { },
		proto: { },
		static: { },
		staticLazy: { },
		staticLazyFunc: { },
	};
	subtim.copy = tim.copy;
	input = vm.runInThisContext( input, { filename: inFilename } );
	//subtim.require = subrequire.bind( undefined, inFilename );
	subtim.require = ( ) => undefined;
	tim.require = ( ) => undefined;
	input( def, ( ) => undefined, { }, undefined );
	const afile = Path.FromString( inFilename );
	const pkg = tim.packages.getPackageOfDir( afile.back );
	const trace = TraceSpec.FromAFile( afile, pkg );
	const spec = Spec.FromDef( def, afile, trace, { } );
	const ast = Generate.generate( spec );
	let output = Formatter.format( ast );
	if( !output.endsWith( '\n' ) ) output += '\n';
	console.log( '  writing ' + outFilename );
	await fs.writeFile( outFilename, output );
}

console.log( '  done' );

} )( );
