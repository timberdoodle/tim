/*
| General tim stuff used in node and browser.
*/
'use strict';

/*
| A lazy value is computed and fixated before it is needed.
|
| obj: object to ahead the lazy value for
| name: name to ahead
| value: value to ahead
*/
tim.aheadValue =
	function( obj, name, value )
{
/**/if( CHECK && value === undefined ) throw new Error( );

	// FUTURE CHECK if value is already set.

	return( obj.__lazy[ name ] = value );
};

/*
| A function taking a key and no side effects
| is computed for a value and fixated before it is needed.
|
| obj:   object to ahead for
| name:  property to ahead
| key:   function ( key ) value to ahead
| value: value ( result ) to ahead
*/
tim.aheadFunction =
	function( obj, name, key,  value )
{
/**/if( CHECK )
/**/{
/**/	if( name === undefined ) throw new Error( );
/**/	if( key === undefined ) throw new Error( );
/**/	if( value === undefined ) throw new Error( );
/**/}

	let c = obj.__lazy[ name ];
	if( !c ) c = obj.__lazy[ name ] = new Map( );
	c.set( key, value );
	return value;
};

/*
| Copies one object (not deep!)
|
| Also doesn't do hasOwnProperty checking since that one
| is only to be used on vanilla objects.
|
| ~obj: the object to copy from
*/
tim.copy =
	function( obj )
{
	const c = { };
	for( let k in obj ) c[ k ] = obj[ k ];
	return c;
};

/*
| Copies one object (not deep!).
| Also lets another object override the former.
| Used in creators of adjusting tims.
|
| Also doesn't do hasOwnProperty checking since that one
| is only to be used on vanilla objects.
*/
tim.copy2 =
	function(
		obj,  // the object to copy from
		o2  // overriding object
	)
{
	const c = { };
	for( let k in obj ) c[ k ] = obj[ k ];
	if( o2 ) for( let k in o2 ) c[ k ] = o2[ k ];
	return c;
};

/*
| Tests if the object has a lazy value set.
*/
tim.hasLazyValueSet =
	function( obj, name )
{
	return obj.__lazy[ name ] !== undefined;
};

/*
| A trap to catches all invalid tim object access.
*/
tim.trap = {};
	new Proxy(
		{ },
		{
			get : function( target, prop, receiver )
			{
				if( tim.__inspecting__ ) return target[ prop ];
				if( prop === '__lazy' ) return target.__lazy;
				if( prop === 'then' ) return target.then;
				throw new Error( prop );
			},
		}
	);
