/*
| Comfort utility for better debug inspection.
*/
'use strict';


const util = require( 'util' );
//const utilOpts = { depth: null, showProxy: true };
//const utilOpts = { depth: null };
const utilOpts = { depth: 50 };

console.inspect =
	function( ...args )
{
	tim.__inspecting__ = true;
	for( let a = 0, alen = args.length; a < alen; a++ )
	{
		args[ a ] = util.inspect( args[ a ], utilOpts );
	}
	console.log.apply( console.log, args );
	tim.__inspecting__ = false;
};
