/*
| Prepares a tim to be ran.
| Used by node and browser.
*/
'use strict';

const util = NODE && require( 'util' );

/*
| Prepares a tim to be ran.
|
| ~truss: the truss to prepare.
*/
const prepareCommon =
	function( truss )
{
	const proto = tim._proto;
	const runtime = truss.runtime;
	const def = truss.def;

	if( def.extend )
	{
		const eTruss = tim._requireTruss( truss, true, def.extend );
		// tim._readyTruss( eTruss ); (made ready by wrapper )

		// this truss might have been made ready by requiring extender
		if( truss.ready ) return;

		const edef = eTruss.def;
		const protoMask = { };
		const staticMask = { };
		for( let name in def.static ) staticMask[ name ] = true;
		for( let name in def.staticLazy ) staticMask[ name ] = true;
		for( let name in def.staticLazyFunc ) staticMask[ name ] = true;
		for( let name in def.lazy ) protoMask[ name ] = true;
		for( let name in def.lazyFunc ) protoMask[ name ] = true;
		for( let name in def.proto ) protoMask[ name ] = true;

		for( let name in edef.static )
		{
			if( !staticMask[ name ] ) def.static[ name ] = edef.static[ name ];
		}

		for( let name in edef.staticLazy )
		{
			if( !staticMask[ name ] ) def.staticLazy[ name ] = edef.staticLazy[ name ];
		}

		for( let name in edef.staticLazyFunc )
		{
			if( !staticMask[ name ] ) def.staticLazyFunc[ name ] = edef.staticLazyFunc[ name ];
		}

		for( let name in edef.lazy )
		{
			if( !protoMask[ name ] ) def.lazy[ name ] = edef.lazy[ name ];
		}

		for( let name in edef.lazyFunc )
		{
			if( !protoMask[ name ] ) def.lazyFunc[ name ] = edef.lazyFunc[ name ];
		}

		for( let name in edef.proto )
		{
			if( !protoMask[ name ] ) def.proto[ name ] = edef.proto[ name ];
		}

		if( NODE && edef.inspect && !def.inspect )
		{
			def.inspect = edef.inspect;
		}
	}

	// assigns statics
	for( let name in def.static )
	{
		runtime[ name ] = def.static[ name ];
	}

	// assigns lazy statics
	for( let name in def.staticLazy )
	{
		proto.lazyStaticValue( runtime, name, def.staticLazy[ name ] );
	}

	// assigns lazy static funcs
	for( let name in def.staticLazyFunc )
	{
		proto.lazyStaticFunc( runtime, name, def.staticLazyFunc[ name ] );
	}

	if( !def.abstract )
	{
		const prototype = runtime.prototype;

		// assigns lazy values to the prototype
		for( let name in def.lazy )
		{
			proto.lazyValue( prototype, name, def.lazy[ name ] );
		}

		// assigns lazy integer functions to the prototype
		for( let name in def.lazyFunc )
		{
			proto.lazyFunction( prototype, name, def.lazyFunc[ name ] );
		}

		// assigns functions to the prototype
		for( let name in def.proto )
		{
			prototype[ name ] = def.proto[ name ];
		}

		if( NODE && def.inspect ) prototype[ util.inspect.custom ] = def.inspect;
	}

	return runtime;
};

if( NODE )
{
	/*
	| FIXME.
	*/
	tim._prepareAsync =
		async function( truss )
	{
		if( truss.ready ) return;

		if( truss.def.extend )
		{
			const eTruss = tim._requireTruss( truss, true, truss.def.extend );
			await tim._readyTrussAsync( eTruss );
		}

		const runtime = prepareCommon( truss );

		truss.ready = true;
		for( let required in truss.requires )
		{
			const rTruss = tim._requireTruss( truss, false, required );
			await tim._readyTrussAsync( rTruss );
		}

		return runtime;
	};

	/*
	| FIXME.
	*/
	tim._prepareSync =
		function( truss )
	{
		if( truss.ready ) return;

		if( truss.def.extend )
		{
			const eTruss = tim._requireTruss( truss, true, truss.def.extend );
			tim._readyTrussSync( eTruss );
		}

		const runtime = prepareCommon( truss );

		truss.ready = true;
		for( let required in truss.requires )
		{
			const rTruss = tim._requireTruss( truss, false, required );
			tim._readyTrussSync( rTruss );
		}

		return runtime;
	};
}
else
{
	/*
	| Prepares a tim to be ran.
	| Browser mode.
	|
	| ~truss: the truss to prepare.
	*/
	tim._prepare =
		function( truss )
	{
		if( truss.def.extend )
		{
			const eTruss = tim._requireTruss( truss, true, truss.def.extend );
			tim._readyTrussSync( eTruss );
		}

		return prepareCommon( truss );
	};
}

