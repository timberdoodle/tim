/*
| Loads the def of a tim.
*/
'use strict';

const fs = require( 'fs' );
const vm = require( 'vm' );
const readOptions = Object.freeze( { encoding: 'utf8' } );

const _core = tim._core;
const nodeModule = require( 'node:module' );

/*
| The scaffold.
*/
let timOwnName;
const _scaffold = { };

/*
| Adds a package to scaffold.
|
| ~name:       name of the package.
| ~rootDir:    rootDir of the package
| ~metadata:   import.meta(ESM) or module(CJS) in the package
| ~type:       'cjs' or 'esm'
| ~timcodeGen: if true generate timcode.
*/
_core.addPackage =
	async function( name, rootDir, metadata, type, timcodeGen )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( typeof( name ) !== 'string' ) throw new Error( );
/**/	if( typeof( rootDir ) !== 'string' ) throw new Error( );
/**/	if( typeof( timcodeGen ) !== 'boolean' ) throw new Error( );
/**/}

	if( _scaffold.name ) throw new Error( );

	let exports;
	try { exports = await fs.promises.stat( rootDir + 'exports.json' ); }
	catch( e ) { /* ignore */ }

	if( exports )
	{
		exports = await fs.promises.readFile( rootDir + 'exports.json' ) + '';
		exports = JSON.parse( exports );
	}

	switch( type )
	{
		case 'cjs':
			// common js package
			_scaffold[ name ] =
				Object.freeze( {
					dir: rootDir,
					entries: { },
					exports: exports,
					module: metadata,
					require: metadata.require.bind( metadata ),
					timcodeGen: timcodeGen,
					type: 'cjs',
				} );
			break;

		case 'esm':
			// es module package
			_scaffold[ name ] =
				Object.freeze( {
					dir: rootDir,
					entries: { },
					exports: exports,
					timcodeGen: timcodeGen,
					type: 'esm',
					url: metadata.url,
				} );
			break;

		default: throw new Error( );
	}
};

/*
| Adds a truss to the scaffold.
*/
const addTruss =
_core.addTruss =
	function( pkgname, timname )
{
	let truss = _scaffold[ pkgname ].entries[ timname ];
	if( truss ) return truss;

	truss =
	_scaffold[ pkgname ].entries[ timname ] =
	{
		def:
		{
			proto: { },
			lazy: { },
			lazyFunc: { },
			static: { },
			staticLazy: { },
			staticLazyFunc: { },
		},
		requires: { },
		timname: timname,
		pkgname: pkgname,
		coupled: false,
		ready: false,
		runtime: { },
	};

	return truss;
};

/*
| Returns a truss, if existing.
*/
_core.getTruss =
	function( pkgname, timname )
{
	return _scaffold[ pkgname ].entries[ timname ];
};

/*
| Initializes the scaffold.
|
| ~ownname: name of tim ('tim' or 'ouroboros')
| ~rootDir: tim's rootDir,
| ~module: tim's module handle
*/
_core.init =
	async function( ownname, rootDir, module )
{
	let exports = await fs.promises.readFile( rootDir + 'exports.json' ) + '';
	exports = JSON.parse( exports );
	timOwnName = ownname;
	_scaffold[ ownname ] =
		Object.freeze( {
			dir: rootDir,
			entries: { },
			module: module,
			require: module.require.bind( module ),
			timcodeGen: false,
			type: 'cjs',
			exports: exports,
		} );
};

/*
| Requires other tims.
|
| ~baseTruss: truss of the requiring tim
| ~exportMapping: true if names are to be mapped via exports (not in timcode)
| ~required: string of the tim required
|
| ~return: the truss of the required tim
*/
const requireTruss =
tim._requireTruss =
	function( baseTruss, exportMapping, required )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	let timname, pkgname;
	let ios = required.indexOf( ':' );
	if( ios >= 0 )
	{
		timname = required.substr( ios + 1 );
		pkgname = required.substr( 0, ios );
	}
	else
	{
		timname = required;
		pkgname = baseTruss.pkgname;
	}

	if( pkgname !== baseTruss.pkgname && exportMapping )
	{
		const pkg = _scaffold[ pkgname ];
		if( !pkg ) throw new Error( 'package "' + pkgname + '" unknown' );

		const exports = pkg.exports;
/**/	if( CHECK && timname.endsWith( '.js' ) ) throw new Error( );

		timname = exports[ timname ];
		if( !timname ) throw new Error( '"' + required + '" not exported' );
	}

	if( !timname.endsWith( '.js' ) ) timname += '.js';

	baseTruss.requires[ pkgname + ':' + timname ] = true;
	return addTruss( pkgname, timname );
};

/*
| Forces a truss to be made ready.
|
| ~truss: the truss to make ready.
*/
tim._readyTrussAsync =
	async function( truss )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( truss.ready ) return;

	await loadAsync( truss );
	await coupleAsync( truss );
	await tim._prepareAsync( truss );
};

/*
| Forces a truss to be made ready.
|
| ~truss: the truss to make ready.
*/
tim._readyTrussSync =
	function( truss )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( truss.ready ) return;

	loadSync( truss );
	coupleSync( truss );
	tim._prepareSync( truss );
};

/*
| Scaffold special require for tim/root.js
*/
_core.requireTimBoot =
	async function( required )
{
	const truss = addTruss( timOwnName, required );
	await loadAsync( truss );
	await coupleAsync( truss );
	await tim._prepareAsync( truss );
	return truss.runtime;
};

/*
| Requires other tims.
|
| ~truss: (to be bound) truss of the requiring tim
| ~exportMapping: (to be bound) true if names are to be mapped via exports (not in timcode)
| ~required: string of the tim required
*/
const timRequire =
	function( baseTruss, exportMapping, required )
{
	return requireTruss( baseTruss, exportMapping, required ).runtime;
};

/*
| Couples generated timcode.
|
| ~truss: the truss to couple.
*/
const coupleAsync =
_core.coupleAsync =
	async function( truss )
{
	if( truss.coupled ) return;
	const pkgname = truss.pkgname;
	const timname = truss.timname;
	const filename = _scaffold[ pkgname ].dir + 'src/' + timname;
	const timcodeFilename = _scaffold[ pkgname ].dir + 'timcode/' + timname.replace( /\//g, '-' );

	let code;
	if( _scaffold[ pkgname ].timcodeGen )
	{
		// tests if the timcode is out of date or not existing
		// and creates it if so.
		const inStat = await fs.promises.stat( filename );
		let outStat;
		try { outStat = await fs.promises.stat( timcodeFilename ); }
		catch( e ) { /* ignore */ }

		if( !outStat || inStat.mtime > outStat.mtime )
		{
			await _core.driveGeneratorAsync( truss, pkgname, filename, timcodeFilename );
		}
	}

	if( truss.coupled ) return;
	if( !code )
	{
		code = await fs.promises.readFile( timcodeFilename, readOptions ) + '';
	}
	let input = '( function( Self ) {' + code + '\n} )';
	input = vm.runInThisContext( input, { filename: timcodeFilename } );
	const previousTimRequire = tim.require;
	tim.require = timRequire.bind( undefined, truss, false );
	input( truss.runtime );
	tim.require = previousTimRequire;
	truss.coupled = true;
};

/*
| Couples generated timcode.
|
| ~truss: the truss to couple.
*/
const coupleSync =
_core.coupleSync =
	function( truss )
{
	if( truss.coupled ) return;

	const pkgname = truss.pkgname;
	const timname = truss.timname;
	const filename = _scaffold[ pkgname ].dir + 'src/' + timname;
	const timcodeFilename = _scaffold[ pkgname ].dir + 'timcode/' + timname.replace( /\//g, '-' );

	let code;
	if( _scaffold[ pkgname ].timcodeGen )
	{
		// tests if the timcode is out of date or not existing
		// and creates it if so.
		const inStat = fs.statSync( filename );
		let outStat;
		try { outStat = fs.statSync( timcodeFilename ); }
		catch( e ) { /* ignore */ }

		if( !outStat || inStat.mtime > outStat.mtime )
		{
			_core.driveGeneratorSync( truss, pkgname, filename, timcodeFilename );
		}
	}

	if( truss.coupled ) return;

	if( !code )
	{
		code = fs.readFileSync( timcodeFilename, readOptions ) + '';
	}
	let input = '( function( Self ) {' + code + '\n} )';
	input = vm.runInThisContext( input, { filename: timcodeFilename } );
	const previousTimRequire = tim.require;
	tim.require = timRequire.bind( undefined, truss, false );
	input( truss.runtime );
	tim.require = previousTimRequire;
	truss.coupled = true;
};

/*
| Loads a file defining a ti2c class.
|
| ~truss: the truss to load.
*/
const loadSync =
_core.loadSync =
	function( truss )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );
	if( truss.ready ) return;

	const runtime = truss.runtime;
	const def = truss.def;
	const pkg = _scaffold[ truss.pkgname ];
	const timname = truss.timname;

	// TODO explicit src
	const filename = pkg.dir + 'src/' + timname;
	const code = fs.readFileSync( filename, readOptions ) + '';

	switch( pkg.type )
	{
		case 'cjs':
		{
			let input = '( function( def, module, require, Self ) {' + code + '\n} )';
			input = vm.runInThisContext( input, { filename: filename } );
			const previousTimRequire = tim.require;
			tim.require = timRequire.bind( undefined, truss, true );
			input( def, pkg.module, pkg.require, runtime );
			tim.require = previousTimRequire;
			return;
		}

		case 'esm': throw new Error( );
		default: throw new Error( );
	}
};

// XXX
const esmImports = new Map();

/*
| FIXME.
*/
const esmLinker =
	async function( pkg, baseTruss, specifier, referencingModule )
{
	if( specifier === '((ti2c::loop))' )
	{
		// provides the ti2c loop back interface
		const exportNames = [ 'def', 'Self', 'tim' ];

		const loop =
			new vm.SyntheticModule(
				exportNames,
				( ) =>
				{
					loop.setExport( 'def', baseTruss.def );
					loop.setExport( 'tim', tim );
					loop.setExport( 'Self', baseTruss.runtime );
				},
				{
					identifier: '((ti2c::loop))',
					context: referencingModule.context
				}
			);

		return loop;
	}

	if( specifier.startsWith( '{' ) )
	{
		// imports a tim
		if( !specifier.endsWith( '}' ) ) throw new Error( );

		const required = specifier.substr( 1, specifier.length - 2 );
		const rTruss = requireTruss( baseTruss, true, required );
		if( esmImports.has( rTruss ) )
		{
			return esmImports.get( rTruss );
		}

		const imported =
			new vm.SyntheticModule(
				[ 'default', required ],
				( ) =>
				{
					imported.setExport( 'default', rTruss.runtime );
				},
				{
					identifier: required,
					context: referencingModule.context
				}
			);

		esmImports.set( rTruss, imported );
		return imported;
	}

	if( esmImports.has( specifier ) )
	{
		return esmImports.get( specifier );
	}

	const require = nodeModule.createRequire( pkg.url );
	const mod = await import( require.resolve( specifier ) );

	const exportNames = Object.keys( mod );
	const imported =
		new vm.SyntheticModule(
			exportNames,
			( ) =>
			{
				exportNames.forEach( key => imported.setExport( key, mod[ key ] ) );
			},
			{
				identifier: specifier,
				context: referencingModule.context
			}
		);

	esmImports.set( specifier, imported );
	return imported;
};

const esmContext =
	vm.createContext( {
		console: console,
		CHECK: CHECK,
		NODE: true,
	} );

/*
| Loads a file defining a ti2c class.
|
| ~truss: the truss to load.
*/
const loadAsync =
_core.loadAsync =
	async function( truss )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );
	if( truss.ready ) return;

	const pkg = _scaffold[ truss.pkgname ];
	const timname = truss.timname;

	// TODO explicit src
	const filename = pkg.dir + 'src/' + timname;
	let code = await fs.promises.readFile( filename, readOptions ) + '';

	switch( pkg.type )
	{
		case 'esm':
		{
			const input = 'import { def, Self, tim } from "((ti2c::loop))";' + code;
			const stm =
				new vm.SourceTextModule(
					input,
					{
						identifier: filename,
						context: esmContext,
					}
				);
			await stm.link( esmLinker.bind( undefined, pkg, truss ) );
			await stm.evaluate( );
			return;
		}

		case 'cjs':
		{
			let input = '( function( def, module, require, Self ) {' + code + '\n} )';
			input = vm.runInThisContext( input, { filename: filename } );
			const previousTimRequire = tim.require;
			tim.require = timRequire.bind( undefined, truss, true );
			input( truss.def, pkg.module, pkg.require, truss.runtime );
			tim.require = previousTimRequire;
			return;
		}

		default: throw new Error( );
	}
};

