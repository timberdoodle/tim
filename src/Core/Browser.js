/*
| Initalizes the tim environment in the browser.
*/
'use strict';

var tim = { };
// global pass flag for creators
var pass = tim.pass = Object.freeze( { } );
// created by getBrowserInitCode( )
var _packages;

/*
| Initializes the tim package register.
*/
{
	for( let pname in _packages )
	{
		const pkg = _packages[ pname ];
		const entries = pkg.entries;
		for( let name in entries ) entries[ name ].pkg = pkg;
	}
}

/*
| Require function for a module to bind to it's leaf.
|
| ~baseTruss: truss of the requiring tim
| ~exportMapping: true if names are to be mapped via exports (not in timcode)
| ~required: string of the required tim.
*/
tim._requireTruss =
	function( baseTruss, exportMapping, required )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	let entries;

	// relative paths no longer supported.
	const r0 = required[ 0 ];
	if( r0 === '.' || r0 === '~' || r0 === ':' ) throw new Error( );

	const ios = required.indexOf( ':' );
	if( required.indexOf( ':' ) >= 0 )
	{
		// it is an import
		const pkgname = required.substr( 0, ios );
		required = required.substr( ios + 1 );
		const pkg = _packages[ pkgname ];
		if( exportMapping ) required = pkg.exports[ required ];
		if( !required ) throw new Error( );
		entries = pkg.entries;
	}
	else
	{
		entries = baseTruss.pkg.entries;
	}

	if( !required.endsWith( '.js' ) ) required = required + '.js';

	return entries[ required ];
};

/*
| Forces a truss to be made ready.
|
| ~truss: the truss to make ready.
*/
tim._readyTrussAsync =
tim._readyTrussSync =
	function( truss )
{
	// nothing in case of browser.
};

/*
| Require function for a module to bind to it's leaf.
|
| ~baseTruss: truss the require comes from
| ~exportMapping: true if names are to be mapped via exports (not in timcode)
| ~required: string of the required tim.
*/
const _require =
	function( baseTruss, exportMapping, required )
{
	return tim._requireTruss( baseTruss, exportMapping, required ).runtime;
};


/*
| Loads a tim in browser mode.
|
| ~pkgname: name of the package of the tim
| ~timname: name of the tim
| ~isTimcode: true if in timcode
| ~callee: the actual wrapped definition code
*/
tim._load =
	function( pkgname, timname, isTimcode, callee )
{
	let truss = _packages[ pkgname ].entries[ timname ];

	const def = truss.def =
	{
		proto : { },
		lazy : { },
		lazyFunc : { },
		static : { },
		staticLazy : { },
		staticLazyFunc : { },
	};

	tim.require = _require.bind( undefined, truss, !isTimcode );
	callee( def, truss.runtime );
	tim.require = undefined;
	tim._prepare( truss );
};

/*
| Loads a tim module in browser mode.
|
| ~pkgname: name of the package of the tim
| ~timname: name of the tim
*/
tim._loadESM1 =
	function( pkgname, timname )
{
	let truss = _packages[ pkgname ].entries[ timname ];
	truss.def =
	{
		proto : { },
		lazy : { },
		lazyFunc : { },
		static : { },
		staticLazy : { },
		staticLazyFunc : { },
	};
	return truss;
};

/*
| Loads a tim module in browser mode.
|
| ~truss: the truss
*/
tim._loadESM2 =
	function( truss )
{
	tim._prepare( truss );
};


