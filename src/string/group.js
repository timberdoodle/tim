/*
| A group of strings.
*/
'use strict';

def.group = [ 'string' ];
def.json = 'StringGroup';
