/*
| A list of strings.
*/
'use strict';

def.list = [ 'string' ];
def.json = 'StringList';

/*
| The StringList joined into one string.
|
| ~sep: seperator to join.
*/
def.lazyFunc.join = function( sep ) { return this._list.join( sep ); };
