/*
| Bootstraps tim in node environment.
*/
'use strict';

// the tim module.
global.tim = module.exports;

// global pass flag for creators
global.pass = Object.freeze( { } );

{
	// silences the experimental vm warnings
	const originalEmit = process.emit;
	process.emit =
		( name, data, ...args ) =>
	{
		if(
			name === 'warning'
			&& typeof data === 'object'
			&& data.name === 'ExperimentalWarning'
		)
		{
			return false;
		}
		else
		{
			return originalEmit.apply( process, arguments );
		}
	};
}

/*
| Initalizes tim.
*/
tim.init =
	async( ) => {
const _core = tim._core = { };
require( './Core/Inspect' );
require( './Core/Common' );
tim.proto = require( './Core/Proto' );
require( './Core/Scaffold' );
require( './Core/Prepare' );
require( './Hash/Timur32' );

const ownname = global.OUROBOROS ? 'ouroboros': 'tim';

// adds tim itself to scaffold
let timrootdir;
{
	const ending = 'src/root.js';
	const filename = module.filename;
	if( !filename.endsWith( ending ) ) throw new Error( );
	timrootdir = filename.substr( 0, filename.length - ending.length );
	await _core.init( ownname, timrootdir, module );
}

/*
| Scaffold requiretim for tim booting.
*/
const requiretim = _core.requireTimBoot;

/*
| Bootstrapping.
*/
let Path;
{
	Path = await requiretim( 'Path/Self.js' );
	const Packages = await requiretim( 'Spec/Packages.js' );

	tim.packages = Packages.Empty;
	await tim.packages.addPackage(
		Path.FromString( timrootdir ),
		Path.FromString( './src/' ),
		global.OUROBOROS ? 'ouroboros': 'tim',
		undefined, // module
		false, // timcodeGen
	);

	// loads the generator driver
	const Drive = await requiretim( 'Generate/Drive.js' );
	_core.driveGeneratorSync = Drive.driveSync;
	_core.driveGeneratorAsync = Drive.driveAsync;
}

/*
| Adds the package to a timberman.
*/
tim.addTimbermanCatalog =
	function( timberman )
{
	return(
		timberman.updateResource(
			timberman.get( 'tim-packages-init.js' )
			.create( 'data', tim.packages.getBrowserInitCode( ) )
		)
	);
};

/*
| Adds the tim resources to a timberman.
|
| ~timberman: the timberman to add the resources to
| ~list: the list(s) the tag the resources with
*/
tim.addTimbermanResources =
	async function( timberman, list )
{
	const base = Path.FromString( module.filename ).parent;
	return await timberman.addResource(
		base,
		{ name: 'tim-packages-init.js',                               list: list, },
		{ name: 'tim-Core-Browser.js', file: './Core/Browser.js', list: list, },
		{ name: 'tim-Core-Prepare.js', file: './Core/Prepare.js', list: list, },
		{ name: 'tim-Hash-Timur32.js', file: './Hash/Timur32.js', list: list, },
		{ name: 'tim-Core-Proto.js',   file: './Core/Proto.js',   list: list, },
		{ name: 'tim-Core-Common.js',  file: './Core/Common.js',  list: list, },
	);
};

/*
| Readies all imported tims.
|
| This is currently needed due to CJS<->ESM compatiblity.
| FIXME remove once everything is ESM.
*/
tim.ready =
	async function( )
{
	await tim.packages.ready( );
};

/*
| Registers a tim package
|
| ~name:     the name of the package
| ~metadata: the metadata of the module context
|            module for common js, import.meta for es modules
| ~src:      the source directory path
| ~ending:   the relative ending of registering filename to source path
|            (used to determine root path)
|
| ~return: the package spec.
*/
tim.register =
	async function( name, metadata, src, ending )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( typeof( ending ) !== 'string' ) throw new Error( );
/**/	if( typeof( src ) !== 'string' ) throw new Error( );
/**/	if( !src.endsWith( '/' ) ) throw new Error( );
/**/	if( typeof( ending ) !== 'string' ) throw new Error( );
/**/}

	let filename;
	if( metadata.require )
	{
		filename = metadata.filename;
	}
	else if( metadata.url )
	{
		filename = metadata.url;
		if( !filename.startsWith( 'file://' ) ) throw new Error( );
		filename = filename.substr( 7 );
	}
	else
	{
		throw new Error( 'invalid metadata' );
	}
	if( !filename.endsWith( ending ) ) throw new Error( );

	const srcAPath = filename.substr( 0, filename.length - ending.length );
	if( !srcAPath.endsWith( src ) ) throw new Error( );

	const rootdir = srcAPath.substr( 0, srcAPath.length - src.length );
	if( src.substr( 0, 2 ) !== './' ) src = './' + src;

	const srcPath = Path.FromString( src );
	const rootpath = Path.FromString( rootdir );

	const pkg =
		await tim.packages.addPackage(
			rootpath,
			srcPath,
			name,
			metadata,
			true /* timcodeGen */
		);

	return pkg;
};

};
