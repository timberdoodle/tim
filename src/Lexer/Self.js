/*
| The javascript lexer turns a string into a series of tokens.
*/
'use strict';

def.abstract = true;

const Token = tim.require( 'Lexer/Token/Self' );
const TokenList = tim.require( 'Lexer/Token/List' );

/*
| Tokenizes a javascript string.
|
| ~str: string to tokenize.
|
| ~return: a list of tokens.
*/
def.static.tokenize =
	function( str )
{
	if( typeof( str ) !== 'string' ) throw new Error( );

	const tokens = [ ];
	let line = Self._getLine( str, 0 );
	let lineNr = 1;
	let pos = 0;

	for( let a = 0, alen = str.length; a < alen; a++, pos++ )
	{
		const ch = str[ a ];

		if( ch === '\n' )
		{
			line = Self._getLine( str, a + 1 );
			lineNr++;
			pos = -1;
			continue;
		}

		// skips whitespaces
		if( ch === ' ' ) continue;

		// tabs
		if( ch === '\t' ) { pos += 7; continue; }

		// a keyword or an identifier
		if( ch.match( /[a-zA-Z_$]/ ) )
		{
			const tpos = pos;
			let value = ch;
			while( a + 1 < alen && str[ a + 1 ].match( /[a-zA-Z0-9_$]/ ) )
			{
				value += str[ ++a ];
				pos++;
			}

			if( Token.keywords.has( value ) )
			{
				tokens.push( Token.LLPTV( line, lineNr, tpos, value ) );
			}
			else
			{
				tokens.push( Token.LLPTV( line, lineNr, tpos, 'identifier', value ) );
			}
			continue;
		}

		// a number
		if( ch.match(/[0-9]/ ) )
		{
			let value = ch;
			while( a + 1 < alen && str[ a + 1 ].match( /[0-9x.A-Fa-f]/ ) )
			{
				value += str[ ++a ];
				pos++;
			}
			/*
			value = parseInt( value, 10 );
			if( a + 1 < alen && str[ a + 1 ] === '.' )
			{
				// it's a comma value
				a++; pos++; // skips the comma
				let cv = str[ ++a ]; pos++;
				while( a + 1 < alen && str[ a + 1 ].match( /[0-9]/ ) )
				{
					cv += str[ ++a ];
					pos++;
				}
				let cvl = cv.length;
				cv = parseInt( cv, 10 );
				while( cvl > 0 ) { cv /= 10; cvl--; }
				value += cv;
			}
			*/
			tokens.push( Token.LLPTV( line, lineNr, pos, 'number', value ) );
			continue;
		}

		switch( ch )
		{
			case ':':
			case ';':
			case ',':
			case '(':
			case ')':
			case '[':
			case ']':
			case '{':
			case '}':
			case '~':
				tokens.push( Token.LLPTV( line, lineNr, pos, ch ) );
				continue;

			case '"':
			{
				// a string literal
				let value = '';
				const tpos = pos;
				a++; pos++;
				if( a >= alen ) throw new Error( '" missing' );
				while( str[ a ] !== '"' )
				{
					const ch2 = str[ a ];
					if( ch2 === '\\' && a < alen && str[ a + 1 ] === '"' )
					{
						value += '\\"';
						a += 2; pos += 2;
					}
					else if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\\' )
					{
						value += '\\\\';
						a += 2; pos += 2;
					}
					else
					{
						value += ch2;
						a++; pos++;
					}
					if( a >= alen ) throw new Error( '" missing' );
				}
				tokens.push( Token.LLPTV( line, lineNr, tpos, 'string', value ) );
				continue;
			}

			case '\'':
			{
				// a string literal
				const tpos = pos;
				let value = '';
				a++; pos++;
				if( a >= alen ) throw new Error( '\' missing' );
				while( str[ a ] !== '\'' )
				{
					const ch2 = str[ a ];
					// currently transforms ' strings to " strings
					if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\'' )
					{
						value += '\\\'';
						a += 2; pos += 2;
					}
					else if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\\' )
					{
						value += '\\\\';
						a += 2; pos += 2;
					}
					else
					{
						value += ch2;
						a++; pos++;
					}
					if( a >= alen ) throw new Error( '\' missing' );
				}
				tokens.push( Token.LLPTV( line, lineNr, tpos, 'string', value ) );
				continue;
			}

			case '<':
				if( a + 1 < alen && str[ a + 1 ] === '<' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '<<' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '<=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '<' ) );
				}
				continue;

			case '>':
				if( a + 2 < alen && str[ a + 1 ] === '>' && str[ a + 2 ] === '>' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '>>>' ) );
					a += 2; pos += 2;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '>' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '>>' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '>=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '>' ) );
				}
				continue;

			case '?':
				if( a + 1 < alen && str[ a + 1 ] === '.' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '?.' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '?' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '??' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '?' ) );
				}
				continue;

			case '.':
				if( a + 2 < alen && str[ a + 1 ] === '.' && str[ a + 2 ] === '.' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '...' ) );
					a += 2; pos += 2;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, ch ) );
				}
				continue;

			case '=':
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					if( a + 2 < alen && str[ a + 2 ] === '=' )
					{
						tokens.push( Token.LLPTV( line, lineNr, pos, '===' ) );
						a += 2; pos += 2;
						continue;
					}
					else throw new Error( 'Use === instead of ==');
				}
				else if( a + 1 < alen && str[ a + 1 ] === '>' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '=>' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '=' ) );
				}
				continue;

			case '!':
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					if( a + 2 < alen && str[ a + 2 ] === '=' )
					{
						tokens.push( Token.LLPTV( line, lineNr, pos, '!==' ) );
						a += 2; pos += 2;
						continue;
					}
					else throw new Error( 'Use !== instead of !=');
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '!' ) );
				}
				continue;

			case '+':
				if( a + 1 < alen && str[ a + 1 ] === '+' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '++' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '+=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '+' ) );
				}
				continue;

			case '-':
				if( a + 1 < alen && str[ a + 1 ] === '-' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '--' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '-=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '-' ) );
				}
				continue;

			case '*':
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '*=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '*' ) );
				}
				continue;

			case '^':
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '^=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '^' ) );
				}
				continue;

			case '%':
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '%=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '%' ) );
				}
				continue;

			case '/':
				if( a + 1 < alen && str[ a + 1 ] === '/' )
				{
					// double slash comment
					a++;
					while( a + 1 < alen && str[ a ] !== '\n' ){ a++; }
					lineNr++;
					line = Self._getLine( str, a + 1 );
					pos = -1;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '*' )
				{
					// c-style comment
					a++; pos++;
					while( a + 1 < alen && ( str[ a ] !== '*' || str[ a + 1 ] !== '/' ) )
					{
						pos++;
						if( str[ a ] === '\n' )
						{
							line = Self._getLine( str, a + 1 );
							lineNr++;
							pos = 0;
						}
						a++;
					}
					a++; pos++;
				}
				else if(
					tokens.length > 0
					&& Self._beforeRegex.has( tokens[ tokens.length - 1 ].type )
				)
				{
					// it is a regular expression
					let regex = '';
					a++; pos++;
					let bracketLevel = 0;
					while( a + 1 < alen && ( str[ a ] !== '/' || bracketLevel > 0 ) )
					{
						// TODO handle backslash slash
						const ch2 = str[ a ];
						regex += ch2;
						switch( ch2 )
						{
							case '[': bracketLevel++; break;
							case ']': bracketLevel--; break;
						}
						pos++; a++;
					}
					tokens.push( Token.LLPTV( line, lineNr, pos, 'regex', regex ) );
					let flags = '';
					while( a + 1 < alen && Self._regexFlags.has( str[ a + 1 ] ) )
					{
						flags += str[ a + 1 ];
						pos++; a++;
					}
					if( flags !== '' )
					{
						tokens.push( Token.LLPTV( line, lineNr, pos, 'regex-flags', flags ) );
					}
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '/=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '/' ) );
				}
				continue;

			case '|':
				if( a + 1 < alen && str[ a + 1 ] === '|' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '||' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '|=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '|' ) );
				}
				continue;

			case '&':
				if( a + 1 < alen && str[ a + 1 ] === '&' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '&&' ) );
					a++; pos++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '&=' ) );
					a++; pos++;
				}
				else
				{
					tokens.push( Token.LLPTV( line, lineNr, pos, '&' ) );
				}
				continue;

			default:
			{
				console.error( 'Error in line ' + lineNr + ':' );
				console.error( line );
				let sp = '';
				for( let b = 1; b < pos; b++ ) sp += ' ';
				console.error( sp + '^' );
				console.error( sp + 'lexer error' );
				console.error( );
				throw new Error( );
			}
		}
	}
	return TokenList.create( 'list:init', tokens );
};

/*
| Tokens after which a '/' is supposed to be a regelar expression.
*/
def.staticLazy._beforeRegex = ( ) =>
	new Set( [ '=', ',', '(', '=>', 'return' ] );

/*
| Regular expression flags
*/
def.staticLazy._regexFlags = ( ) =>
	new Set( [ 'd', 'g', 'i', 'm', 's', 'u', 'y' ] );

/*
| Returns the line starting at p.
*/
def.static._getLine =
	function( str, p )
{
	let slen = str.length;
	let p2 = p;
	while( p2 < slen && str[ p2 ] !== '\n' ) p2++;
	return str.substring( p, p2 );
};
