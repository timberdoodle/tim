/*
| A lexer token.
*/
'use strict';

def.attributes =
{
	// line of the token
	line: { type: 'string' },

	// line nummer of the token
	lineNr: { type: 'integer' },

	// position of the token in the line
	pos: { type: 'integer' },

	// the token type
	type: { type: 'string' },

	// the token value
	value: { type: [ 'undefined', 'number', 'boolean', 'string' ] }
};

/*
| Shows an error at the token.
*/
def.proto.error =
	function( message )
{
	console.error( 'Error in line ' + this.lineNr + ':' );
	console.error( this.line );
	let sp = '';
	for( let p = 0, pos = this.pos; p < pos; p++ ) sp += ' ';
	console.error( sp + '^' );
	console.error( sp + message );
	console.error( );
	throw new Error( );
};

/*
| Shortcut to create a token of type t with value v.
|
| ~line: line
| ~lineNr: line number
| ~pos: position in line
| ~type: type
| ~value: value maybe undefined
*/
def.static.LLPTV =
	function( line, lineNr, pos, type, value )
{
	return(
		Self.create(
			'line', line,
			'lineNr', lineNr,
			'pos', pos,
			'type', type,
			'value', value,
		)
	);
};

/*
| Allowed token types.
*/
def.staticLazy._allowedTokens = ( ) =>
	new Set( [
		'=', '.', ',', '+', '-', '*', '/', '<', '>', '?', '^', '~',
		':', ';', '!', '[', ']', '(', ')', '{', '}', '&', '|', '%',
		'++', '--', '+=', '-=', '*=', '/=', '^=', '%=', '|=', '??',
		'&=', '<=', '>=', '=>', '||', '&&', '?.', '<<', '>>',
		'>>>', '===', '!==', '...',
		'async', 'await',
		'case', 'catch', 'const', 'default', 'delete', 'do', 'else',
		'false', 'finally', 'for', 'function', 'identifier', 'if', 'in',
		'instanceof', 'let', 'new', 'null', 'number', 'of',
		'regex', 'regex-flags', 'return', 'string', 'switch',
		'throw', 'true', 'try', 'typeof', 'while', 'yield',
	] );

/*
| Allow keywords.
*/
def.staticLazy.keywords = ( ) =>
	new Set( [
		'async', 'await',
		'case', 'catch', 'const', 'delete', 'default', 'do', 'else',
		'false', 'finally', 'for', 'function', 'if', 'in', 'instanceof',
		'let', 'new', 'null', 'of', 'return', 'switch', 'throw', 'true',
		'try', 'typeof', 'while', 'yield',
	] );

/*
| Forces a keyword token to a identifier.
*/
def.lazy.forceIdentifier =
	function( )
{
	if( !Self.keywords.has( this.type ) ) return this;
	return this.create( 'type', 'identifier', 'value', this.type );
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( !Self._allowedTokens.has( this.type ) ) throw new Error( );
/**/}
};
