/*
| A partial parser for javascript.
|
| This parser must not use ast-shorthands,
| because these are using the parser.
*/
'use strict';

def.abstract = true;

const And = tim.require( 'Ast/And' );
const ArrayLiteral = tim.require( 'Ast/ArrayLiteral' );
const ArrowFunction = tim.require( 'Ast/ArrowFunction' );
const Assign = tim.require( 'Ast/Assign' );
const AstNumber = tim.require( 'Ast/Number' );
const AstString = tim.require( 'Ast/String' );
const AstBoolean = tim.require( 'Ast/Boolean' );
const Await = tim.require( 'Ast/Await' );
const BitwiseAnd = tim.require( 'Ast/BitwiseAnd' );
const BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign' );
const BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift' );
const BitwiseNot = tim.require( 'Ast/BitwiseNot' );
const BitwiseOr = tim.require( 'Ast/BitwiseOr' );
const BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign' );
const BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift' );
const BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift' );
const BitwiseXor = tim.require( 'Ast/BitwiseXor' );
const BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign' );
const Block = tim.require( 'Ast/Block' );
const Call = tim.require( 'Ast/Call' );
const Case = tim.require( 'Ast/Case/Self' );
const CaseList = tim.require( 'Ast/Case/List' );
const Comma = tim.require( 'Ast/Comma' );
const Condition = tim.require( 'Ast/Condition' );
const ConditionalDot = tim.require( 'Ast/ConditionalDot' );
const Const = tim.require( 'Ast/Const' );
const Declaration = tim.require( 'Ast/Declaration' );
const Delete = tim.require( 'Ast/Delete' );
const DestructDecl = tim.require( 'Ast/DestructDecl' );
const Differs = tim.require( 'Ast/Differs' );
const Divide = tim.require( 'Ast/Divide' );
const DivideAssign = tim.require( 'Ast/DivideAssign' );
const Dot = tim.require( 'Ast/Dot' );
const DoWhile = tim.require( 'Ast/DoWhile' );
const Equals = tim.require( 'Ast/Equals' );
const ExprList = tim.require( 'Ast/ExprList' );
const For = tim.require( 'Ast/For' );
const ForIn = tim.require( 'Ast/ForIn' );
const ForOf = tim.require( 'Ast/ForOf' );
const Func = tim.require( 'Ast/Func/Self' );
const FuncArg = tim.require( 'Ast/Func/Arg/Self' );
const FuncArgList = tim.require( 'Ast/Func/Arg/List' );
const GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual' );
const GreaterThan = tim.require( 'Ast/GreaterThan' );
const If = tim.require( 'Ast/If' );
const Instanceof = tim.require( 'Ast/Instanceof' );
const LessOrEqual = tim.require( 'Ast/LessOrEqual' );
const LessThan = tim.require( 'Ast/LessThan' );
const Let = tim.require( 'Ast/Let' );
const Lexer = tim.require( 'Lexer/Self' );
const Member = tim.require( 'Ast/Member' );
const Minus = tim.require( 'Ast/Minus' );
const MinusAssign = tim.require( 'Ast/MinusAssign' );
const Multiply = tim.require( 'Ast/Multiply' );
const MultiplyAssign = tim.require( 'Ast/MultiplyAssign' );
const Negate = tim.require( 'Ast/Negate' );
const New = tim.require( 'Ast/New' );
const Not = tim.require( 'Ast/Not' );
const Null = tim.require( 'Ast/Null' );
const NullishCoalescence = tim.require( 'Ast/NullishCoalescence' );
const ObjLiteral = tim.require( 'Ast/ObjLiteral' );
const Or = tim.require( 'Ast/Or' );
const ParserSpec = tim.require( 'Parser/Spec' );
const ParserTokenList = tim.require( 'Parser/TokenList' );
const Plus = tim.require( 'Ast/Plus' );
const PlusAssign = tim.require( 'Ast/PlusAssign' );
const PostDecrement = tim.require( 'Ast/PostDecrement' );
const PostIncrement = tim.require( 'Ast/PostIncrement' );
const PreDecrement = tim.require( 'Ast/PreDecrement' );
const PreIncrement = tim.require( 'Ast/PreIncrement' );
const Regex = tim.require( 'Ast/Regex' );
const Remainder = tim.require( 'Ast/Remainder' );
const RemainderAssign = tim.require( 'Ast/RemainderAssign' );
const Return = tim.require( 'Ast/Return' );
const State = tim.require( 'Parser/State' );
const StringList = tim.require( 'string/list' );
const Switch = tim.require( 'Ast/Switch' );
const Throw = tim.require( 'Ast/Throw' );
const Token = tim.require( 'Lexer/Token/Self' );
const Try = tim.require( 'Ast/Try' );
const Typeof = tim.require( 'Ast/Typeof' );
const Undefined = tim.require( 'Ast/Undefined' );
const Var = tim.require( 'Ast/Var' );
const While = tim.require( 'Ast/While' );
const Yield = tim.require( 'Ast/Yield' );

/*
| Parses code to create an ast tree.
|
| ~args: list of strings to parse or ast subtrees
*/
def.static.block =
	function( ...args )
{
	const tokens = Self._tokenizeArray( args );
	let block = Block.Empty;
	if( tokens.length === 0 ) return block;
	let state = State.create( 'tokens', tokens, 'pos', 0, 'ast', undefined );
	while( !state.reachedEnd )
	{
		state = Self._parseToken( state, Self._parseStatementStart );
		block = block.append( state.ast );
		if( !state.reachedEnd && state.current.type === ';' ) state = state.advance( pass );
		state = state.stay( undefined );
	}
	return block;
};

/*
| Parses code to create an ast tree.
|
| ~array: the array of elements to parse
| ~start: parses at 'block', 'expression', or 'statement' level
*/
def.static.parseArray =
	function( array, start )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( !Array.isArray( array ) ) throw new Error( );
/**/}

	const tokens = Self._tokenizeArray( array );
	if( tokens.length === 0 ) return undefined;
	let state = State.create( 'tokens', tokens, 'pos', 0, 'ast', undefined );

	switch( start )
	{
		case 'expr':
			state = Self._parseToken( state, Self._parseExpressionStart );
			break;
		case 'statement':
			state = Self._parseToken( state, Self._parseStatementStart );
			break;
		default: throw new Error( );
	}

	// ignores one ending semicolon
	if( !state.reachedEnd && state.current.type === ';' ) state = state.advance( pass );
	if( !state.reachedEnd ) throw new Error( 'too many tokens' );

	return state.ast;
};

/*
| Parses code to create an ast tree.
|
| ~args: list of strings to parse or ast subtrees
*/
def.static.statement =
	function( ...args )
{
	return Self.parseArray( args, 'statement' );
};

/*
| Parses code as expression.
|
| ~args: list of strings to parse or ast subtrees.
*/
def.static.expr =
	function( ...args )
{
	return Self.parseArray( args, 'expr' );
};


/*
| Forces expr into a block.
*/
def.static._forceBlock =
	function( expr )
{
	if( expr.timtype === Block ) return expr;
	return Block.Empty.append( expr );
};

/*
| Returns the spec for a state
|
| ~state: current state
| ~statement: true if a statement is allowed here
*/
def.static._getSpec =
	function( state, statement )
{
	let spec;
	if( !state.ast )
	{
		if( statement ) spec = Self._statementSpecs[ state.current.type ];
		if( !spec ) spec = Self._leftSpecs[ state.current.type ];
	}
	else
	{
		spec = Self._rightSpecs[ state.current.type ];
	}
	if( !spec ) state.current.error( 'unexpected token' );
	return spec;
};

/*
| Handler for array literals.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleArrayLiteral =
	function( state, spec )
{
/**/if( CHECK && state.ast ) throw new Error( );

	// this is an array literal
	let alit = ArrayLiteral.Empty;
	state = state.advance( undefined );
	if( state.current.type !== ']' )
	{
		// there are elements
		while( state.current.type !== ']' )
		{
			state = Self._parseToken( state, spec );
			alit = alit.append( state.ast );

			// finished array literal?
			if( state.current.type === ']' ) break;

			if( state.current.type !== ',' )
			{
				state.current.error( 'parse error' );
				throw new Error( );
			}

			state = state.advance( undefined );
		}
	}

	// advances over closing square bracket
	return state.advance( alit );
};

/*
| Handlow for arrow functions.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleArrow =
	function( state, spec )
{
	// unwinds the state to be identifiers.
	let ast = state.ast;
	let args = StringList.Empty;

	if( ast === undefined )
	{
		// it's a ( ) =>
	}
	else if( ast.timtype === Var )
	{
		args = args.append( ast.name );
	}
	else if( ast.timtype === Comma )
	{
		while( ast.timtype === Comma )
		{
			const right = ast.right;
			if( right.timtype !== Var )
			{
				state.current.error( 'invalid arrow function' );
				throw new Error( );
			}
			args = args.append( right.name );
			ast = ast.left;
		}
		if( ast.timtype !== Var )
		{
			state.current.error( 'invalid arrow function' );
			throw new Error( );
		}
		const args2 = args.append( ast.name );
		args = StringList.Empty;
		for( let s of args2.reverse( ) ) args = args.append( s );
	}
	else
	{
		state.current.error( 'invalid arrow function' );
		throw new Error( );
	}

	// goes over the =>
	state = state.advance( undefined );

	if( state.current.type === '{' )
	{
		// it is a function body.
		state = Self._handleBlock( state );
	}
	else
	{
		// it is an expression.
		//state = Self._parseToken( state, Self._parseExpressionStart );

		// special expression start handling which should stop at a ,
		state = Self._parseToken(
			state, ParserSpec.Handler( '_handleParserError', 1.5 )
		);
	}

	const body = state.ast;
	return state.stay( ArrowFunction.create( 'args', args, 'body', body ) );
};

/*
| Handler for await operations.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleAwait =
	function( state, spec )
{
	const ast = state.ast;
	if( ast ) throw new Error( 'parse error' );
	state = Self._parseToken( state.advance( undefined ), spec );
	return state.stay( Await.create( 'call', state.ast ) );
};

/*
| Handler for boolean
|
| ~state: current parser state
*/
def.static._handleBlock =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== '{' ) throw new Error( );
/**/}

	state = state.advance( pass ); // skips the '{';
	let block = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = Self._parseToken( state, Self._parseStatementStart );
		block = block.append( state.ast );
		if( state.current.type === ';' ) state = state.advance( pass );
		state = state.stay( undefined );
	}
	return state.advance( block );
};

/*
| Handler for boolean literals.
|
| ~state: current parser state
*/
def.static._handleBooleanLiteral =
	function( state )
{
	let bool;
	switch( state.current.type )
	{
		case 'true': bool = true; break;
		case 'false': bool = false; break;
		default: throw new Error( );
	}
	return state.advance( AstBoolean.create( 'boolean', bool ) );
};

/*
| Handler for ( ) in case of calls.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleCall =
	function( state, spec )
{
	const ast = state.ast;

/**/if( CHECK && !ast ) throw new Error( );

	let call = Call.create( 'func', ast );
	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'expected ")"' );
	if( state.current.type !== ')' )
	{
		// there are arguments
		for( ;; )
		{
			do
			{
				state = Self._parseToken( state, spec );
				if( state.current.type === ';' ) throw new Error( 'parse error' );
			}
			while(
				!state.reachedEnd
				&& state.current.type !== ')'
				&& state.current.type !== ','
			);

			if( state.reachedEnd ) throw new Error( 'expected ")"' );

			if( state.ast ) call = call.$arg( state.ast );

			// finished call?
			if( state.current.type === ')' ) break;
			if( state.current.type === ',' )
			{
				state = state.advance( undefined );
				if( state.current.type === ')' ) break;
				continue;
			}

			state.current.error( 'parse error' );
			throw new Error( );
		}
	}

	// advances over closing bracket
	return state.advance( call );
};

/*
| Handler for ? : conditionals
|
| ~state, // current parser state
| ~spec   // operator spec
*/
def.static._handleCondition =
	function( state, spec )
{
	const condition = state.ast;

/**/if( CHECK )
/**/{
/**/	if( !condition ) throw new Error( );
/**/	if( state.current.type !== '?' ) throw new Error( );
/**/}

	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'parse error' );
	state = Self._parseToken( state, spec );
	const then = state.ast;
	state.expect( ':' );
	state = Self._parseToken( state.advance( undefined ), spec );

	return(
		state.stay(
			Condition.create(
				'condition', condition,
				'then', then,
				'elsewise', state.ast
			)
		)
	);
};

/*
| Handler for conditional dots.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleConditionalDot =
	function( state, spec )
{
	const ast = state.ast;
	if( !ast ) throw new Error( );
	let name = state.preview;
	if( name.type === 'delete' )
	{
		// "delete" may be used as identifier here
		name = name.create( 'type', 'identifier', 'value', 'delete' );
	}
	else if( name.type !== 'identifier' )
	{
		state.current.error( 'parse error' );
		throw new Error( );
	}

	return(
		state.create(
			'ast', ConditionalDot.create( 'expr', state.ast, 'member', name.value ),
			'pos', state.pos + 2
		)
	);
};

/*
| Handler for do (while).
|
| ~state: current parser state
*/
def.static._handleDo =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'do' ) throw new Error( );
/**/}

	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseStatementStart );
	const body = Self._forceBlock( state.ast );
	if( state.current.type === ';' ) state = state.advance( pass );
	state.expect( 'while' );
	state = state.advance( undefined, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseStatementStart );
	const condition = state.ast;
	state.expect( ')' );
	return state.advance( DoWhile.create( 'body', body, 'condition', condition ) );
};

/*
| Handler for dots.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleDot =
	function( state, spec )
{
	const ast = state.ast;
	if( !ast ) throw new Error( );
	let name = state.preview.forceIdentifier;

	if( name.type !== 'identifier' )
	{
		state.current.error( 'parse error' );
		throw new Error( );
	}

	return(
		state.create(
			'ast', Dot.create( 'expr', state.ast, 'member', name.value ),
			'pos', state.pos + 2
		)
	);
};

/*
| Generic handler for dualistic operations.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleDualisticOps =
	function( state, spec )
{
	const ast = state.ast;
	if( !ast ) throw new Error( 'parse error' );
	state = Self._parseToken( state.advance( undefined ), spec );
	return state.stay( spec.astCreator.create( 'left', ast, 'right', state.ast ) );
};

/*
| Handler for for.
|
| ~state: current parser state
| ~spec:  operator spec
*/
def.static._handleFor =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'for' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'unexpected end of file' );

	if( state.current.type === 'let' )
	{
		state = Self._handleLet( state );
	}
	else if( state.current.type !== ';' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}

	if( state.current.type === 'in' || state.current.type === 'of' )
	{
		let loopVar = state.ast;
		let letVar = false;
		const forType = state.current.type;
		if( loopVar.timtype === Let )
		{
			if( loopVar.length !== 1 ) throw new Error( 'invalid let in for of/in loop' );
			const letEntry = loopVar.get( 0 );
			if( letEntry.assign ) throw new Error( 'invalid assignment in for of/in loop' );
			loopVar = Var.create( 'name', letEntry.name );
			letVar = true;
		}
		else if( loopVar.timtype !== Var )
		{
			throw new Error( 'invalid loop variable' );
		}

		state = Self._parseToken( state.advance( undefined ), Self._parseExpressionStart );
		const obj = state.ast;
		state.expect( ')' );
		state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );
		const block = state.ast;

		if( !Self._noSemicolon.has( block.timtype ) )
		{
			if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
			state = state.advance( pass );
		}

		switch( forType )
		{
			case 'in':
				return(
					state.stay(
						ForIn.create(
							'variable', loopVar,
							'letVar', letVar,
							'object', obj,
							'block', Self._forceBlock( block )
						)
					)
				);
			case 'of':
				return(
					state.stay(
						ForOf.create(
							'variable', loopVar,
							'letVar', letVar,
							'object', obj,
							'block', Self._forceBlock( block )
						)
					)
				);
			default: throw new Error( );
		}
	}

	if( state.current.type !== ';' ) throw new Error( '";" or "in" or "of" expected' );
	state = state.advance( pass );
	const init = state.ast;
	state = state.stay( undefined );
	if( state.current.type !== ';' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}
	const condition = state.ast;
	state.expect( ';' );
	state = state.advance( undefined );
	if( state.current.type !== ')' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}
	const iterate = state.ast;
	state.expect( ')' );
	state = state.advance( undefined );
	state = Self._parseToken( state, Self._parseStatementStart );
	const block = state.ast;
	if( !Self._noSemicolon.has( block.timtype ) )
	{
		if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
		state = state.advance( pass );
	}
	return(
		state.stay(
			For.create(
				'init', init,
				'condition', condition,
				'iterate', iterate,
				'block', Self._forceBlock( block )
			)
		)
	);
};

/*
| Handler for function declaration (those with function names).
|
| ~state: current parser state
*/
def.static._handleFunctionDeclaration =
	function( state )
{
/**/if( CHECK && state.ast ) throw new Error( );

	let isAsync = false;
	if( state.current.type === 'async' )
	{
		isAsync = true;
		state = state.advance( pass, 'function' );
	}
	state = state.advance( pass, 'identifier' );
	const name = state.current.value;
	state = state.advance( pass, '(' );
	state = state.advance( pass );
	let args = FuncArgList.Empty;
	while( state.current.type !== ')' )
	{
		let rest = false;
		if( state.current.type === '...' )
		{
			rest = true;
			state = state.advance( pass );
		}
		state.expect( 'identifier' );
		args = args.append( FuncArg.create( 'name', state.current.value, 'rest', rest ) );
		state = state.advance( pass, ')', ',' );
		if( state.current.type === ',' ) state = state.advance( pass );
	}
	state = state.advance( pass, '{' );
	state = state.advance( );

	let body = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = state.stay( undefined );
		state = Self._parseToken( state, Self._parseStatementStart );
		body = body.append( state.ast );
		if( state.current.type === ';' ) state = state.advance( pass );
	}
	return(
		state.advance(
			Func.create(
				'args', args,
				'body', body,
				'isAsync', isAsync,
				'name', name,
			)
		)
	);
};

/*
| Handler for ( ) in case of groupings.
|
| ~state: current parser state
| ~spec:  operator spec
*/
def.static._handleGrouping =
	function( state, spec )
{
/**/if( CHECK && state.ast ) throw new Error( );

	// it may actually be ( ) => arrow function
	if( state.foresight( ')', '=>' ) )
	{
		return(
			Self._handleArrow(
				state.advance( undefined ).advance( undefined ),
				Self._rightSpecs[ '=>' ]
			)
		);
	}

	state = state.advance( undefined );
	state = Self._parseToken( state, spec );
	while( state.current.type !== ')' )
	{
		state = Self._parseToken( state, spec );
		if( state.reachedEnd ) throw new Error( 'expected ")"' );
	}
	return state.advance( pass );
};

/*
| Handler for identifiers.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleIdentifier =
	function( state )
{
	return state.advance( Var.create( 'name', state.current.value ) );
};

/*
| Handler for if.
|
| ~state: current parser state
*/
def.static._handleIf =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'if' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );
	const condition = state.ast;
	state.expect( ')' );
	state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );

	const then = state.ast;
	if( !Self._noSemicolon.has( then.timtype ) )
	{
		if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
		state = state.advance( pass );
	}

	let elsewise;
	if( state.current && state.current.type === 'else' )
	{
		state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );
		elsewise = state.ast;

		if( !Self._noSemicolon.has( elsewise.timtype ) )
		{
			if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
			state = state.advance( pass );
		}
	}

	return(
		state.stay(
			If.create(
				'condition', condition,
				'then', Self._forceBlock( then ),
				'elsewise', elsewise && Self._forceBlock( elsewise )
			)
		)
	);
};

/*
| Handler for inline function
|
| ~state: current parser state
*/
def.static._handleInlineFunction =
	function( state )
{
/**/if( CHECK && state.ast ) throw new Error( );

	let isAsync = false;
	if( state.current.type === 'async' )
	{
		isAsync = true;
		state = state.advance( pass, 'function' );
	}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	let args = FuncArgList.Empty;
	while( state.current.type !== ')' )
	{
		let rest = false;
		if( state.current.type === '...' )
		{
			rest = true;
			state = state.advance( pass );
		}
		state.expect( 'identifier' );
		args = args.append( FuncArg.create( 'name', state.current.value, 'rest', rest ) );
		state = state.advance( pass, ')', ',' );
		if( state.current.type === ',' ) state = state.advance( pass );
	}
	state = state.advance( pass, '{' );
	state = state.advance( );

	let body = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = state.stay( undefined );
		state = Self._parseToken( state, Self._parseStatementStart );
		body = body.append( state.ast );
		if( state.current.type === ';' ) state = state.advance( pass );
	}
	return(
		state.advance(
			Func.create(
				'args', args,
				'body', body,
				'isAsync', isAsync,
			)
		)
	);
};

/*
| Handler for member operations.
|
| ~state: current parser state
| ~spec:  operator spec
*/
def.static._handleMember =
	function( state, spec )
{
	const ast = state.ast;
/**/if( CHECK && !ast ) throw new Error( );
	state = Self._parseToken( state.advance( undefined ), spec );
	while( state.current.type !== ']' )
	{
		state = Self._parseToken( state, spec );
		if( state.reachedEnd ) throw new Error( );
	}
	return state.advance( Member.create( 'expr', ast, 'member', state.ast ) );
};

/*
| Generic handler for left side mono operations.
| ~state: current parser state
| ~spec:  operator spec
*/
def.static._handleMonoOps =
	function( state, spec )
{
	const ast = state.ast;
	if( ast )
	{
		// postfix (increment or decrement)
		return state.advance( spec.astCreator.create( 'expr', state.ast ) );
	}
	state = Self._parseToken( state.advance( undefined ), spec );
	return state.stay( spec.astCreator.create( 'expr', state.ast ) );
};

/*
| Handler for let.
|
| ~state: current parser state
*/
def.static._handleLet =
	function( state )
{
	const type = state.current.type;

/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( type !== 'const' && type !== 'let' ) throw new Error( );
/**/}

	let decl = type === 'const' ? Const.Empty : Let.Empty;
	state = state.advance( pass );
	for( ;; )
	{
		if( state.reachedEnd ) return state.stay( decl );
		if( state.current.type === ';' ) return state.stay( decl );

		let entry;
		switch( state.current.type )
		{
			case 'identifier':
			{
				let name = state.current.value;
				state = state.advance( pass );
				if( state.reachedEnd )
				{
					let entry = Declaration.create( 'name', name );
					decl = decl.append( entry );
					return state.stay( decl );
				}
				entry = Declaration.create( 'name', name );
				break;
			}
			case '{':
			{
				state = Self._handleObjectLiteral( state, Self._leftSpecs[ '{' ] );
				entry = DestructDecl.create( 'literal', state.ast );
				break;
			}
			default:
				state.current.error( 'unexpected token' );
				throw new Error( );
		}

		switch( state.current.type )
		{
			case '=':
			{
				state =
					Self._parseToken( state.advance( undefined ), Self._statementSpecs[ 'let' ] );
				entry = entry.create( 'assign', state.ast );
				decl = decl.append( entry );
				if( !state.reachedEnd && state.current.type === ',' ) state = state.advance( pass );
				break;
			}
			case ',':
			{
				state = state.advance( undefined );
				decl = decl.append( entry );
				break;
			}
			case ';':
			case 'of':
			case 'in':
			{
				decl = decl.append( entry );
				return state.stay( decl );
			}
			default: throw new Error( 'unexpected token', state.current.type );
		}
	}
};

/*
| Handler for new operations.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleNew =
	function( state, spec )
{
	const ast = state.ast;
	if( ast ) throw new Error( 'parse error' );
	state = Self._parseToken( state.advance( undefined ), spec );
	return state.stay( New.create( 'call', state.ast ) );
};

/*
| Handler for null.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleNull = function( state ) { return state.advance( Null.singleton ); };

/*
| Handler for numeric literals.
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleNumber =
	function( state )
{
	return state.advance( AstNumber.create( 'number', state.current.value ) );
};

/*
| Handler for { } Object literals
|
| ~state: current parser state
| ~spec: operator spec
*/
def.static._handleObjectLiteral =
	function( state, spec )
{
	const ast = state.ast;
	if( ast ) throw new Error( 'parse error' );

	// this is an array literal
	let olit = ObjLiteral.create( );

	// skips the '{'
	state = state.advance( undefined, '}', 'identifier', 'string' );

	while( state.current.type !== '}' )
	{
		let name;
		switch( state.current.type )
		{
			case 'identifier':
			case 'string':
				name = state.current.value;
				break;
			// FUTURE add '[' case
			default:
				throw new Error( );
		}

		state = state.advance( pass );
		if( state.current.type === ':' )
		{
			state = state.advance( pass );
			state = Self._parseToken( state, spec );
			olit = olit.create( 'twig:add', name, state.ast );
		}
		else
		{
			olit = olit.create( 'twig:add', name, Undefined.singleton );
		}
		state.expect( ',', '}' );
		if( state.current.type === ',' ) state = state.advance( undefined );
	}

	// advances over closing square bracket
	return state.advance( olit );
};

/*
| Generic parser error.
|
| ~state: (current parser state)
| ~spec:  (operator spec)
*/
def.static._handleParserError =
	function( )
{
	throw new Error( 'parse error' );
};

/*
| Generic pass handler.
| It just passes back up
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handlePass =
	function( state )
{
	return state;
};

/*
| Handler for regular expressions.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleRegex =
	function( state )
{
	let flags, string;
	if( state.preview.type === 'regex-flags' )
	{
		string = state.current.value;
		state = state.advance( pass );
		flags = state.current.value;
	}
	else
	{
		string = state.current.value;
	}
	return state.advance( Regex.create( 'string', string, 'flags', flags ) );
};

/*
| Handler for return.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleReturn =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'return' ) throw new Error( );
/**/}
	state = state.advance( pass );

	if( state.current.type === ';' ) return state.stay( Return.create( ) );

	state = Self._parseToken( state, Self._parseExpressionStart );
	return state.stay( Return.create( 'expr', state.ast ) );
};

/*
| Handler for string literals.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleString =
	function( state )
{
	return state.advance( AstString.create( 'string', state.current.value ) );
};

/*
| Handler for switch.
|
| ~state: current parser state
*/
def.static._handleSwitch =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'switch' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );

	const statement = state.ast;
	if( state.current.type !== ')' ) throw new Error( 'expected ":"' );
	state = state.advance( undefined, '{' );
	state = state.advance( pass, 'case', 'default', '}' );

	let cases = CaseList.Empty;
	let defaultCase;

	while( state.current.type !== '}' )
	{
		if( state.current.type === 'default' )
		{
			state = state.advance( undefined, ':' );
			state = state.advance( pass );
			let block = Block.Empty;
			while( state.current.type !== '}' && state.current.type !== 'case' )
			{
				state = Self._parseToken( state, Self._parseStatementStart );
				block = block.append( state.ast );
				if( state.current.type === ';' ) state = state.advance( pass );
				state = state.stay( undefined );
			}
			defaultCase = block;
			continue;
		}

		let values = ExprList.Empty;
		while( state.current.type === 'case' )
		{
			state = state.advance( undefined );
			state = Self._parseToken( state, Self._parseExpressionStart );
			state.expect( ':' );
			values = values.append( state.ast );
			state = state.advance( undefined );
		}

		let block = Block.Empty;
		while(
			state.current.type !== '}'
			&& state.current.type !== 'case'
			&& state.current.type !== 'default'
		)
		{
			state = Self._parseToken( state, Self._parseStatementStart );
			block = block.append( state.ast );
			if( state.current.type === ';' ) state = state.advance( pass );
			state = state.stay( undefined );
		}
		cases = cases.append( Case.create( 'block', block, 'values', values ) );
	}
	return(
		state.advance(
			Switch.create(
				'cases', cases,
				'defaultCase', defaultCase,
				'statement', statement,
			)
		)
	);
};

/*
| Handler for throw.
|
| ~state: current parser state
| ~spec:  (operator spec)
*/
def.static._handleThrow =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'throw' ) throw new Error( );
/**/}

	state = Self._parseToken( state.advance( pass ), Self._parseExpressionStart );
	return state.stay( Throw.create( 'expr', state.ast ) );
};

/*
| Handler for try.
|
| ~state: current parser state
*/
def.static._handleTry =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'try' ) throw new Error( );
/**/}

	state = state.advance( pass, '{' );
	state = Self._parseToken( state, Self._parseStatementStart );
	const tryStatement = state.ast;
	state.expect( 'catch' );
	state = state.advance( undefined, '(' );
	state = state.advance( pass, 'identifier' );
	const exceptionVar = Var.create( 'name', state.current.value );
	state = state.advance( undefined, ')' );
	state = state.advance( undefined, '{' );
	state = Self._parseToken( state, Self._parseStatementStart );
	const catchStatement = state.ast;
	return(
		state.stay(
			Try.create(
				'exceptionVar', exceptionVar,
				'catchStatement', catchStatement,
				'tryStatement', tryStatement,
			)
		)
	);
};

/*
| Handler for while.
|
| ~state: current parser state
*/
def.static._handleWhile =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'while' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );
	const condition = state.ast;
	state.expect( ')' );
	state = state.advance( undefined );
	state = Self._parseToken( state, Self._parseStatementStart );

	const body = state.ast;
	if( !Self._noSemicolon.has( body.timtype ) )
	{
		state.expect( ';' );
		state = state.advance( pass );
	}

	return state.stay( While.create( 'condition', condition, 'body', body ) );
};

/*
| Left token specifications for unary operants.
|
| They are consulted when the current parse buffer is *not* empty.
*/
def.staticLazy._leftSpecs = ( ) =>
{
	const c = ParserSpec.Handler;
	const s = { };
	s[ 'true' ]       = c( '_handleBooleanLiteral', 100 );
	s[ 'false' ]      = c( '_handleBooleanLiteral', 100 );
	s.identifier      = c( '_handleIdentifier',     100 );
	s.null            = c( '_handleNull',           100 );
	s.number          = c( '_handleNumber',         100 );
	s.regex           = c( '_handleRegex',          100 );
	s.string          = c( '_handleString',         100 );

	s[ '(' ]          = c( '_handleGrouping',        19, 'l2r' );
	s[ 'new' ]        = c( '_handleNew',             17, 'r2l' );
	s[ '++' ]         = c( '_handleMonoOps',         16, 'n/a', PreIncrement );
	s[ '--' ]         = c( '_handleMonoOps',         16, 'n/a', PreDecrement );
	s[ '~' ]          = c( '_handleMonoOps',         15, 'r2l', BitwiseNot );
	s[ '-' ]          = c( '_handleMonoOps',         15, 'r2l', Negate );
	s[ '!' ]          = c( '_handleMonoOps',         15, 'r2l', Not );
	s[ 'delete' ]     = c( '_handleMonoOps',         15, 'r2l', Delete );
	s[ 'typeof' ]     = c( '_handleMonoOps',         15, 'r2l', Typeof );
	s[ 'await' ]      = c( '_handleMonoOps',         15, 'r2l', Await );
	s[ 'yield' ]      = c( '_handleMonoOps',          2, 'r2l', Yield );
	s[ '[' ]          = c( '_handleArrayLiteral',   1.5, 'l2r' );
	s[ '{' ]          = c( '_handleObjectLiteral',  1.5 );
	s[ ',' ]          = c( '_handleDualisticOps',     1, 'l2r', Comma );
	s[ 'async' ]      = c( '_handleInlineFunction',   0 );
	s[ 'function' ]   = c( '_handleInlineFunction',   0 );
	return Object.freeze( s );
};

/*
| Right token specifications for unary operants.
|
| They are consulted when the current parse buffer is empty.
*/
def.staticLazy._rightSpecs = ( ) =>
{
	const c = ParserSpec.Handler;
	const s = { };
	s[ '=>' ]         = c( '_handleArrow',           20, 'l2r' );
	s[ '(' ]          = c( '_handleCall',            19, 'l2r' );
	s[ '[' ]          = c( '_handleMember',          18, 'l2r' );
	s[ ']' ]          = c( '_handlePass',            18 ); // 0?
	s[ '.' ]          = c( '_handleDot',             18, 'l2r' );
	s[ '?.' ]         = c( '_handleConditionalDot',  18, 'l2r' );
	s[ '++' ]         = c( '_handleMonoOps',         16, 'n/a', PostIncrement );
	s[ '--' ]         = c( '_handleMonoOps',         16, 'n/a', PostDecrement );
	s[ '*' ]          = c( '_handleDualisticOps',    13, 'l2r', Multiply );
	s[ '/' ]          = c( '_handleDualisticOps',    13, 'l2r', Divide );
	s[ '%' ]          = c( '_handleDualisticOps',    13, 'l2r', Remainder );
	s[ '+' ]          = c( '_handleDualisticOps',    12, 'l2r', Plus );
	s[ '-' ]          = c( '_handleDualisticOps',    12, 'l2r', Minus );
	s[ '<<' ]         = c( '_handleDualisticOps',    11, 'l2r', BitwiseLeftShift );
	s[ '>>' ]         = c( '_handleDualisticOps',    11, 'l2r', BitwiseRightShift );
	s[ '>>>' ]        = c( '_handleDualisticOps',    11, 'l2r', BitwiseUnsignedRightShift );
	s[ '<=' ]         = c( '_handleDualisticOps',    10, 'l2r', LessOrEqual );
	s[ '<' ]          = c( '_handleDualisticOps',    10, 'l2r', LessThan );
	s[ '>=' ]         = c( '_handleDualisticOps',    10, 'l2r', GreaterOrEqual );
	s[ '>' ]          = c( '_handleDualisticOps',    10, 'l2r', GreaterThan );
	s[ '===' ]        = c( '_handleDualisticOps',     9, 'l2r', Equals );
	s[ '!==' ]        = c( '_handleDualisticOps',     9, 'l2r', Differs );
	s[ 'instanceof' ] = c( '_handleDualisticOps',     9, 'l2r', Instanceof );
	s[ '&' ]          = c( '_handleDualisticOps',     8, 'l2r', BitwiseAnd );
	s[ '^' ]          = c( '_handleDualisticOps',     7, 'l2r', BitwiseXor );
	s[ '|' ]          = c( '_handleDualisticOps',     6, 'l2r', BitwiseOr );
	s[ '&&' ]         = c( '_handleDualisticOps',     5, 'l2r', And );
	s[ '||' ]         = c( '_handleDualisticOps',     4, 'l2r', Or );
	s[ '??' ]         = c( '_handleDualisticOps',     4, 'l2r', NullishCoalescence );
	s[ '?' ]          = c( '_handleCondition',        3, 'l2r' );
	s[ '=' ]          = c( '_handleDualisticOps',     2, 'r2l', Assign );
	s[ '+=' ]         = c( '_handleDualisticOps',     2, 'r2l', PlusAssign );
	s[ '-=' ]         = c( '_handleDualisticOps',     2, 'r2l', MinusAssign );
	s[ '*=' ]         = c( '_handleDualisticOps',     2, 'r2l', MultiplyAssign );
	s[ '/=' ]         = c( '_handleDualisticOps',     2, 'r2l', DivideAssign );
	s[ '&=' ]         = c( '_handleDualisticOps',     2, 'r2l', BitwiseAndAssign );
	s[ '|=' ]         = c( '_handleDualisticOps',     2, 'r2l', BitwiseOrAssign );
	s[ '^=' ]         = c( '_handleDualisticOps',     2, 'r2l', BitwiseXorAssign );
	s[ '%=' ]         = c( '_handleDualisticOps',     2, 'r2l', RemainderAssign );
	s[ ',' ]          = c( '_handleDualisticOps',     1, 'l2r', Comma );
	s[ ')' ]          = c( '_handlePass',             0 );
	s[ ':' ]          = c( '_handlePass',             0 );
	s[ ';' ]          = c( '_handlePass',            -1 );
	s[ 'of' ]         = c( '_handlePass',            -1 );
	s[ 'in' ]         = c( '_handlePass',            -1 );
	s[ '}' ]          = c( '_handlePass',            -1 );
	s[ 'else' ]       = c( '_handlePass',            -1 );
	return Object.freeze( s );
};

/*
| Phony spec denoting start of parsing on expression possiblity.
*/
def.staticLazy._parseExpressionStart = ( ) =>
	ParserSpec.Handler( '_handleParserError', 0 );

/*
| Phony spec denoting start of parsing on statement/expression possiblity
*/
def.staticLazy._parseStatementStart = ( ) =>
	ParserSpec.Handler( '_handleParserError', -1 );

/*
| Statement token specifications.
*/
def.staticLazy._statementSpecs = ( ) =>
{
	const s = { };
	s[ 'async' ] = ParserSpec.create( 'handler', '_handleFunctionDeclaration' );
	s[ 'do' ] = ParserSpec.create( 'handler', '_handleDo' );
	s[ 'if' ] = ParserSpec.create( 'handler', '_handleIf' );
	s[ 'for' ] = ParserSpec.create( 'handler', '_handleFor' );
	s[ 'function' ] = ParserSpec.create( 'handler', '_handleFunctionDeclaration' );
	// 1.5: stronger than comma, so multiple let definitions are not treated as comma operator
	s[ 'const' ] = s[ 'let' ] = ParserSpec.create( 'handler', '_handleLet', 'prec', 1.5 );
	s[ 'return' ] = ParserSpec.create( 'handler', '_handleReturn' );
	s[ 'switch' ] = ParserSpec.create( 'handler', '_handleSwitch' );
	s[ 'try' ] = ParserSpec.create( 'handler', '_handleTry' );
	s[ 'throw' ] = ParserSpec.create( 'handler', '_handleThrow' );
	s[ 'while' ] = ParserSpec.create( 'handler', '_handleWhile' );
	s[ '{' ] = ParserSpec.create( 'handler', '_handleBlock' );
	//s[ '}' ] = ParserSpec.create( 'handler', '_handlePass', 'prec', 0 );
	return Object.freeze( s );
};

/*
| Ast nodes which ate their semicolon.
*/
def.staticLazy._noSemicolon = ( ) =>
{
	const set = new Set( );
	set.add( Block );
	set.add( DoWhile );
	set.add( For );
	set.add( ForIn );
	set.add( ForOf );
	set.add( Func );
	set.add( If );
	set.add( Switch );
	set.add( Try );
	set.add( While );
	return set;
};

/*
| Parses a token at current state (which has a pos from a tokenList).
|
| ~state: current parser state
| ~spec: current operator specification
*/
def.static._parseToken =
	function( state, spec )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( !state.ast && state.current.timtype !== Token )
	{
		// this is already a preparsed astTree.
		state = state.advance( state.current );
	}
	else
	{
		const tokenSpec = Self._getSpec( state, spec.prec < 0 );
		// TODO needed?
		if( tokenSpec.handler === '_handlePass' ) return state;
		state = Self[ tokenSpec.handler ]( state, tokenSpec );
	}

	switch( state.ast.timtype )
	{
		case Block:
		case DoWhile:
		case For:
		case ForIn:
		case ForOf:
		case If:
		case Switch:
		case Try:
		case While:
			return state;
		case Func:
			// only named functions stop parsing
			if( state.ast.name ) return state;
			break;
	}

	while( !state.reachedEnd )
	{
		const nextSpec = Self._getSpec( state, false );
		if(
			nextSpec.prec === undefined
			|| nextSpec.prec < spec.prec
			|| ( nextSpec.prec === spec.prec && spec.associativity === 'l2r' )
			|| nextSpec.handler === '_handlePass'
		) break;
		state = Self._parseToken( state, nextSpec );
	}
	return state;
};

/*
| Tokenizes an array.
|
| ~array: the array
*/
def.static._tokenizeArray =
	function( array )
{
	let tokens = ParserTokenList.create( );
	for( let arg of array )
	{
		if( arg === undefined ) continue;
		if( typeof( arg ) === 'string' )
		{
			tokens = tokens.appendList( Lexer.tokenize( arg ) );
		}
		else if( Array.isArray( arg ) )
		{
			tokens = tokens.appendList( Self._tokenizeArray( arg ) );
		}
		else
		{
			tokens = tokens.append( arg );
		}
	}
	return tokens;
};
