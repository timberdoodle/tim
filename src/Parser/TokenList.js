/*
| A parser token list.
| can hold lexed token as well as ast subtrees.
*/
'use strict';

def.list =
[
	'< Ast/Type/Statement',
	'< Ast/Type/Expr',
	'Lexer/Token/Self',
];
