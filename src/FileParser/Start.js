/*
| Parses a file.
*/
'use strict';

Error.stackTraceLimit = Infinity;
global.CHECK = true;
global.NODE = true;

require( '../root' );
const pkg = tim.register( 'repl', module, 'src/', 'FileParser/Start.js' );
pkg.require( 'FileParser/Root' ).init( );
