/*
| Runs the file parser tool.
*/
'use strict';

def.abstract = true;

const fs = require( 'fs' );
//const util = require( 'util' );

const Format = tim.require( 'Format/Self' );
const Parser = tim.require( 'Parser/Self' );

const listing = require( '../Ouroboros/Listing' );

const exceptions = new Set( [ 'src/Core/Browser.js' ] );

/*
| Parses all .js files in a directory.
| Recurses into subdirs.
*/
def.static.dir =
	function( dirname )
{
	const dir = fs.readdirSync( dirname, { encoding: 'utf8', withFileTypes: true } );
	for( let entry of dir )
	{
		const subname = dirname + '/' + entry.name;
		if( entry.isDirectory( ) )
		{
			Self.dir( subname );
			continue;
		}
		if( !subname.endsWith( '.js' ) ) continue;
		if( exceptions.has( subname ) ) continue;
		console.log( '--- ' + subname + ' ---' );
		const text = fs.readFileSync( subname ) + '';
		const tree = Parser.block( text );
		// XXX
		console.log( Format.format( tree ) );
	}
};

/*
| Entrypoint.
*/
def.static.init =
	function( )
{
	if( process.argv.length < 3 )
	{
		console.log( 'Filename to parse missing.' );
		return;
	}

	if( process.argv[ 2 ] === '--listing' )
	{
		for( let filename of listing )
		{
			console.log( filename );
			const text = fs.readFileSync( 'src/' + filename ) + '';
			const tree = Parser.block( text );
			console.log( Format.format( tree ) );
		}
		return;
	}

	if( process.argv[ 2 ] === '--dir' )
	{
		if( process.argv.length < 4 )
		{
			console.log( 'Dir to scan missing.' );
			return;
		}
		Self.dir( process.argv[ 3 ] );
		return;
	}

	const filename = process.argv[ 2 ];
	const text = fs.readFileSync( filename ) + '';

	const tree = Parser.block( text );
	console.log( Format.format( tree ) );
};
