/*
| Plan.
*/
'use strict';

def.abstract = true;

const Piece = tim.require( 'Plan/Piece/Self' );
const Subs = tim.require( 'Plan/Piece/Subs' );
const Types = tim.require( 'protean/group' );

/*
| Helper to build a sub piece
|
| ~o: user object to build from.
| ~name: name of the plan piece.
*/
function buildSubPiece( o, name )
{
	let subs = o.subs;
	let types = o.types;
	const at = o.at || false;
	const key = o.key || false;

	if( subs && !subs.timtype ) subs = buildSubs( subs );
	if( types && !types.timtype ) types = Types.TimTypes.apply( undefined, types );

	return(
		Piece.create(
			'at', at,
			'key', key,
			'name', name,
			'subs', subs,
			'types', types,
		)
	);
}

/*
| Helper to build subs.
*/
function buildSubs( subs )
{
	for( let name in subs )
	{
		const s = subs[ name ];
		// FIXME remove boolean
		if( typeof( s ) === 'boolean' || typeof( s ) === 'function' || s.timtype ) continue;
		subs[ name ] = buildSubPiece( s, name );
	}
	return Subs.Table( subs );
}

/*
| Builds a plan.
*/
def.static.Build =
	function( o )
{
	const name = o.name;
	let subs = o.subs;
	let types = o.types;
	const at = o.at || false;
	const key = o.key || false;

	if( subs && !subs.timtype ) subs = buildSubs( subs );
	if( types && !types.timtype ) types = Types.TimTypes.apply( undefined, types );

	return(
		Piece.create(
			'at', at,
			'key', key,
			'name', name,
			'subs', subs,
			'types', types,
		)
	);
};
