/*
| Plan piece.
*/
'use strict';

def.attributes =
{
	// true if this plan piece has an at
	at: { type: 'boolean', defaultValue: 'false' },

	// true if this plan piece has a key
	key: { type: 'boolean', defaultValue: 'false' },

	// name of the plan piece
	name: { type: 'string' },

	// sub traces
	subs: { type: [ 'undefined', 'Plan/Piece/Subs' ] },

	// types for this plan piece
	types: { type: [ 'undefined', 'protean/group' ] },
};

/*
| Checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK && this.at && this.key ) throw new Error( );
};
