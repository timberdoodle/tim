/*
| A group for plan sub pieces.
|
| It is either the plan piece itself, or a true boolean to look up in "plan".
*/
'use strict';

def.group = [ 'function', 'Plan/Piece/Self' ];
