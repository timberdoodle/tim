/*
| Formats an abstract syntax tree into a .js file.
*/
'use strict';

def.abstract = true;

const MaxLineWidth = 98;

const And = tim.require( 'Ast/And' );
const ArrayLiteral = tim.require( 'Ast/ArrayLiteral' );
const ArrowFunction = tim.require( 'Ast/ArrowFunction' );
const Assign = tim.require( 'Ast/Assign' );
const AstBoolean = tim.require( 'Ast/Boolean' );
const Await = tim.require( 'Ast/Await' );
const BitwiseAnd = tim.require( 'Ast/BitwiseAnd' );
const BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign' );
const BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift' );
const BitwiseNot = tim.require( 'Ast/BitwiseNot' );
const BitwiseOr = tim.require( 'Ast/BitwiseOr' );
const BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign' );
const BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift' );
const BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift' );
const BitwiseXor = tim.require( 'Ast/BitwiseXor' );
const BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign' );
const Block = tim.require( 'Ast/Block' );
const Break = tim.require( 'Ast/Break' );
const Call = tim.require( 'Ast/Call' );
const Check = tim.require( 'Ast/Check' );
const Comma = tim.require( 'Ast/Comma' );
const Comment = tim.require( 'Ast/Comment' );
const Condition = tim.require( 'Ast/Condition' );
const ConditionalDot = tim.require( 'Ast/ConditionalDot' );
const Const = tim.require( 'Ast/Const' );
const Context = tim.require( 'Format/Context' );
const Continue = tim.require( 'Ast/Continue' );
const Declaration = tim.require( 'Ast/Declaration' );
const Delete = tim.require( 'Ast/Delete' );
const DestructDecl = tim.require( 'Ast/DestructDecl' );
const Differs = tim.require( 'Ast/Differs' );
const Divide = tim.require( 'Ast/Divide' );
const DivideAssign = tim.require( 'Ast/DivideAssign' );
const DoWhile = tim.require( 'Ast/DoWhile' );
const Dot = tim.require( 'Ast/Dot' );
const Equals = tim.require( 'Ast/Equals' );
const For = tim.require( 'Ast/For' );
const ForIn = tim.require( 'Ast/ForIn' );
const ForOf = tim.require( 'Ast/ForOf' );
const Func = tim.require( 'Ast/Func/Self' );
const Generator = tim.require( 'Ast/Generator' );
const GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual' );
const GreaterThan = tim.require( 'Ast/GreaterThan' );
const If = tim.require( 'Ast/If' );
const Instanceof = tim.require( 'Ast/Instanceof' );
const LessOrEqual = tim.require( 'Ast/LessOrEqual' );
const LessThan = tim.require( 'Ast/LessThan' );
const Let = tim.require( 'Ast/Let' );
const Member = tim.require( 'Ast/Member' );
const Minus = tim.require( 'Ast/Minus' );
const MinusAssign = tim.require( 'Ast/MinusAssign' );
const Multiply = tim.require( 'Ast/Multiply' );
const MultiplyAssign = tim.require( 'Ast/MultiplyAssign' );
const Negate = tim.require( 'Ast/Negate' );
const New = tim.require( 'Ast/New' );
const Not = tim.require( 'Ast/Not' );
const Null = tim.require( 'Ast/Null' );
const NullishCoalescence = tim.require( 'Ast/NullishCoalescence' );
const AstNumber = tim.require( 'Ast/Number' );
const ObjLiteral = tim.require( 'Ast/ObjLiteral' );
const Or = tim.require( 'Ast/Or' );
const Plus = tim.require( 'Ast/Plus' );
const PlusAssign = tim.require( 'Ast/PlusAssign' );
const PostDecrement = tim.require( 'Ast/PostDecrement' );
const PostIncrement = tim.require( 'Ast/PostIncrement' );
const PreDecrement = tim.require( 'Ast/PreDecrement' );
const PreIncrement = tim.require( 'Ast/PreIncrement' );
const Regex = tim.require( 'Ast/Regex' );
const Remainder = tim.require( 'Ast/Remainder' );
const RemainderAssign = tim.require( 'Ast/RemainderAssign' );
const Return = tim.require( 'Ast/Return' );
const Sep = tim.require( 'Ast/Sep' );
const AstString = tim.require( 'Ast/String' );
const Switch = tim.require( 'Ast/Switch' );
const Throw = tim.require( 'Ast/Throw' );
const Try = tim.require( 'Ast/Try' );
const Typeof = tim.require( 'Ast/Typeof' );
const Undefined = tim.require( 'Ast/Undefined' );
const Var = tim.require( 'Ast/Var' );
const VarDec = tim.require( 'Ast/VarDec' );
const While = tim.require( 'Ast/While' );
const Yield = tim.require( 'Ast/Yield' );

/*
| Returns true if 'line' fits in a line.
*/
const fits = ( context, line ) =>
{
	if( !line ) return false;
	return line.length <= MaxLineWidth - context.indent * 4;
};

/*
| Formats a block as file.
*/
def.static.format =
	function( block )
{
	const context = Context.create( );
	if( block.timtype === Block )
	{
		let text = Self._block( context, block, true );
		if( text.charAt( 0 ) === '\n' ) text = text.substr( 1 );
		return text;
	}
	else return Self._statement( context, block );
};

/*
| Formats an array literal.
|
| FUTURE format also inline
*/
def.static._arrayLiteral =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== ArrayLiteral ) throw new Error( );

	if( expr.length === 0 ) return '[ ]';
	const ci = context.Inc;
	let text = '[' + ci.sep;
	let first = true;
	for( let e of expr )
	{
		if( first ) first = false; else text += ',' + ci.sep;
		text += Self._expression( ci, e, ArrayLiteral );
	}
	return text + context.sep + ']';
};

/*
| Formats an arrow function.
*/
def.static._arrowFunction =
	function( context, afunc )
{
	let text = '';
	const args = afunc.args;
	if( args.length === 0 ) text = '( )';
	else
	{
		let first = true;
		text += '( ';
		for( let arg of args )
		{
			if( first ) first = false;
			else text += ', ';

			text += arg;
		}
		text += ' )';
	}
	text += ' => ';
	const body = afunc.body;

	if( body.timtype === Block ) text += Self._block( context, body );
	else text += Self._expression( context, body );

	return text;
};

/*
| Formats an assignment (=,+=,-=,*=,/=).
*/
def.static._assign =
	function( context, assign )
{
	let op;
	switch( assign.timtype )
	{
		case Assign: op = '='; break;
		case BitwiseAndAssign: op = '&='; break;
		case BitwiseOrAssign: op = '|='; break;
		case BitwiseXorAssign: op = '^='; break;
		case DivideAssign: op = '/='; break;
		case MinusAssign: op = '-='; break;
		case MultiplyAssign: op = '*='; break;
		case PlusAssign: op = '+='; break;
		case RemainderAssign: op = '%='; break;
		default: throw new Error( );
	}

	const rcontext = assign.right.timtype === Assign ? context : context.Inc;

	return(
		Self._expression( context, assign.left, assign.timtype )
		+ ' ' + op
		+ rcontext.sep
		+ Self._expression( rcontext, assign.right, assign.timtype )
	);
};

/*
| Formats an await expression.
*/
def.static._await =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Await ) throw new Error( );

	return 'await ' + Self._expression( context, expr.expr, Delete );
};

/*
| Formats a block.
|
| ~context:    the context to format in
| ~block:      the block to format to
| ~noBrackets: omit brackets
*/
def.static._block =
	function( context, block, noBrackets )
{
/**/if( CHECK && block.timtype !== Block ) throw new Error( );

	let blockContext, text;
	if( !noBrackets ) { blockContext = context.Inc; text = '{'; }
	else { blockContext = context; text = ''; }

	for( let a = 0, alen = block.length; a < alen; a++ )
	{
		const sa = a + 1 < alen ? block.get( a + 1 ) : undefined;
		const sb = a > 0 ? block.get( a - 1 ) : undefined;
		const s = block.get( a );
		let sc = s.timtype !== Check ? blockContext : blockContext.Check;
		text += sc.sep;

		let line;
		if( !sc.inline ) line = Self._statement( sc.Inline, s, sb, sa );
		if( !fits( sc, line ) ) line = Self._statement( sc, s, sb, sa );
		text += line;
	}
	if( !noBrackets ) text += context.sep + '}';
	return text;
};

/*
| Formats a boolean literal use.
*/
def.static._boolean = ( context, expr ) => ( expr.boolean ? 'true' : 'false' );

/*
| Formats a break statement.
*/
def.static._break = ( context, statement ) => 'break';

/*
| Formats a call.
*/
def.static._call =
	function( context, call, snuggle )
{
/**/if( CHECK && call.timtype !== Call ) throw new Error( );

	let text = Self._expression( snuggle ? context.Inline : context, call.func, Call );
	const args = call.args;
	if( args.length === 0 ) return text + '( )';

	const subContext = context.Inc;
	text += '(' + subContext.sep;
	let first = true;
	for( let arg of args )
	{
		if( !first ) text += ',' + subContext.sep; else first = false;
		text += Self._expression( subContext, arg );
	}
	return text + context.sep + ')';
};

/*
| Formats a conditional checking code.
*/
def.static._check =
	function( context, check )
{
/**/if( CHECK && !context.check ) throw new Error( );

	return 'if( CHECK )' + context.sep + Self._block( context, check.block );
};

/*
| Formats a comma operator.
*/
def.static._comma =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Comma ) throw new Error( );

	return(
		Self._expression( context, expr.left, Comma )
		+ ',' + context.sep
		+ Self._expression( context, expr.right, Comma )
	);
};

/*
| Formats a comment.
*/
def.static._comment =
	function( context, comment, prev )
{
	if( context.inline ) return false;
	let text = '';
	if( prev ) text += context.sep;
	text += '/*';
	for( let c of comment )
	{
		if( c === '' ) text += context.sep + '|';
		else text += context.sep + '| ' + c;
	}
	text += context.sep + '*/';
	return text;
};

/*
| Formats a condition expression.
| The ? : thingy.
*/
def.static._condition =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Condition ) throw new Error( );

	return(
		Self._expression( context, expr.condition, Condition )
		+ context.sep + '? '
		+ Self._expression( context.Inline, expr.then, Condition )
		+ context.sep + ': '
		+ Self._expression( context.Inline, expr.elsewise, Condition )
	);
};

/*
| Formats a conditional dot.
*/
def.static._conditionalDot =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== ConditionalDot ) throw new Error( );

	return(
		Self._expression( context, expr.expr, ConditionalDot )
		+ '?.' + expr.member
	);
};

/*
| Formats a continue statement.
*/
def.static._continue = ( context, statement ) => 'continue';

/*
| Formats let/constant a variable declaration.
*/
def.static._declare =
	function( context, expr )
{
	let text;
	switch( expr.timtype )
	{
		case Const: text = 'const '; break;
		case Let: text = 'let '; break;
		default: throw new Error( );
	}

	let first = true;
	for( let entry of expr )
	{
		if( first ) first = false; else text += ',' + context.sep;
		switch( entry.timtype )
		{
			case Declaration:
				text += entry.name;
				break;
			case DestructDecl:
				text += Self._objLiteral( context, entry.literal );
				break;
			default:
				throw new Error( );
		}
		if( entry.assign )
		{
			let subContext = context;
			if( entry.assign.timtype !== Assign ) subContext = context.Inc;
			text += ' =' + subContext.sep + Self._expression( subContext, entry.assign );
		}
	}
	return text;
};

/*
| Formats a delete expression.
*/
def.static._delete =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Delete ) throw new Error( );

	return 'delete ' + Self._expression( context, expr.expr, Delete );
};

/*
| Formats a do while loop.
*/
def.static._doWhile =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		'do'
		+ context.sep + Self._statement( context, expr.body )
		+ ci.sep
		+ 'while ('
		+ Self._expression( ci.Inline, expr.condition )
		+ context.sep + ')'
	);
};

/*
| Formats a dualistic operation
*/
def.static._dualOp =
	function( context, expr )
{
	const op = Self._dualOpMap.get( expr.timtype );
	if( !op ) throw new Error( );
	return(
		Self._expression( context, expr.left, expr.timtype )
		+ context.sep
		+ op + ' ' + Self._expression( context, expr.right, expr.timtype )
	);
};

/*
| Maps dual operations to a string.
*/
def.staticLazy._dualOpMap = ( ) =>
{
	const map = new Map( );
	map.set( And,                       '&&'  );
	map.set( BitwiseAnd,                '&'   );
	map.set( BitwiseLeftShift,          '<<'  );
	map.set( BitwiseOr,                 '|'   );
	map.set( BitwiseRightShift,         '>>'  );
	map.set( BitwiseUnsignedRightShift, '>>>' );
	map.set( BitwiseXor,                '^'   );
	map.set( Differs,                   '!==' );
	map.set( Divide,                    '/'   );
	map.set( Equals,                    '===' );
	map.set( GreaterOrEqual,            '>='  );
	map.set( GreaterThan,               '>'   );
	map.set( LessOrEqual,               '<='  );
	map.set( LessThan,                  '<'   );
	map.set( Minus,                     '-'   );
	map.set( Multiply,                  '*'   );
	map.set( NullishCoalescence,        '??'  );
	map.set( Or,                        '||'  );
	map.set( Plus,                      '+'   );
	map.set( Remainder,                 '%'   );
	return map;
};

/*
| Formats a dot.
*/
def.static._dot =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Dot ) throw new Error( );

	return(
		Self._expression( context, expr.expr, Dot )
		+ '.' + expr.member
	);
};

/*
| Formats an expression.
|
| context:  context to be formated in
| expr:     the expression to format
| ptimtype: timtype of parenting expression may be undefined
*/
def.static._expression =
	function( context, expr, ptimtype )
{
	const prec = Self._precTable.get( expr.timtype );
	if( prec === undefined ) throw new Error( expr.timtype.__DEBUG_PATH__ );
	let pprec;
	if( ptimtype )
	{
		pprec = Self._precTable.get( ptimtype );
		if( pprec === undefined ) throw new Error( ptimtype.__DEBUG_PATH__ );
	}

	// FIXME undefined should throw error.
	let bracket = pprec !== undefined && prec < pprec;
	// FIXME take care of left-to-right, right-to-left precedence
	let line;
	if( !context.inline ) line = Self._expression2( context.Inline, expr, bracket );
	if( !fits( context, line ) ) line = Self._expression2( context, expr, bracket );
	return line;
};

/*
| Helper to _expression
*/
def.static._expression2 =
	function( context, expr, bracket )
{
	const formatter = Self._exprFormatter.get( expr.timtype );
	let cinc = bracket ? context.Inc : context;
	return(
		( bracket ? '(' + cinc.sep : '' )
		+ Self[ formatter ]( cinc, expr )
		+ ( bracket ? context.sep + ')' : '' )
	);
};

/*
| Table of all expression formatters.
*/
def.staticLazy._exprFormatter = ( ) =>
	new Map( [
		[ And,                       '_dualOp'         ],
		[ ArrayLiteral,              '_arrayLiteral'   ],
		[ ArrowFunction,             '_arrowFunction'  ],
		[ Assign,                    '_assign'         ],
		[ AstBoolean,                '_boolean'        ],
		[ AstString,                 '_string'         ],
		[ Await,                     '_await'          ],
		[ BitwiseAnd,                '_dualOp'         ],
		[ BitwiseAndAssign,          '_assign'         ],
		[ BitwiseLeftShift,          '_dualOp'         ],
		[ BitwiseNot,                '_preOp'          ],
		[ BitwiseOr,                 '_dualOp'         ],
		[ BitwiseOrAssign,           '_assign'         ],
		[ BitwiseRightShift,         '_dualOp'         ],
		[ BitwiseUnsignedRightShift, '_dualOp'         ],
		[ BitwiseXor,                '_dualOp'         ],
		[ BitwiseXorAssign,          '_assign'         ],
		[ Call,                      '_call'           ],
		[ Comma,                     '_comma'          ],
		[ Condition,                 '_condition'      ],
		[ ConditionalDot,            '_conditionalDot' ],
		[ Const,                     '_declare'        ],
		[ Delete,                    '_delete'         ],
		[ Differs,                   '_dualOp'         ],
		[ Divide,                    '_dualOp'         ],
		[ DivideAssign,              '_assign'         ],
		[ Dot,                       '_dot'            ],
		[ Equals,                    '_dualOp'         ],
		[ Func,                      '_func'           ],
		[ Generator,                 '_func'           ],
		[ GreaterOrEqual,            '_dualOp'         ],
		[ GreaterThan,               '_dualOp'         ],
		[ Instanceof,                '_instanceof'     ],
		[ LessOrEqual,               '_dualOp'         ],
		[ LessThan,                  '_dualOp'         ],
		[ Let,                       '_declare'        ],
		[ Member,                    '_member'         ],
		[ Minus,                     '_dualOp'         ],
		[ MinusAssign,               '_assign'         ],
		[ Multiply,                  '_dualOp'         ],
		[ MultiplyAssign,            '_assign'         ],
		[ Negate,                    '_preOp'          ],
		[ New,                       '_new'            ],
		[ Not,                       '_preOp'          ],
		[ Null,                      '_null'           ],
		[ NullishCoalescence,        '_dualOp'         ],
		[ AstNumber,                 '_number'         ],
		[ ObjLiteral,                '_objLiteral'     ],
		[ Or,                        '_dualOp'         ],
		[ Plus,                      '_dualOp'         ],
		[ PlusAssign,                '_assign'         ],
		[ PostDecrement,             '_postOp'         ],
		[ PostIncrement,             '_postOp'         ],
		[ PreDecrement,              '_preOp'          ],
		[ PreIncrement,              '_preOp'          ],
		[ Regex,                     '_regex'          ],
		[ Remainder,                 '_dualOp'         ],
		[ RemainderAssign,           '_assign'         ],
		[ Typeof,                    '_typeof'         ],
		[ Undefined,                 '_undefined'      ],
		[ Var,                       '_var'            ],
		[ Yield,                     '_yield'          ],
	] );


/*
| Formats a classical for loop.
*/
def.static._for =
	function( context, expr )
{
	const ci = context.Inc;
	let forLine;

	const cil = ci.Inline;
	const init = expr.init ? Self._expression( cil, expr.init ) : '';
	const condition = expr.condition ? Self._expression( cil, expr.condition ) : '';
	const iterate = expr.iterate ? Self._expression( cil, expr.iterate ) : '';

	if( !ci.inline )
	{
		forLine =
			'for('
			+ cil.sep + init + ';' + cil.sep
			+ condition + ';' + cil.sep
			+ iterate + cil.sep + ')';
	}
	if( !fits( context, forLine ) )
	{
		forLine =
			'for('
			+ ci.sep + init + ';' + ci.sep
			+ condition + ';' + ci.sep
			+ iterate + context.sep + ')';
	}
	return forLine + context.sep + Self._block( context, expr.block );
};

/*
| Formats a for-in/for-of loop.
*/
def.static._forInOf =
	function( context, expr )
{
	const iof = expr.timtype === ForIn ? 'in' : 'of';

	return(
		'for( '
		+ ( expr.letVar ? 'let ' : '' )
		+ expr.variable.name
		+ ' ' + iof + ' '
		+ Self._expression( context.Inline, expr.object, Instanceof )
		+ ' )'
		+ context.sep + Self._block( context, expr.block )
	);
};

/*
| Formats a function.
*/
def.static._func =
	function( context, func )
{
	let text = '';
	let head = '';
	if( func.isAsync ) head = 'async ';
	head += 'function';
	if( func.timtype === Generator ) head += '*';
	if( func.name !== undefined ) head += ' ' + func.name;
	const args = func.args;
	if( args.length === 0 ) { text = head + '( )'; }
	else
	{
		let line;
		if( !context.inline )
		{
			line = head + Self._funcArgs( context.Inline, func );
		}
		if( fits( context, line ) ) text += line;
		else text += head + Self._funcArgs( context, func );
	}
	// formats the body one indentation decremented
	const dc = context.Dec;
	return text + dc.sep + Self._block( dc, func.body );
};

/*
| Formts arguments of a function (or generator).
*/
def.static._funcArgs =
	function( context, func )
{
	const args = func.args;
	let text = '(';
	for( let a = 0, alen = args.length; a < alen; a++ )
	{
		const arg = args.get( a );
		const name = arg.name;
		if( !name ) console.log( 'TODO' );
		if( !name ) continue; // TODO remove empty arguments.
		const comma = a + 1 < alen ? ',' : '';
		text +=
			context.Inc.sep
			+ ( arg.rest ? '...' : '' )
			+ ( arg.name || '' )
			+ comma;
	}
	return text + context.sep + ')';
};

/*
| Formats an if statement.
*/
def.static._if =
	function( context, statement )
{
/**/if( CHECK && statement.timtype !== If ) throw new Error( );

	const cond = statement.condition;
	const ci = context.Inc;
	let condition;
	if( !context.inline )
	{
		const expr = Self._expression( ci.Inline, cond );
		if( expr ) condition = 'if( ' + expr + ' )';
	}
	if( !fits( context, condition ) )
	{
		condition = 'if(' + ci.sep + Self._expression( ci, cond ) + context.sep + ')';
	}
	let then = context.sep + Self._block( context, statement.then );
	let elsewise = '';
	if( statement.elsewise )
	{
		elsewise =
			context.sep + 'else'
			+ context.sep + Self._block( context, statement.elsewise );
	}
	return condition + then + elsewise;
};

/*
| Formats an instanceof expression.
*/
def.static._instanceof =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Instanceof ) throw new Error( );

	return(
		Self._expression( context, expr.left, Instanceof )
		+ context.sep + 'instanceof' + context.sep
		+ Self._expression( context, expr.right, Instanceof )
	);
};

/*
| Formats a member.
*/
def.static._member =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Member ) throw new Error( );

	return (
		Self._expression( context, expr.expr, Member )
		+ '[' + context.sep
		+ Self._expression( context.Inc, expr.member )
		+ context.sep + ']'
	);
};

/*
| Formats a new expression.
*/
def.static._new =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== New ) throw new Error( );

	return 'new ' + Self._call( context, expr.call, true );
};

/*
| Formats a null.
*/
def.static._null = ( context, expr ) => 'null';

/*
| Formats a number literal use.
*/
def.static._number = ( context, expr ) => ( '' + expr.number );

/*
| Formats an object literal.
| FUTURE format also inline
*/
def.static._objLiteral =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== ObjLiteral ) throw new Error( );

	if( expr.length === 0 ) return '{ }';
	const ci = context.Inc;
	const cii = ci.Inc;
	let text = '{';
	// FIXME make it a for let and first..
	for( let a = 0, alen = expr.length; a < alen; a++ )
	{
		const key = expr.getKey( a );
		text += ci.sep + key;
		const sexpr = expr.get( key );

		if( sexpr !== Undefined.singleton )
		{
			text += ':' + cii.sep;
			// FUTURE, precTable.Objliteral
			text += Self._expression( cii, expr.get( key ) );
		}
		if( a + 1 < alen ) text += ',';
	}
	return text + context.sep + '}';
};

/*
| Formats a post dec- or increment.
*/
def.static._postOp =
	function( context, expr )
{
	let op;
	switch( expr.timtype )
	{
		case PostDecrement: op = '--'; break;
		case PostIncrement: op = '++'; break;
		default: throw new Error( );
	}
	return Self._expression( context, expr.expr, expr.timtype ) + op;
};

/*
| Expression precedence table.
*/
def.staticLazy._precTable = ( ) =>
	new Map(
	[
		[ Const,                    100 ],
		[ ArrayLiteral,             100 ],
		[ AstBoolean,               100 ],
		[ AstNumber,                100 ],
		[ AstString,                100 ],
		[ Let,                      100 ],
		[ Null,                     100 ],
		[ Undefined,                100 ],
		[ ObjLiteral,               100 ],
		[ Regex,                    100 ],
		[ Var,                      100 ],
		[ ArrowFunction,             99 ],
		[ Member,                    18 ],
		[ ConditionalDot,            18 ],
		[ Dot,                       18 ],
		[ Call,                      18 ],
		[ New,                       17 ],
		[ Func,                      16 ], // ???
		[ Generator,                 16 ], // ???
		[ PostDecrement,             16 ],
		[ PostIncrement,             16 ],
		[ BitwiseNot,                15 ],
		[ Delete,                    15 ],
		[ Negate,                    15 ],
		[ Not,                       15 ],
		[ PreDecrement,              15 ],
		[ PreIncrement,              15 ],
		[ Typeof,                    15 ],
		[ Await,                     15 ],
		[ Divide,                    13 ],
		[ Multiply,                  13 ],
		[ Remainder,                 13 ],
		[ Minus,                     12 ],
		[ Plus,                      12 ],
		[ BitwiseLeftShift,          11 ],
		[ BitwiseUnsignedRightShift, 11 ],
		[ BitwiseRightShift,         11 ],
		[ GreaterOrEqual,            10 ],
		[ GreaterThan,               10 ],
		[ Instanceof,                10 ],
		[ LessOrEqual,               10 ],
		[ LessThan,                  10 ],
		[ Differs,                    9 ],
		[ Equals,                     9 ],
		[ BitwiseAnd,                 8 ],
		[ BitwiseXor,                 7 ],
		[ BitwiseOr,                  6 ],
		[ And,                        5 ],
		[ Or,                         4 ],
		[ NullishCoalescence,         4 ],
		[ Condition,                  3 ],
		[ Assign,                     2 ],
		[ BitwiseAndAssign,           2 ],
		[ BitwiseOrAssign,            2 ],
		[ BitwiseXorAssign,           2 ],
		[ DivideAssign,               2 ],
		[ MinusAssign,                2 ],
		[ MultiplyAssign,             2 ],
		[ PlusAssign,                 2 ],
		[ RemainderAssign,            2 ],
		[ Yield,                      2 ],
		[ Comma,                      1 ],
	] );


/*
| Formats a monoistic operator.
*/
def.static._preOp =
	function( context, expr )
{
	let op;
	switch( expr.timtype )
	{
		case BitwiseNot: op = '~'; break;
		case PreDecrement: op = '--'; break;
		case PreIncrement: op = '++'; break;
		case Negate: op = '-'; break;
		case Not: op = '!'; break;
		default: throw new Error( );
	}

	return op + Self._expression( context, expr.expr, expr.timtype );
};

/*
| Formats a regular expression.
*/
def.static._regex =
	function( context, expr )
{
	return '/' + expr.string + '/' + expr.flags;
};

/*
| Formats a return statement.
*/
def.static._return =
	function( context, statement )
{
/**/if( CHECK && statement.timtype !== Return ) throw new Error( );

	const ci = context.Inc;
	const expr = statement.expr;

	if( !expr ) return 'return';

	if( context.inline )
	{
		return 'return ' + Self._expression( ci, statement.expr );
	}
	else
	{
		return(
			'return('
			+ ci.sep + Self._expression( ci, statement.expr )
			+ context.sep + ')'
		);
	}
};

/*
| Formats a separator.
*/
def.static._sep = function( context ) { return ''; };

/*
| Formats a statement.
|
| ~context: context to be formated in
| ~statement: the statement to be formated
| ~prev: the previous statement (or undefined)
| ~next: the next statement (or undefined)
*/
def.static._statement =
	function( context, statement, prev, next )
{
	let line;
	switch( statement.timtype )
	{
		case Block: line = Self._block( context, statement, false ); break;
		case Break: line = Self._break( context, statement ); break;
		case Check: line = Self._check( context, statement ); break;
		case Comment: line = Self._comment( context, statement, prev ); break;
		case Const: line = Self._declare( context, statement ); break;
		case Continue: line = Self._continue( context, statement ); break;
		case DoWhile: line = Self._doWhile( context, statement ); break;
		case For: line = Self._for( context, statement ); break;
		case ForIn: line = Self._forInOf( context, statement ); break;
		case ForOf: line = Self._forInOf( context, statement ); break;
		case If: line = Self._if( context, statement ); break;
		case Let: line = Self._declare( context, statement ); break;
		case Return: line = Self._return( context, statement ); break;
		case Sep: line = Self._sep( context ); break;
		case Switch: line = Self._switch( context, statement ); break;
		case Throw: line = Self._throw( context, statement ); break;
		case Try: line = Self._try( context, statement ); break;
		case VarDec: line = Self._varDec( context, statement, prev ); break;
		case While: line = Self._while( context, statement ); break;
		default: line = Self._expression( context, statement ); break;
	}
	if( line === false ) return false;
	switch( statement.timtype )
	{
		case And:
		case Assign:
		case AstString:
		case AstBoolean:
		case Await:
		case BitwiseAnd:
		case BitwiseAndAssign:
		case BitwiseOr:
		case BitwiseOrAssign:
		case BitwiseXor:
		case BitwiseXorAssign:
		case Break:
		case Call:
		case Const:
		case Continue:
		case ConditionalDot:
		case Comma:
		case Delete:
		case Divide:
		case DivideAssign:
		case Dot:
		case GreaterOrEqual:
		case GreaterThan:
		case LessOrEqual:
		case LessThan:
		case Let:
		case Member:
		case Minus:
		case MinusAssign:
		case Multiply:
		case MultiplyAssign:
		case Negate:
		case New:
		case Not:
		case AstNumber:
		case Plus:
		case PlusAssign:
		case PostDecrement:
		case PostIncrement:
		case PreDecrement:
		case PreIncrement:
		case Regex:
		case Remainder:
		case RemainderAssign:
		case Return:
		case Throw:
		case Var:
		case Yield:
			return line + ';';
		case Block:
		case Check:
		case Comment:
		case DoWhile:
		case For:
		case ForIn:
		case ForOf:
		case Func:
		case If:
		case Sep:
		case Switch:
		case Try:
		case While:
			return line;
		case VarDec:
			return(
				next?.timtype === VarDec
				? line + ',\n'
				: line + ';\n'
			);
		default: throw new Error( statement.__DEBUG_PATH__);
	}
};

/*
| Formats a string literal.
*/
def.static._string =
	function( context, expr )
{
	return '\'' + expr.string.replaceAll( '\'', '\\\'' ) + '\'';
};

/*
| Formats a switch statement
*/
def.static._switch =
	function( context, _switch )
{
	const caseContext = context.Inc;
	let text =
		'switch( ' + Self._expression( context.Inline, _switch.statement ) + ' )'
		+ context.sep + '{';
	const cases = _switch.cases;
	for( let c of cases )
	{
		const values = c.values;
		for( let v of values )
		{
			text +=
				caseContext.sep + 'case '
				+ Self._expression( caseContext.Inline, v ) + ':';
		}
		text += Self._block( caseContext.Inc, c.block, true );
	}
	if( _switch.defaultCase )
	{
		text +=
			caseContext.sep + 'default :'
			+ Self._block( caseContext.Inc, _switch.defaultCase, true );
	}
	return text + context.sep + '}';
};

/*
| Formats a throw statement.
*/
def.static._throw =
	function( context, statement )
{
/**/if( CHECK && statement.timtype !== Throw ) throw new Error( );

	const ci = context.Inc;
	return(
		'throw ('
		+ ci.sep
		+ Self._expression( context.Inc, statement.expr )
		+ ci.sep
		+ ')'
	);
};

/*
| Formats a try statement.
*/
def.static._try =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		'try'
		+ context.sep + Self._statement( ci, expr.tryStatement )
		+ ci.sep + 'catch( ' + expr.exceptionVar.name + ' )'
		+ context.sep + Self._statement( ci, expr.catchStatement )
	);
};


/*
| Formats a typeof expression.
*/
def.static._typeof =
	function( context, expr )
{
/**/if( CHECK && expr.timtype !== Typeof ) throw new Error( );

	return(
		'typeof('
		+ context.sep
		+ Self._expression( context.Inc, expr.expr, Typeof )
		+ context.sep
		+ ')'
	);
};

/*
| Formats an undefined.
*/
def.static._undefined = ( context, expr ) => 'undefined';

/*
| Formats a variable use.
*/
def.static._var = ( context, expr ) => expr.name;

/*
| Formats a variable declaration.
*/
def.static._varDec =
	function( context, varDec, prev )
{
	// true when this defines a function
	let isFunc = false;
	let text = '';

	if( varDec.assign )
	{
		if( varDec.assign.timtype === Func ) isFunc = true;
		else if(
			varDec.assign.timtype === Assign
			&& varDec.assign.right.timtype === Func
		)
		{
			// TODO allow abitrary amount of assignments
			isFunc = true;
		}
	}

	if( !isFunc )
	{
		if( !prev || prev.timtype !== VarDec )
		{
			if( !context.inline ) text += context.sep + 'var' + '\n';
			else text += 'var ';
		}

		context = context.Inc; // TODO?
		text += context.sep;
		text += varDec.name;
	}
	else
	{
		// functions are not combined in VarDecs
		text = context.sep + 'var ' + varDec.name;
	}

	if( varDec.assign )
	{
		text += ' =' + context.sep;
		if( varDec.assign.timtype !== Assign ) context = context.Inc;
		text += Self._expression( context, varDec.assign );
	}
	return text;
};

/*
| Formats a while loop.
*/
def.static._while =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		'while('
		+ ci.sep
		+ Self._expression( ci.Inline, expr.condition )
		+ context.sep + ')'
		+ context.sep + Self._statement( context, expr.body )
	);
};

/*
| Formats a yield operator.
*/
def.static._yield =
	function( context, expr )
{
	const ci = context.Inc;
	if( context.inline ) return 'yield ' + Self._expression( ci, expr.expr, expr.timtype );
	return(
		'yield('
		+ ci.sep + Self._expression( ci, expr.expr, expr.timtype )
		+ context.sep + ')'
	);
};
