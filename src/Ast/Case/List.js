/*
| Ast; a list of case statements.
*/
'use strict';

def.extend = 'Ast/Base';

// the case statements
def.list = [ 'Ast/Case/Self' ];
