/*
| Case statements in abstract syntax trees.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the statement
	block: { type: 'Ast/Block' },

	// the values of the case
	values: { type: 'Ast/ExprList', defaultValue: 'require("Ast/ExprList").Empty' }
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '';
	for( let e of this.values ) result += 'case ' + recurse( e ) + ': ';
	return result + recurse( this.block );
};

/*
| Adds an expression to the values.
*/
def.proto.$value =
	function( expr )
{
	return this.create( 'values', this.values.append( expr ) );
};
