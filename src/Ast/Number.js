/*
| Ast; a number literal.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes = { number: { type: 'string' } };

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse) { return '' + this.number; };
