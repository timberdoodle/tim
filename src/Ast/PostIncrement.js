/*
| Ast; a post increment (x++).
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to post-increment
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse ) { return '( ' + recurse( this.expr ) + ' )++'; };
