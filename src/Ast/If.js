/*
| Ast; if statement.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	condition: { type: [ '< Ast/Type/Expr' ] },
	then: { type: 'Ast/Block' },
	elsewise: { type: [ 'undefined', 'Ast/Block' ] }
};

const AstBlock = tim.require( 'Ast/Block' );
const Parser = tim.require( 'Parser/Self' );

/*
| Creates an if with the elsewise block set.
|
| ~args: parseables
*/
def.proto.$elsewise =
	function( ...args )
{
	let elsewise = Parser.parseArray( args, 'statement' );
	if( elsewise.timtype !== AstBlock ) elsewise = AstBlock.Empty.append( elsewise );
	return this.create( 'elsewise', elsewise );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'if( '
		+ recurse( this.condition )
		+ ' ) ' + recurse( this.then )
		+ ( this.elsewise ? 'else ' + recurse( this.elsewise ) : '' )
		+ ' '
	);
};
