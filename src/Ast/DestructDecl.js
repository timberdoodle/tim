/*
| Ast; a destructing assignment entry to varDec, let or const.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the object literal to assign to.
	literal: { type: 'Ast/ObjLiteral' },

	// assignment
	assign: { type: [ '< Ast/Type/Expr', 'undefined' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	const assign = this.assign;
	return(
		recurse( this.literal )
		+ ( assign ? ' = ( ' + recurse( assign ) + ' )' : '' )
	);
};
