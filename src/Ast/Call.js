/*
| A call in an abstract syntax tree.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression yielding a function to call
	func: { type: [ '< Ast/Type/Expr' ] },

	// call arguments
	args: { type: [ 'Ast/ExprList' ], defaultValue: 'require("Ast/ExprList").Empty' },
};

const Parser = tim.require( 'Parser/Self' );
const ExprList = tim.require( 'Ast/ExprList' );

/*
| Returns a call with a parameter appended
| ~parseables
*/
def.proto.$arg =
	function( ...args )
{
	const arg = Parser.parseArray( args, 'expr' );
	return this.create( 'args', this.args.append( arg ) );
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const func = this.func.walk( transform );
	const list = [ ];
	for( let arg of this.args )
	{
		list.push( arg.walk( transform ) );
	}
	return transform( this.create( 'func', func, 'args', ExprList.Array( list ) ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '( ' + recurse( this.func ) + ' )';
	if( this.length === 0 ) return result + '( )';
	else
	{
		result += '( ';
		let first = true;
		for( let arg of this.args )
		{
			if( first ) first = false; else result += ', ';
			result += recurse( arg );
		}
		result += ' )';
	}
	return result;
};
