/*
| ast negation (unary minus)
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to negate
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '-( ' + recurse( this.expr ) + ' )';
};
