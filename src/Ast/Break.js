/*
| Ast; const variable declarations.
*/
'use strict';

def.extend = 'Ast/Base';
def.singleton = true;

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'break'; };
