/*
| Ast; bitwise left shift operation.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	left: { type: [ '< Ast/Type/Expr' ] },
	right: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const left = this.left.walk( transform );
	const right = this.right.walk( transform );
	return transform( this.create( 'left', left, 'right', right ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.left ) + ' )' + ' << ( ' + recurse( this.right ) + ' )';
};
