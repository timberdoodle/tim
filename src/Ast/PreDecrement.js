/*
| Ast; a pre decrement (--x operator).
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to pre-decrement
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return '--( ' + recurse( this.expr ) + ' )'; };
