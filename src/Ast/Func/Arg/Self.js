/*
| Ast; a function argument.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// argument comment
	comment: { type: [ 'undefined', 'string' ] },

	// argument name
	name: { type: [ 'undefined', 'string' ] },

	// true if it's a ...arg
	rest: { type: 'boolean', defaultValue: 'false' },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return this.name; };
