/*
| Ast; a list of function arguments.
*/
'use strict';

def.extend = 'Ast/Base';

def.list = [ 'Ast/Func/Arg/Self' ];
