/*
| Ast; a new call.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the constructor call
	call: { type: 'Ast/Call' }
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'new ( ' + recurse( this.call ) + ' )'; };
