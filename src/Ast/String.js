/*
| Ast; a string literal.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes = { string: { type: 'string' } };

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk = function( transform ) { return transform( this ); };

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return '"' + this.string + '"'; };
