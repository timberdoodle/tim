/*
| A call to delete.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to delete
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'delete ( ' + recurse( this.expr ) + ' )'; };
