/*
| Ast; a code block.
*/
'use strict';

def.extend = 'Ast/Base';

def.list = [ '< Ast/Type/Statement', '< Ast/Type/Expr' ];

const Comment = tim.require( 'Ast/Comment' );
const Parser = tim.require( 'Parser/Self' );
const Return = tim.require( 'Ast/Return' );
const Shorthand = tim.require( 'Ast/Shorthand' );

/*
| Returns the block with a parsed statement appended.
*/
def.proto.$ =
	function( ...args )
{
	const ast = Parser.parseArray( args, 'statement' );
	if( ast === undefined ) return this;
	return(
		ast.timtype === Self
		? this.appendList( ast )
		: this.append( ast )
	);
};

/*
| Returns the block with an assignment appended.
*/
def.proto.$assign =
	function( left, right )
{
	return this.append( Shorthand.$assign( left, right ) );
};

/*
| Returns the block with a break statement appended.
*/
def.proto.$break = function( ) { return this.append( Shorthand.$break ); };

/*
| Recreates the block with a call appended.
| ~func
| ~ars
*/
def.proto.$call = function( ...args )
{
	return this.append( Shorthand.$call.apply( Shorthand, args ) );
};

/*
| Returns the block with a check appended.
| ~block
*/
def.proto.$check =
	function( ...args )
{
	return this.append( Shorthand.$check.apply( Shorthand, args ) );
};

/*
| Returns the block with a comment appended.
*/
def.proto.$comment =
	function( ...args )
{
	let header = args[ 0 ];
	// arguments have to be a list of strings otherwise
	if( header.timtype !== Comment ) header = Comment.Array( args );
	return this.append( header );
};

/*
| Returns the block with a const decleration appended.
| ~name,
| ~assign
*/
def.proto.$const =
	function( ...args )
{
	return this.append( Shorthand.$const.apply( Shorthand, args ) );
};

/*
| Returns the block with a continue statement appended.
*/
def.proto.$continue = function( ) { return this.append( Shorthand.$continue ); };

/*
| Returns the block with a delete statement appended.
*/
def.proto.$delete = function( expr) { return this.append( Shorthand.$delete( expr ) ); };

/*
| Returns the block with an if appended.
*/
def.proto.$if =
	function( condition, then, elsewise )
{
	return this.append( Shorthand.$if( condition, then, elsewise ) );
};

/*
| Returns the block with a error throwing appended.
*/
def.proto.$fail =
	function( message )
{
	return this.append( Shorthand.$fail( message ) );
};

/*
| Returns the block with a classical for loop appended.
*/
def.proto.$for =
	function( init, condition, iterate, block )
{
	return this.append( Shorthand.$for( init, condition, iterate, block ) );
};

/*
| Returns the block with a for-in loop appended.
*/
def.proto.$forIn =
	function( variable, object, block )
{
	return this.append( Shorthand.$forIn( variable, object, block ) );
};

/*
| Returns the block with a for-in loop appended.
*/
def.proto.$forInLet =
	function( variable, object, block )
{
	return this.append( Shorthand.$forInLet( variable, object, block ) );
};

/*
| Returns the block with a for-of loop appended.
*/
def.proto.$forOf =
	function( variable, object, block )
{
	return this.append( Shorthand.$forOf( variable, object, block ) );
};

/*
| Returns the block with a for-of loop appended.
*/
def.proto.$forOfLet =
	function( variable, object, block )
{
	return this.append( Shorthand.$forOfLet( variable, object, block ) );
};

/*
| Returns the block with a variable decleration appended.
| ~name   variable name
| ~assign variable assignment
*/
def.proto.$let =
	function( ...args )
{
	return this.append( Shorthand.$let.apply( Shorthand, args ) );
};

/*
| Shorthand for creating new calls.
*/
def.proto.$new = function( call ) { return this.append( Shorthand.$new( call ) ); };

/*
| Returns the block with a plus-assignment appended.
*/
def.proto.$plusAssign =
	function( left, right )
{
	return this.append( Shorthand.$plusAssign( left, right ) );
};

/*
| Returns the block with a term appended.
*/
def.proto.$return =
	function( expr )
{
	if( expr.timtype !== Return ) expr = Shorthand.$return( expr );
	return this.append( expr );
};

/*
| Separator. (just a new line for formatter)
*/
def.lazy.$sep = function( ) { return this.append( Shorthand.$sep ); };

/*
| Returns the block with a variable decleration appended.
*/
def.proto.$varDec =
	function( name, assign )
{
	return this.append( Shorthand.$varDec( name, assign ) );
};

/*
| Returns the block with a classical for loop appended.
*/
def.proto.$while =
	function( condition, body )
{
	return this.append( Shorthand.$while( condition, body ) );
};

/*
| Returns the block with a yield keyword appended.
*/
def.proto.$yield =
	function( expr )
{
	return this.append( Shorthand.$yield( expr ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '{ ';
	for( let s of this ) result += recurse( s ) + '; ';
	return result + '} ';
};
