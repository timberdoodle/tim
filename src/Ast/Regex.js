/*
| Ast; a regular expression
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the regular expression string
	string: { type: 'string' },

	// the regular expression flags
	flags: { type: 'string', defaultValue: '""' },
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk = function( transform ) { return transform( this ); };

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return '/' + this.string + '/' + this.flags; };
