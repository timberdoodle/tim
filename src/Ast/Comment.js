/*
| Ast; a comment.
*/
'use strict';

def.extend = 'Ast/Base';

def.list = [ 'string' ];

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '';
	for( let s of this ) result += '/* ' + s + ' */';
	return result;
};
