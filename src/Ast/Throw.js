/*
| Ast; throw statement.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to throw
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'throw ( ' + recurse( this.expr ) + ' )'; };
