/*
| Ast; a do while loop.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the looped body
	body: { type: 'Ast/Block' },

	// the while condition
	condition: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'do{ '
		+ recurse( this.body )
		+ ' } while( '
		+ recurse( this.condition )
		+ ' ) '
	);
};
