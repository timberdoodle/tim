/*
| Ast; a boolean literal.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes = { boolean: { type: 'boolean' } };

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk = function( transform ) { return transform( this ); };

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return this.boolean ? 'true' : 'false'; };
