/*
| Ast; return statement.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to return
	expr: { type: [ 'undefined', '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'return ( ' + recurse( this.expr ) + ' )'; };
