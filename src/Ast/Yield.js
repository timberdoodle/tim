/*
| Ast; a yield keyword.
*/
'use strict';

def.attributes =
{
	// the expression to yield
	expr: { type: [ '< Ast/Type/Expr' ] }
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'yield ( ' + recurse( this.expr ) + ' )'; };
