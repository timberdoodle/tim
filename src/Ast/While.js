/*
| Ast; a while loop.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the looped body
	// FIXME maybe force block only.
	body: { type: [ 'Ast/Block', '< Ast/Type/Expr' ] },

	// the while condition
	condition: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'while( '
		+ recurse( this.condition )
		+ ' ) ' + recurse( this.body )
		+ ' '
	);
};
