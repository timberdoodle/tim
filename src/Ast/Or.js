/*
| Ast; a logical or.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	left: { type: [ '< Ast/Type/Expr' ] },
	right: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.left ) + ' )' + ' || ( ' + recurse( this.right ) + ' )';
};
