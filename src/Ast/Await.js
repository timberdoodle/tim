/*
| Ast; an await call.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to delete
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'await ( ' + recurse( this.expr ) + ' )'; };
