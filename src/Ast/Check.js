/*
| Ast; optional checks in abstract syntax trees.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the code block',
	block: { type: 'Ast/Block' },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = 'if( CHECK ) { ';
	for( let s of this ) result += recurse( s ) + '; ';
	return result + '} ';
};
