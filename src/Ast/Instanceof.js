/*
| Ast; test if left is an instance of right.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	left: { type: [ '< Ast/Type/Expr' ] },
	right: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.left ) + ' )' + ' instanceof ( ' + recurse( this.right ) + ' )';
};
