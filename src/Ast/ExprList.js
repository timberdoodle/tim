/*
| Ast; a list of expressions.
*/
'use strict';

def.extend = 'Ast/Base';

// the case statements
def.list = [ '< Ast/Type/Expr' ];
