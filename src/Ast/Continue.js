/*
| Ast; continues current loop.
*/
'use strict';

def.extend = 'Ast/Base';
def.singleton = true;

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'continue'; };
