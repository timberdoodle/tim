/*
| Ast; object literal.
*/
'use strict';

def.extend = 'Ast/Base';

def.twig = [ '< Ast/Type/Expr' ];

const Parser = tim.require( 'Parser/Self' );

/*
| Returns an object literal with a key-expr pair added.
*/
def.proto.add =
	function( key, ...parseables )
{
	return this.create( 'twig:add', key, Parser.parseArray( parseables, 'expr' ) );
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
|
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	// TODO actually walk through the objects
	return transform( this );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	if( this.length === 0 ) return '{ }';
	let result = '{ ';
	let first = true;
	for( let key of this.keys )
	{
		if( first ) first = false; else result += ', ';
		const arg = this.get( key );
		result += key + ' : ' + recurse( arg );
	}
	return result + ' }';
};
