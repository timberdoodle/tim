/*
| Ast; variable declarations.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// variable name
	name: { type: 'string' },

	// assignment of variable
	assign: { type: [ '< Ast/Type/Expr', 'undefined' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	const assign = this.assign;
	return this.name + ( assign ? ' = ( ' + recurse( this.assign ) + ' )' : '' );
};
