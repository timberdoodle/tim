/*
| Various shorthands for abstract syntax trees.
*/
'use strict';

def.abstract = true;

const AstAnd = tim.require( 'Ast/And' );
const AstArrayLiteral = tim.require( 'Ast/ArrayLiteral' );
const AstAssign = tim.require( 'Ast/Assign' );
const AstBlock = tim.require( 'Ast/Block' );
const AstBoolean = tim.require( 'Ast/Boolean' );
const AstBreak = tim.require( 'Ast/Break' );
const AstCall = tim.require( 'Ast/Call' );
const AstCheck = tim.require( 'Ast/Check' );
const AstComma = tim.require( 'Ast/Comma' );
const AstComment = tim.require( 'Ast/Comment' );
const AstCondition = tim.require( 'Ast/Condition' );
const AstConst = tim.require( 'Ast/Const' );
const AstContinue = tim.require( 'Ast/Continue' );
const AstDeclaration = tim.require( 'Ast/Declaration' );
const AstDelete = tim.require( 'Ast/Delete' );
const AstDiffers = tim.require( 'Ast/Differs' );
const AstDot = tim.require( 'Ast/Dot' );
const AstEquals = tim.require( 'Ast/Equals' );
const AstFor = tim.require( 'Ast/For' );
const AstForIn = tim.require( 'Ast/ForIn' );
const AstForOf = tim.require( 'Ast/ForOf' );
const AstFunc = tim.require( 'Ast/Func/Self' );
const AstGenerator = tim.require( 'Ast/Generator' );
const AstGreaterThan = tim.require( 'Ast/GreaterThan' );
const AstIf = tim.require( 'Ast/If' );
const AstInstanceOf = tim.require( 'Ast/Instanceof' );
const AstLessThan = tim.require( 'Ast/LessThan' );
const AstLet = tim.require( 'Ast/Let' );
const AstMember = tim.require( 'Ast/Member' );
const AstMultiply = tim.require( 'Ast/Multiply' );
const AstMultiplyAssign = tim.require( 'Ast/MultiplyAssign' );
const AstNew = tim.require( 'Ast/New' );
const AstNot = tim.require( 'Ast/Not' );
const AstNull = tim.require( 'Ast/Null' );
const AstNumber = tim.require( 'Ast/Number' );
const AstObjLiteral = tim.require( 'Ast/ObjLiteral' );
const AstOr = tim.require( 'Ast/Or' );
const AstPlus = tim.require( 'Ast/Plus' );
const AstPlusAssign = tim.require( 'Ast/PlusAssign' );
const AstPreIncrement = tim.require( 'Ast/PreIncrement' );
const AstReturn = tim.require( 'Ast/Return' );
const AstSep = tim.require( 'Ast/Sep' );
const AstString = tim.require( 'Ast/String' );
const AstSwitch = tim.require( 'Ast/Switch' );
const AstThrow = tim.require( 'Ast/Throw' );
const AstTypeof = tim.require( 'Ast/Typeof' );
const AstUndefined = tim.require( 'Ast/Undefined' );
const AstVar = tim.require( 'Ast/Var' );
const AstVarDec = tim.require( 'Ast/VarDec' );
const AstWhile = tim.require( 'Ast/While' );
const AstYield = tim.require( 'Ast/Yield' );

const Parser = tim.require( 'Parser/Self' );

/*
| Ensures argument becomes a block.
*/
const ensureBlock =
	function( arg )
{
	if( arg.timtype === AstBlock ) return arg;
	return AstBlock.Empty.append( Parser.statement( arg ) );
};

/*
| Shorthand for creating ands.
*/
def.static.$and =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			AstAnd.create(
				'left', Parser.expr( left ),
				'right', Parser.expr( right )
			)
		);
		return Self.$and.apply( this, args );
	}
	return AstAnd.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating blocks.
*/
def.staticLazy.$array = ( ) => AstArrayLiteral.Empty;

/*
| Shorthand for creating assignments.
*/
def.static.$assign =
	function( left, right )
{
	return(
		AstAssign.create(
			'left', Parser.expr( left ),
			'right', Parser.expr( right )
		)
	);
};

/*
| Shorthand for creating blocks.
*/
def.staticLazy.$block = ( ) => AstBlock.Empty;

/*
| Shorthand for ast break.
*/
def.staticLazy.$break = ( ) => AstBreak.singleton;

/*
| Shorthand for creating calls.
|
| ~func: the function to call
| ~..args: arguments of the call
*/
def.static.$call =
	function( func, ...args )
{
	let call = AstCall.create( 'func', Parser.expr( func ) );
	for( let arg of args ) call = call.$arg( arg );
	return call;
};

/*
| Shorthand for creating Ast check blocks.
*/
def.static.$check =
	function( arg )
{
	const Ast = Parser.statement.apply( Parser, arguments );
	return AstCheck.create( 'block', ensureBlock( Ast ) );
};

/*
| Shorthand for creating comma operators
*/
def.static.$comma =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			AstComma.create(
				'left', Parser.expr( left ),
				'right', Parser.expr( right )
			)
		);
		return Self.$comma.apply( this, args );
	}
	return(
		AstComma.create(
			'left', Parser.expr( left ),
			'right', Parser.expr( right )
		)
	);
};

/*
| Shorthand for creating comments
*/
def.static.$comment =
	function( ...args )
{
	return AstComment.create( 'list:init', Array.prototype.slice.call( args ) );
};

/*
| Shorthand for creating conditions.
*/
def.static.$condition =
	function( condition, then, elsewise )
{
	return(
		AstCondition.create(
			'condition', Parser.expr( condition ),
			'then', Parser.expr( then ),
			'elsewise', Parser.expr( elsewise )
		)
	);
};

/*
| Shorthand for let variable declerations.
|
| ~name:   variable name
| ~assign: variable assignment
*/
def.static.$const =
	function( name, assign )
{
	return(
		AstConst.create(
			'list:init',
			[ AstDeclaration.create( 'name', name, 'assign', assign && Parser.expr( assign ) ) ]
		)
	);
};

/*
| Shorthand for Ast continue.
*/
def.staticLazy.$continue = ( ) => AstContinue.singleton;

/*
| Shorthand for creating differs.
*/
def.static.$differs =
	function( left, right )
{
	return AstDiffers.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating delete calls.
*/
def.static.$delete =
	function( expr )
{
	return AstDelete.create( 'expr', Parser.expr( expr ) );
};

/*
| Shorthand for creating dots.
*/
def.static.$dot = ( expr, member ) => AstDot.create( 'expr', expr, 'member', member );

/*
| Shorthand for creating equals.
*/
def.static.$equals =
	function( left, right )
{
	return AstEquals.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Forward general expression parsing.
*/
def.static.$expr =
	function( )
{
	return Parser.expr.apply( Parser, arguments );
};

/*
| Shorthand for 'false' literals.
*/
def.staticLazy.$false = ( ) => AstBoolean.create( 'boolean', false );

/*
| Shorthand for Ast code that throws a fail.
*/
def.static.$fail =
	function( message )
{
	let call = AstCall.create( 'func', AstVar.create( 'name', 'Error' ) );
	if( message )
	{
		if( typeof( message ) === 'string' ) message = AstString.create( 'string', message );
		call = call.$arg( message );
	}
	return AstThrow.create( 'expr', AstNew.create( 'call', call ) );
};

/*
| Shorthand for creating less-than comparisons.
*/
def.static.$lessThan =
	function( left, right )
{
	return AstLessThan.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for let variable declerations.
|
| name:   variable name
| assign: variable assignment
*/
def.static.$let =
	function( name, assign )
{
	return(
		AstLet.Elements(
			AstDeclaration.create( 'name', name, 'assign', assign && Parser.expr( assign ) )
		)
	);
};

/*
| Shorthand for creating greater-than comparisons.
*/
def.static.$greaterThan =
	function( left, right )
{
	return AstGreaterThan.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating ifs.
*/
def.static.$if =
	function( condition, then, elsewise )
{
	return(
		AstIf.create(
			'condition', Parser.expr( condition ),
			'then', ensureBlock( then ),
			'elsewise', elsewise && ensureBlock( elsewise )
		)
	);
};

/*
| Shorthand for creating for loops.
*/
def.static.$for =
	function( init, condition, iterate, block )
{
	return(
		AstFor.create(
			'init', Parser.statement( init ),
			'condition', Parser.expr( condition ),
			'iterate', Parser.expr( iterate ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-in loops.
*/
def.static.$forIn =
	function( variable, object, block )
{
	return(
		AstForIn.create(
			'variable', Parser.expr( variable ),
			'letVar', false,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-in loops with 'let' for variable.
*/
def.static.$forInLet =
	function( variable, object, block )
{
	return(
		AstForIn.create(
			'variable', Parser.expr( variable ),
			'letVar', true,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-of loops.
*/
def.static.$forOf =
	function( variable, object, block )
{
	return(
		AstForOf.create(
			'variable', Parser.expr( variable ),
			'letVar', false,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-of loops with 'let' for variable.
*/
def.static.$forOfLet =
	function( variable, object, block )
{
	return(
		AstForOf.create(
			'variable', Parser.expr( variable ),
			'letVar', true,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating functions.
*/
def.static.$func =
	function( arg )
{
	return AstFunc.create( 'body', ensureBlock( arg ) );
};

/*
| Shorthand for creating generator functions.
*/
def.static.$generator =
	function( arg )
{
	return AstGenerator.create( 'body', ensureBlock( arg ) );
};

/*
| Shorthand for creating instanceof expressions.
*/
def.static.$instanceof =
	( left, right ) =>
	AstInstanceOf.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );

/*
| Shorthand for creating members.
*/
def.static.$member =
	function( expr, member )
{
	return AstMember.create( 'expr', expr, 'member', member );
};

/*
| Shorthand for creating multiplies.
*/
def.static.$multiply =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			AstMultiply.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.$multiply.apply( this, args );
	}
	return AstMultiply.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating multiply-assignments.
*/
def.static.$multiplyAssign =
	function( left, right )
{
	return AstMultiplyAssign.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating new calls.
*/
def.static.$new = ( call) => AstNew.create( 'call', call );

/*
| Shorthand for creating negations.
*/
def.static.$not = ( expr) => AstNot.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for Ast nulls.
*/
def.staticLazy.$null = ( ) => AstNull.singleton;

/*
| Shorthand for creating number literals.
*/
def.static.$number = ( number ) => AstNumber.create( 'number', '' + number );

/*
| Shorthand for creating object literals.
*/
def.static.$objLiteral = ( ) => AstObjLiteral.create( );

/*
| Shorthand for creating ors.
*/
def.static.$or =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice( 0, 2,
			AstOr.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.$or.apply( this, args );
	}
	return AstOr.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating pluses.
*/
def.static.$plus =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			AstPlus.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.$plus.apply( this, args );
	}
	return AstPlus.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating plus-assignments.
*/
def.static.$plusAssign =
	( left, right ) =>
	AstPlusAssign.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );

/*
| Shorthand for creating pre-increments.
*/
def.static.$preIncrement =
	function( expr )
{
	expr = Parser.expr( expr );
	return AstPreIncrement.create( 'expr', expr );
};

/*
| Shorthand for creating a return statement
*/
def.static.$return = ( expr ) => AstReturn.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for Ast separations.
*/
def.staticLazy.$sep = ( ) => AstSep.singleton;

/*
| Shorthand for creating string literals.
*/
def.static.$string = ( string ) => AstString.create( 'string', string );

/*
| Shorthand for creating switch statements.
*/
def.static.$switch = ( statement ) =>
	AstSwitch.create( 'statement', Parser.statement( statement ) );

/*
| Shorthand for 'true' literals.
*/
def.staticLazy.$true = ( ) => AstBoolean.create( 'boolean', true );

/*
| Shorthand for creating typeofs.
*/
def.static.$typeof = ( expr ) => AstTypeof.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for 'undefined'
*/
def.staticLazy.$undefined = ( ) => AstUndefined.singleton;

/*
| Shorthand for creating variable uses.
*/
def.static.$var = ( name ) => AstVar.create( 'name', name );

/*
| Shorthand for variable declerations.
|
| name:   variable name
| assign: variable assignment
*/
def.static.$varDec =
	function( name, assign )
{
	return(
		AstVarDec.create(
			'name', name,
			'assign', arguments.length > 1 ? Parser.expr( assign ) : undefined
		)
	);
};

/*
| Shorthand for creating for loops.
*/
def.static.$while =
	function( condition, body )
{
	return(
		AstWhile.create(
			'condition', Parser.expr( condition ),
			'body', ensureBlock( body )
		)
	);
};

/*
| Shorthand for creating yield keywords.
*/
def.static.$yield = ( expr ) => AstYield.create( 'expr', Parser.expr( expr ) );
