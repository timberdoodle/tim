/*
| Ast; a typeof of an expression.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to get the type of
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'typeof( ' + recurse( this.expr ) + ' )'; };
