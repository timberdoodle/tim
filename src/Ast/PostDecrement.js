/*
| Ast; post decrement (x-- operator)
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the expression to post-decrement
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return '( ' + recurse( this.expr ) + ' )--'; };
