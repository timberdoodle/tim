/*
| Ast; switch statements.
*/
'use strict';

def.extend = 'Ast/Base';

def.attributes =
{
	// the statement expression
	statement: { type: [ '< /Ast/Type/Expr' ] },

	// cases
	cases: { type: 'Ast/Case/List', defaultValue: 'require("Ast/Case/List").Empty' },

	// the default block
	defaultCase: { type: [ 'undefined', 'Ast/Block' ] },
};

const AstBlock = tim.require( 'Ast/Block' );
const AstCase = tim.require( 'Ast/Case/Self' );
const ExprList = tim.require( 'Ast/ExprList' );
const Parser = tim.require( 'Parser/Self' );

/*
| Shortcut for appending a case to this switch.
| ~coc: case_or_condition
*/
def.proto.$case =
	function( coc, ...parseables )
{
	let _case;
	if( coc.timtype !== AstCase )
	{
		let block = Parser.parseArray( parseables, 'statement' );
		if( block.timtype !== AstBlock ) block = AstBlock.Empty.append( block );
		const value = Parser.statement( coc );
		_case = AstCase.create( 'block', block, 'values', ExprList.Elements( value ) );
	}
	else
	{
		_case = coc;
	}
	return this.create( 'cases', this.cases.append( _case ) );
};

/*
| Shortcut for setting the default case.
*/
def.proto.$default =
	function( ...parseables )
{
	let block = Parser.parseArray( parseables, 'statement' );
	if( block.timtype !== AstBlock ) block = AstBlock.Empty.append( block );
	return this.create( 'defaultCase', block );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let txt = 'switch( ' + recurse( this.statement ) + ') {';
	const cases = this.cases;
	for( let c of cases ) txt += recurse( c );
	const defaultCase = this.defaultCase;
	if( defaultCase ) txt += recurse( defaultCase );
	return txt += '}';
};
