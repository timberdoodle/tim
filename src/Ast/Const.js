/*
| Ast; constant variable declarations.
*/
'use strict';

def.extend = 'Ast/Base';

def.list = [ 'Ast/Declaration', 'Ast/DestructDecl' ];

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = 'const ';
	let first = true;
	for( let e of this )
	{
		if( first ) first = false; else result += ', ';
		result += recurse( e );
	}
	return result;
};
