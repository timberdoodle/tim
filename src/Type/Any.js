/*
| Generic interface for all types.
*/
'use strict';

def.abstract = true;

const Package = tim.require( 'Spec/Package' );
const TypeArbitrary = tim.require( 'Type/Arbitrary' );
const TypeBoolean = tim.require( 'Type/Boolean' );
const TypeDate = tim.require( 'Type/Date' );
const TypeFunction = tim.require( 'Type/Function' );
const TypeInteger = tim.require( 'Type/Integer' );
const TypeNull = tim.require( 'Type/Null' );
const TypeNumber = tim.require( 'Type/Number' );
const TypeProtean = tim.require( 'Type/Protean' );
const TypeString = tim.require( 'Type/String' );
const TypeTim = tim.require( 'Type/Tim' );
const TypeUndefined = tim.require( 'Type/Undefined' );

/*
| Create the type from a string specifier.
|
| ~str: type string to create from
| ~basePackage: base package
*/
def.static.FromString =
	function( str, basePackage )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( str ) !== 'string' ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/}

	switch( str )
	{
		case 'arbitrary': return TypeArbitrary.singleton;
		case 'boolean': return TypeBoolean.singleton;
		case 'date': return TypeDate.singleton;
		case 'integer': return TypeInteger.singleton;
		case 'function': return TypeFunction.singleton;
		case 'null': return TypeNull.singleton;
		case 'number': return TypeNumber.singleton;
		case 'protean': return TypeProtean.singleton;
		case 'string': return TypeString.singleton;
		case 'undefined': return TypeUndefined.singleton;
		default:
			// TODO test primitive by letter case
			return TypeTim.FromString( str, basePackage );
	}
};
