/*
| A tim as type.
|
| This is either a relative path pointer
| or pointer to an imported tim.
*/
'use strict';

def.attributes =
{
	// trace to the spec for the type.
	trace: { type: 'Trace/Self' },
};

const Package = tim.require( 'Spec/Package' );
const Shorthand = tim.require( 'Ast/Shorthand' );
const TraceSpec = tim.require( 'Spec/Trace/Root' );

/*
| Create the type from a string.
|
| ~string: string to create from
| ~basePackage: base package
*/
def.static.FromString =
	function( string, basePackage )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( string ) !== 'string' ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/}

	return Self.create( 'trace', TraceSpec.FromString( string, basePackage, 'spec' ) );
};

/*
| Returns if equalness of this object must be used by a
| .equals( ) call, may have an .equals( ) func or never
| has a .equals( ) call and equalness is simply to be
| determined by '===' operator.
*/
def.proto.equalsConvention = 'must';

/*
| This type does not reference a primitive.
*/
def.proto.isPrimitive = false;

/*
| A require statement used to import this type
|
| ~baseTrace: base trace to import from
*/
def.proto.require =
	function( baseTrace )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const trace = this.trace;
	let s = 'tim.require( "';
	if( baseTrace.get( 1 ).key !== trace.get( 1 ).key )
	{
		// this is an import
		s += trace.get( 1 ).key + ':' + trace.get( 2 ).key;
	}
	else { s += trace.get( 2 ).key; }

	return s + '" )';
};

/*
| The spec of the timtype.
*/
def.lazy.spec =
	function( )
{
	return tim.packages.loadSpecSync( this.trace );
};

/*
| The timtype as imported varname
*/
def.lazy.varname =
	function( )
{
	const trace = this.trace;
	const name = 'tt_' + trace.get( 1 ).key + '_' + trace.get( 2 ).key.replace( /\//g, '_' );
/**/if( CHECK && !name.endsWith( '.js' ) ) throw new Error( );
	// removes the .js
	return name.substr( 0, name.length - 3 );
};

/*
| This varname as ast var.
*/
def.lazy.$varname = function( ) { return Shorthand.$var( this.varname ); };
