/*
| Referencing the date type.
*/
'use strict';

def.singleton = true;
def.proto.equalsConvention = 'mustnot';
def.proto.isPrimitive = true;
