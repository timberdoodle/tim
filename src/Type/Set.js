/*
| A set of types.
*/
'use strict';

def.set = [ '< Type/Types' ];

const fs = require( 'fs' );

const Package = tim.require( 'Spec/Package' );
const Parser = tim.require( 'Parser/Self' );
const TraceSpec = tim.require( 'Spec/Trace/Root' );
const TypeAny = tim.require( 'Type/Any' );

/*
| Creates a type set from an array of type strings.
|
| ~array: array to create from
| ~baseTrace:  base trace to base relative types on
*/
def.static.Array =
	function( array, basePackage )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( !Array.isArray( array ) ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/}

	const types = new Set( );
	for( let entry of array )
	{
		if( entry[ 0 ] === '<' )
		{
			const tbase =
				TraceSpec.FromString(
					entry.substr( 1, entry.length, types ).trim( ),
					basePackage, 'types'
				);
			const tBasePackage = tim.packages.get( tbase.get( 1 ) .key );
			const lines =
				( fs.readFileSync( tim.packages.getFilename( tbase ) ) + '' )
				.split( '\n' );
			for( let line of lines )
			{
				line = line.trim( );
				if( line === '' || line[ 0 ] === '#' ) continue;
				types.add( TypeAny.FromString( line, tBasePackage ) );
			}
		}
		else types.add( TypeAny.FromString( entry, basePackage ) );
	}
	return Self.create( 'set:init', types );
};

/*
| Returns if equalness of this object must be used by a
| .equals( ) call, may have an .equals( ) func or never
| has a .equals( ) call and equalness is simply to be
| determined by '===' operator.
*/
def.lazy.equalsConvention =
	function( )
{
	let first = true;
	let ec;
	for( let type of this )
	{
		if( first )
		{
			first = false;
			ec = type.equalsConvention;
			if( ec === 'can' ) return 'can';
			continue;
		}
		const iec = type.equalsConvention;
		if( iec === 'can' ) return 'can';
		switch( ec )
		{
			case 'must' :
				switch( iec )
				{
					case 'must' : continue;
					case 'mustnot' : return 'can';
					default : throw new Error( );
				}
			case 'mustnot' :
				switch( iec )
				{
					case 'must' : return 'can';
					case 'mustnot' : continue;
					default : throw new Error( );
				}
			default : throw new Error( );
		}
	}
	return ec;
};

/*
| Returns the composition types of this type set.
|
| The result can be:
|   'empty':      the set is empty
|   'primitives': only primitives
|   'mixed':      primitves and tims
|   'tims':       only tims
*/
def.lazy.composition =
	function( )
{
	let s;
	for( let type of this )
	{
		if( type.isPrimitive )
		{
			if( !s ) s = 'primitives';
			else if( s === 'tims' ) return 'mixed';
		}
		else
		{
			if( !s ) s = 'tims';
			if( s === 'primitives' ) return 'mixed';
		}
	}
	return s || 'empty';
};

/*
| Returns the jsonfy code for a 'val' having this type set
*/
def.lazyFunc.jsonfyCode =
	function( val )
{
	switch( this.composition )
	{
		case 'primitives':
			return Parser.expr( 'JSON.stringify(', val, ')' );
		case 'tims':
			return Parser.expr( val, '.jsonfy( iSpacing )' );
		case 'mixed':
			return Parser.expr(
				'typeof(', val, ') === "object"',
				'?', val, '.jsonfy( iSpacing )',
				': JSON.stringify(', val, ')'
			);
		default: throw new Error( );
	}
};
