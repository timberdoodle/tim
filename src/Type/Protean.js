/*
| Referencing the non-type "protean" for any mutable
| objects.
*/
'use strict';

def.singleton = true;
def.proto.equalsConvention = 'mustnot';
def.proto.isPrimitive = true;
