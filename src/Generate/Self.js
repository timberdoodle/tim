/*
| Generates timcode from a tim definition.
*/
'use strict';

def.attributes =
{
	// the spec
	_spec: { type: 'Spec/Self' },
};

// remap interning map after this many collisions
const icollisionsRemap = '1024';

const AstVar = tim.require( 'Ast/Var' );
const TypeArbitrary = tim.require( 'Type/Arbitrary' );
const TypeBoolean = tim.require( 'Type/Boolean' );
const TypeDate = tim.require( 'Type/Date' );
const TypeFunction = tim.require( 'Type/Function' );
const TypeInteger = tim.require( 'Type/Integer' );
const TypeNull = tim.require( 'Type/Null' );
const TypeNumber = tim.require( 'Type/Number' );
const TypeProtean = tim.require( 'Type/Protean' );
const TypeSet = tim.require( 'Type/Set' );
const TypeString = tim.require( 'Type/String' );
const TypeTim = tim.require( 'Type/Tim' );
const TypeUndefined = tim.require( 'Type/Undefined' );
const Parser = tim.require( 'Parser/Self' );
const Shorthand = tim.require( 'Ast/Shorthand' );
const StringList = tim.require( 'string/list' );

/*
| Shorthanding Shorthands.
*/
let shorthandInit = false;
let $;
let $array;
let $expr;
let $and;
let $block;
let $comma;
let $func;
let $generator;
let $if;
let $number;
let $objLiteral;
let $or;
let $string;
let $switch;
let $undefined;

/*
| Type singletons
*/
let tsUndefined;
let tsNull;

/*
| Generates the abstract syntax tree for a timspec.
|
| ~spec: the spec to create timcode for
| ~module: the module relative to which types are
*/
def.static.generate =
	function( spec )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( !shorthandInit )
	{
		shorthandInit = true;
		$ = Parser.statement;
		$and = Shorthand.$and;
		$block = Shorthand.$block;
		$array = Shorthand.$array;
		$comma = Shorthand.$comma;
		$expr = Parser.expr;
		$func = Shorthand.$func;
		$generator = Shorthand.$generator;
		$if = Shorthand.$if;
		$number = Shorthand.$number;
		$objLiteral = Shorthand.$objLiteral;
		$or = Shorthand.$or;
		$string = Shorthand.$string;
		$switch = Shorthand.$switch;
		$undefined = Shorthand.$undefined;
		tsUndefined = TypeUndefined.singleton;
		tsNull = TypeNull.singleton;
	}
	return Self.create( '_spec', spec )._ast;
};

/*
| Generates the alike test(s).
*/
def.lazy._alike =
	function( )
{
	const spec = this._spec;
	if( spec.collectionType !== 'table' ) throw new Error( );
	const alikeList = Object.keys( spec.alike ).sort( );
	let cond;
	let result = $block;
	for( let alikeName of alikeList )
	{
		if( alikeName === '__hash' ) continue;
		const ignores = spec.alike[ alikeName ].ignores;
		result = result.$comment( 'Tests partial equality.' );
		let block =
			$block
			.$( 'if( this === obj ) return true;' )
			.$( 'if( !obj ) return false;' );
		const attributes = this._spec.attributes;
		for( let name of attributes.keys )
		{
			const attr = attributes.get( name );
			if( ignores[ name ] ) continue;
			const ceq =
				this._attributeEquals(
					name,
					$( 'this.', attr.assign ),
					$( 'obj.', attr.assign ),
					'equals'
				);
			cond = cond ? $( cond, '&&', ceq ) : ceq;
		}
		block = block.$( 'return', cond );
		result =
			result
			.$assign(
				$( 'prototype.', alikeName ),
				$func( block ).$arg( 'obj', 'object to compare to' )
			);
	}
	return result;
};

/*
| Executes the generator, returns an ast tree.
*/
def.lazy._ast =
	function( )
{
	const spec = this._spec;
	let code =
		$block
		.$comment(
			'This is an auto generated file.',
			'Editing this might be rather futile.'
		)
		.$( '"use strict"' )
		.$sep
		.$( this._requires );
	if( !spec.abstract ) code = code.$( this._iMapAndCache );
	if( !spec.abstract ) code = code.$( this._constructor );
	if( spec.singleton ) code = code.$( this._singleton );
	if( !spec.abstract )
	{
		code = code.$( this._creator );
		if( spec.json && !spec.customFromJson ) code = code.$( this._fromJsonCreator );
		code = code.$( this._reflection );
		code = code.$( this._timProto );
		if( spec.json ) code = code.$( this._jsonfy );
	}
	if( typeof( spec.json ) === 'string' ) code = code.$( this._jsonType );
	if( spec.alike ) code = code.$( this._alike );
	return code;
};

/*
| Generates the equals condition for an attribute.
|
| ~name: attribute name
| ~le: this value expression
| ~re: other value expression
| ~eqFuncName: the equals function name to call
*/
def.proto._attributeEquals =
	function( name, le, re, eqFuncName )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	const attributes = this._spec.attributes;
	const attr = attributes.get( name );
	const allowsNull = attr.types.timtype === TypeSet && attr.types.has( tsNull );
	const allowsUndefined = attr.types.timtype === TypeSet && attr.types.has( tsUndefined );
	const ceq = $( le, ' === ', re );
	// current test
	let pc;
	// next test (to be appended on current)
	let pn;
	switch( attr.types.equalsConvention )
	{
		case 'mustnot':
			return ceq;
		case 'can':
		case 'must':
			if( allowsNull ) pc = $( le, ' !== null' );
			if( allowsUndefined )
			{
				pn = $( le, ' !== undefined' );
				pc = pc ? $( pc, '&&', pn ) : pn;
			}

			if( eqFuncName === 'equals' ) pn = $( le, '===', re );
			else pn = $( le, '.', eqFuncName, '(', re, ')' );

			if( attr.types.equalsConvention === 'can' )
			{
				pn = $( le, '.timtype', '&&', pn );
			}
			pc = pc ? $( pc, '&&', pn ) : pn;
			return $( ceq, '||', pc );
		default: throw new Error( );
	}
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let block = $block;
	const spec = this._spec;
	if( spec.hasLazy ) block = block.$( 'this.__lazy = { }' );
	if( !spec.abstract ) block = block.$( 'this.__hash = hash' );
	const attributes = spec.attributes;
	// assigns the variables
	for( let attr of attributes )
	{
		const assign = $( 'this.', attr.assign, ' = ', attr.varRef );
		block = block.append( assign );
	}

	switch( spec.collectionType )
	{
		case 'group':
			block = block
				.$( 'this._group = group' )
				.$( 'this.keys = keys' )
				.$( 'Object.freeze( this, group, keys )' );
			break;
		case 'list':
			block = block
				.$( 'this._list = list' )
				.$( 'Object.freeze( this, list )' );
			break;
		case 'set':
			block = block
				.$( 'this._set = set' )
				.$( 'Object.freeze( this, set )' );
			break;
		case 'table':
			block = block.$( 'Object.freeze( this )' );
			break;
		case 'twig':
			block = block
				.$( 'this._twig = twig' )
				.$( 'this.keys = keys' )
				.$( 'Object.freeze( this, twig, keys )' );
			break;
		default: throw new Error( );
	}
	// FUTURE force freezing date attributes
	// calls init checker
	if( spec.check ) block = block.$check( 'this._check( )' );
	let cf = $func( block );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'group': cf = cf.$arg( 'group', 'group' ); break;
			case 'groupDup':
				cf = cf.$arg( 'groupDup', 'true if group is already been duplicated' );
				break;
			case 'hash': cf = cf.$arg( 'hash', 'hash value' ); break;
			case 'keys': cf = cf.$arg( 'keys', 'twig key ranks' ); break;
			case 'list': cf = cf.$arg( 'list', 'list' ); break;
			case 'listDup':
				cf = cf.$arg( 'listDup', 'true if list is already been duplicated' );
				break;
			case 'set': cf = cf.$arg( 'set', 'set' ); break;
			case 'mapDup':
				cf = cf.$arg( 'setDup', 'true if set is already been duplicated' );
				break;
			case 'twig': cf = cf.$arg( 'twig', 'twig' ); break;
			case 'twigDup':
				cf = cf.$arg( 'twigDup', 'true if twig is already been duplicated' );
				break;
			default:
				cf = cf.$arg( name );
				break;
		}
	}
	block =
		$block
		.$comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.$comment( 'In case of checking all unknown access is to be trapped.' )
		.$check(
			$block
			.$( 'const Trap = ', $func( $block ) )
			.$( 'Trap.prototype = tim.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.$comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' );

	if( !spec.abstract ) block = block.$( 'Self.__hash = ', $number( spec.hash ) );
	const trace = spec.trace;
	if( trace )
	{
		block =
			block
			.$comment( 'Reflection for debugging.' )
			.$check(
				'Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ =',
					$string( trace.get( 1 ).key + '/' + trace.get( 2 ).key )
			);
	}
	return block;
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	const spec = this._spec;
	const attributes = spec.attributes;
	const aKeys = attributes.keys;
	const constructorList = [ ];
	if( !spec.abstract ) constructorList.push( 'hash' );
	switch( spec.collectionType )
	{
		case 'group': constructorList.push( 'group', 'keys' ); break;
		case 'list': constructorList.push( 'list' ); break;
		case 'set': constructorList.push( 'set' ); break;
		case 'table': break;
		case 'twig': constructorList.push( 'keys', 'twig' ); break;
		default: throw new Error( );
	}
	for( let key of aKeys ) constructorList.push( 'v_' + key );
	return StringList.Array( constructorList );
};

/*
| Generates the creator.
*/
def.lazy._creator =
	function( )
{
	const spec = this._spec;
	let block =
		$block
		.$( this._creatorVariables )
		.$( this._creatorInheritanceReceiver )
		.$( this._creatorFreeStringsParser )
		.$( this._creatorDefaults )
		.$( this._creatorChecks( false ) )
		.$( this._creatorIntern( false ) );
	const optimization = this._creatorInheritOptimization;
	block = block.$( this._creatorReturn( optimization ) );
	const creator = $func( block ).$arg( 'args', 'free strings', '...' );
	return(
		$block
		.$comment( 'Creates a new object.' )
		.$(
			'Self.', spec.creator, ' = prototype.', spec.creator, ' = ', creator
		)
	);
};

/*
| Generates the creators checks.
|
| ~json: do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	const spec = this._spec;
	let check = $block;
	for( let attr of spec.attributes )
	{
		if( json && !attr.json ) continue;
		if( attr.types.timtype === TypeProtean ) continue;
		const tcheck = this._typeCheckFailCondition( attr.varRef, attr.types );
		if( tcheck ) check = check.$( 'if(', tcheck, ') throw new Error( ); ' );
	}

	switch( spec.collectionType )
	{
		case 'group':
			check =
				check
				// for of loop in js tables not possible
				.$( 'for( let k in group )',
					$block
					.$( 'const o = group[ k ]' )
					.$( 'if(',
						this._typeCheckFailCondition( $( 'o' ), spec.items ),
						') throw new Error( );'
					)
				);
			break;
		case 'list':
			check =
				check
				.$( 'for( let o of list )',
					$block
					.$( 'if(',
						this._typeCheckFailCondition( $( 'o' ), spec.items ),
						') throw new Error( );'
					)
				);
			break;
		case 'set':
			check =
				check
				.$(
					'for( let v of set )',
					$block
					.$( 'if( ',
						this._typeCheckFailCondition( $( 'v' ), spec.items ),
						') throw new Error( );'
					)
				);
			break;
		case 'table':
			break;
		case 'twig':
			// FUTURE check if ranks and twig keys match
			check =
				check
				.$(
					'for( let key of keys )',
					$block
					.$( 'const o = twig[ key ]' )
					.$( 'if( ',
						this._typeCheckFailCondition( $( 'o' ), spec.items ),
						') throw new Error( );'
					)
				);
			break;
		default: throw new Error( );
	}

	return(
		json
		? check
		: ( check.length > 0 ? $block.$check( check ) : $block )
	);
};

/*
| Generates the creators default values
*/
def.lazy._creatorDefaults =
	function( )
{
	let result = $block;
	for( let attr of this._spec.attributes )
	{
		if( attr.defaultValue !== undefined && attr.defaultValue !== $undefined )
		{
			// raise requires
			result =
				result
				.$(
					'if( ', attr.varRef, ' === undefined )',
					attr.varRef, ' = ', attr.defaultValue, ';'
				);
		}
	}
	return result;
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	const spec = this._spec;

	// possible doesn't have any
	if( spec.collectionType === 'table' && spec.attributes.size === 0 ) return;

	let loop = $block.$let( 'arg', 'args[ a + 1 ]' );
	let _switch = $switch( 'args[ a ]' );
	switch( spec.collectionType )
	{
		case 'group':
		{
			let dupCheck =
				$( '{ if( !groupDup ) { group = tim.copy( group ); groupDup = true; } }' );
			_switch =
				_switch
				.$case(
					'"group:init"',
					$block
					.$( 'group = arg' )
					.$( 'groupDup = true' )
					.$break( )
				)
				.$case(
					'"group:set"',
					dupCheck.$( 'group[ arg ] = args[ ++a + 1 ]' ).$break( )
				)
				.$case(
					'"group:remove"',
					dupCheck.$( 'delete group[ arg ]' ).$break( )
				);
			break;
		}
		case 'list':
		{
			const dupCheck = $( '{ if( !listDup ) { list = list.slice( ); listDup = true; } }' );
			_switch =
				_switch
				.$case(
					'"list:init"',
					$block
					.$(
						'if( Array.isArray( arg ) )',
						'{ list = arg; listDup = true; }',
						'else { list = arg._list; listDup = false; }'
					)
					.$break( )
				)
				.$case( '"list:append"', dupCheck.$( 'list.push( arg )' ).$break( ) )
				.$case(
					'"list:insert"',
					dupCheck.$( 'list.splice( arg, 0, args[ ++a + 1 ] )' ).$break( )
				)
				.$case( '"list:remove"', dupCheck.$( 'list.splice( arg, 1 ) ' ).$break( ) )
				.$case( '"list:set"', dupCheck.$( 'list[ arg ] = args[ ++a + 1 ]' ).$break( ) );
			break;
		}
		case 'set':
		{
			const dupCheck =
				$( '{ if( !setDup ) { set = new Set( set ); setDup = true; } }' );
			_switch =
				_switch
				.$case( '"set:add"', dupCheck.$( 'set.add( arg, args[ a + 1 ] )' ).$break( ) )
				.$case(
					'"set:init"',
					$block
					.$check( 'if( !( arg instanceof Set ) ) throw new Error( );' )
					.$( 'set = arg' )
					.$( 'setDup = true' )
					.$break( )
				)
				.$case( '"set:remove"', dupCheck.$( 'set.delete( arg )' ).$break( ) );
			break;
		}
		case 'table':
		{
			for( let attr of spec.attributes )
			{
				_switch =
					_switch
					.$case(
						attr.$name,
						'{',
							'if( arg !== pass ) {', attr.varRef, ' = arg; }',
							'break;',
						'}'
					);
			}
			break;
		}
		case 'twig':
		{
			let dupCheck =
				$(
					'{ if( twigDup !== true )',
					'{',
					'  twig = tim.copy( twig );',
					'  keys = keys.slice( );',
					'  twigDup = true;',
					'} }'
				);
			_switch =
				_switch
				.$case(
					'"twig:add"',
					dupCheck
					.$( 'key = arg' )
					.$( 'arg = args[ ++a + 1 ]' )
					.$( 'if( twig[ key ] !== undefined ) throw new Error( );' )
					.$( 'twig[ key ] = arg' )
					.$( 'keys.push( key )' )
					.$break( )
				)
				.$case(
					'"twig:init"',
					$block
					.$( 'twigDup = true' )
					.$( 'twig = arg' )
					.$( 'keys = args[ ++a + 1 ]' )
					.$check(
						$block
						.$( 'if( !Array.isArray( keys ) ) throw new Error( );' )
						.$(
							'if( Object.keys( twig ).length !== keys.length ) throw new Error( );'
						)
						.$( 'for( let key of keys )',
							$block
							.$( 'if( twig[ key ] === undefined ) throw new Error( );' )
						)
					)
					.$break( )
				)
				.$case(
					'"twig:insert"',
					dupCheck
					.$( 'key = arg' )
					.$( 'rank = args[ a + 2 ]' )
					.$( 'arg = args[ a +  3 ]' )
					.$( 'a += 2' )
					.$( 'if( twig[ key ] !== undefined ) throw new Error( );' )
					.$( 'if( rank < 0 || rank > keys.length ) throw new Error( );' )
					.$( 'twig[ key ] = arg' )
					.$( 'keys.splice( rank, 0, key )' )
					.$break( )
				)
				.$case(
					'"twig:remove"',
					dupCheck
					.$( 'if( twig[ arg ] === undefined ) throw new Error( );' )
					.$( 'delete twig[ arg ]' )
					.$( 'keys.splice( keys.indexOf( arg ), 1 )' )
					.$break( )
				)
				.$case(
					'"twig:set+"',
					dupCheck
					.$( 'key = arg' )
					.$( 'arg = args[ ++a + 1 ]' )
					.$( 'if( twig[ key ] === undefined ) keys.push( key );' )
					.$( 'twig[ key ] = arg' )
					.$break( )
				)
				.$case(
					'"twig:set"',
					dupCheck
					.$( 'key = arg' )
					.$( 'arg = args[ ++a + 1 ]' )
					.$( 'if( twig[ key ] === undefined ) throw new Error( );' )
					.$( 'twig[ key ] = arg' )
					.$break( )
				);
			break;
		}
		default: throw new Error( );
	}
	_switch = _switch.$default( 'throw new Error( args[ a ] )' );
	loop = loop.append( _switch );
	return(
		$block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	const spec = this._spec;
	const attributes = spec.attributes;
	let receiver = $block;
	switch( spec.collectionType )
	{
		case 'group':
			receiver =
				receiver
				.$( 'group = this._group' )
				.$( 'groupDup = false' );
			break;
		case 'list':
			receiver =
				receiver.$( 'list = this._list' )
				.$( 'listDup = false' );
			break;
		case 'set':
			receiver =
				receiver
				.$( 'set = this._set' )
				.$( 'setDup = false' );
			break;
		case 'table':
			for( let attr of attributes )
			{
				const val = $( 'this.', attr.assign );
				receiver = receiver.$( attr.varRef, ' = ', val );
			}
			break;
		case 'twig':
			receiver =
				receiver
				.$( 'twig = this._twig' )
				.$( 'keys = this.keys' )
				.$( 'twigDup = false' );
			break;
	}
	if( receiver === $block && spec.collectionType === 'table' ) return;
	let result = $( 'if ( this !== Self )', receiver, ';' );
	switch( spec.collectionType )
	{
		case 'group': return result.$elsewise( '{ group = { }; groupDup = true; }' );
		case 'list': return result.$elsewise( '{ list = [ ]; listDup = true; }' );
		case 'set': return result.$elsewise( '{ set = new Set( ); setDup = true; }' );
		case 'table': return result;
		case 'twig': return result.$elsewise( '{ twig = { }; keys = [ ]; twigDup = true; }' );
		default: throw new Error( );
	}
};

/*
| Generates the creators interning.
*/
def.lazyFunc._creatorIntern =
	function( json )
{
	const spec = this._spec;
	if( spec.abstract ) return;
	const hashTim = $number( spec.hash );
	if( spec.singleton ) return json ? undefined : $block.$( 'const hash = ', hashTim );

	let hashCall;
	let eqTest;
	// continue searching the hashtable.
	let block =
		$block
		.$if( 'icollisions > ' + icollisionsRemap, $block.$( 'iremap( )' ) );

	let eqReturn = $block;
	if( spec.setGlobal ) eqReturn = eqReturn.$( spec.setGlobal, '= e' );
	eqReturn = eqReturn.$( 'return e' );

	switch( spec.collectionType )
	{
		case 'list':
			block = block.$( 'const len = list.length' );
			// hash making
			hashCall =
				$( 'tim._hashIterable( )' )
				.$arg( hashTim )
				.$arg( 'list' );
			// equality testing
			eqTest =
				$block
				.$( 'const elist = e._list' )
				.$if( 'len !== elist.length', 'continue' )
				.$( 'let eq = true' )
				.$for( 'let a = 0', 'a < len', 'a++',
					$if( 'list[ a ] !== elist[ a ]',
						$block.$( 'eq = false' ).$( 'break' )
					)
				)
				.$if( 'eq', eqReturn );
			break;
		case 'group':
			block = block.$( 'const keys = Object.freeze( Object.keys( group ).sort( ) )' );
			// hash making
			hashCall =
				$( 'tim._hashKeys( )' )
				.$arg( hashTim )
				.$arg( 'keys' )
				.$arg( 'group' );
			// equality testing
			eqTest =
				$block
				.$( 'const ekeys = e.keys' )
				.$( 'const len = keys.length' )
				.$( 'const egroup = e._group' )
				.$if( 'keys.length !== ekeys.length', 'continue' )
				.$( 'let eq = true' )
				.$for(
					'let a = 0', 'a < len', 'a++',
					$block
					.$( 'const key = keys[ a ]' )
					.$if( 'key !== ekeys[ a ] || group[ key ] !== egroup[ key ]',
						$block.$( 'eq = false' ).$( 'break' )
					)
				)
				.$if( 'eq', eqReturn );
			break;
		case 'table':
		{
			// hash making
			hashCall = $( 'tim._hashArgs( ) ' ).$arg( hashTim );
			for( let attr of spec.attributes ) hashCall = hashCall.$arg( attr.varRef );
			// equality testing
			const list = [ ];
			for( let arg of spec.attributes )
				list.push( $( arg.varRef, ' === ', 'e.', arg.name ) );
			if( list.length === 0 ) throw new Error( );
			else if( list.length === 1 ) eqTest = list[ 0 ];
			else eqTest = $and.apply( undefined, list );
			eqTest = $if( eqTest, eqReturn );
			break;
		}
		case 'twig':
			// hash making
			hashCall =
				$( 'tim._hashKeys( )' )
				.$arg( hashTim )
				.$arg( 'keys' )
				.$arg( 'twig' );
			// equality testing
			eqTest =
				$block
				.$( 'const len = keys.length' )
				.$( 'const ekeys = e.keys' )
				.$( 'const etwig = e._twig' )
				.$if( 'keys.length !== ekeys.length', 'continue' )
				.$( 'let eq = true' )
				.$for(
					'let a = 0', 'a < len', 'a++',
					$block
					.$( 'const key = keys[ a ]' )
					.$if( 'key !== ekeys[ a ] || twig[ key ] !== etwig[ key ]',
						$block.$( 'eq = false' ).$( 'break' )
					)
				)
				.$if( 'eq', eqReturn );
			break;
		case 'set':
			// for a set hash values need to be sorted since there is no order
			// in the set.
			hashCall =
				$( 'tim._hashSet( )' )
				.$arg( hashTim )
				.$arg( 'set' );
			// equality testing
			eqTest =
				$block
				.$( 'const eset = e._set' )
				.$if( 'set.size !== eset.size', 'continue' )
				.$( 'let eq = true' )
				.$forOfLet(
					'e', 'set',
					$block
					.$if( '!eset.has( e )', $block.$( 'eq = false' ).$( 'break' ) )
				)
				.$if( 'eq', eqReturn );
			break;
	}
	return(
		block
		// the original hash
		.$( 'const hash = ', hashCall )
		// the position in imap
		.$( 'let ihash = hash' )
		.$( 'let collisions = 0' )
		.$for(
			'let wr = imap.get( hash )',
			'wr',
			$comma( 'wr = imap.get( ++ihash )', 'collisions++' ),
			$block
			.$( 'const e = wr.deref( )' )
			.$( 'if( !e )', $block.$( 'break' ) )  // TODO some parser issues
			.$( eqTest )
		)
		.$if( 'collisions',
			$block
			//.$check( 'console.log( "collision", "' + spec.trace.asString + '", collisions );' )
			.$( 'icollisions += collisions' )
		)
	);
};

/*
| Generates the creators return statement
|
| ~optimization: if defined code optimizing inherits
*/
def.proto._creatorReturn =
	function( optimization )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const spec = this._spec;
	if( spec.singleton )
	{
		return(
			$block
			.$(
				'if( !_singleton ) _singleton = new Constructor('
				+ ( spec.abstract ? ' ' : ' hash ' )
				+ ');'
			)
			.$( 'return _singleton' )
		);
	}
	let call = $( 'Constructor( )' );
	for( let argName of this._constructorList ) call = call.$arg( argName );
	let block =
		$block
		.$( 'const newtim = new', call, ';' )
		.$( optimization );
	if( !spec.abstract ) block = block.$( 'imap.set( ihash, new WeakRef( newtim ) )' );
	if( spec.setGlobal ) block = block.$( spec.setGlobal, '= newtim' );
	block = block.$( 'cPut( newtim )' );
	return block.$( 'return newtim' );
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
/**/if( CHECK && arguments.length !== 0 ) throw new Error( );
	const spec = this._spec;
	const varlist = [ ];
	switch( spec.collectionType )
	{
		case 'group': varlist.push( 'group', 'groupDup' ); break;
		case 'list': varlist.push( 'list', 'listDup' ); break;
		case 'set': varlist.push( 'set', 'setDup' ); break;
		case 'table':
			for( let attr of spec.attributes ) varlist.push( attr.varRef.name );
			break;
		case 'twig': varlist.push( 'key', 'keys', 'rank', 'twig', 'twigDup' ); break;
		default: throw new Error( );
	}
	varlist.sort( );
	let result = $block;
	for( let varname of varlist ) result = result.$let( varname );
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const spec = this._spec;
	const varlist = [ ];
	switch( spec.collectionType )
	{
		case 'group':
			if( spec.json) varlist.push( 'jgroup', 'group' );
			break;
		case 'list':
			if( spec.json ) varlist.push( 'jlist', 'list' );
			break;
		case 'set':
			if( spec.json ) varlist.push( 'jset', 'set' );
			break;
		case 'table':
			for( let attr of spec.attributes ) varlist.push( attr.varRef.name );
			break;
		case 'twig':
			if( spec.json ) varlist.push( 'jval', 'jwig', 'keys', 'twig' );
			break;
		default: throw new Error( );
	}
	varlist.sort( );
	let result = $block;
	for( let varname of varlist ) result = result.$let( varname );
	return result;
};

/*
| Generates a fromJsonCreator's json parser for one attribute
*/
def.proto._fromJsonCreatorAttributeParser =
	function( attr )
{
	// handles attributes that can have only one type
	switch( attr.types.timtype )
	{
		case TypeBoolean:
		case TypeInteger:
		case TypeNumber:
		case TypeString:
			return $( attr.varRef, ' = arg' );
		case TypeTim:
		{
			const ja = attr.types.spec.fromJsonArgs;
			let call = $( attr.types.$varname, '.FromJson( arg )' );
			let first = true;
			for( let a of ja )
			{
				if( first ) { first = false; continue; }
				call = call.$arg( a );
			}
			return $( attr.varRef, ' = ', call );
		}
	}

	// the code switch
	let cSwitch;
	// primitive checks
	const pcs = [ ];
	// table of already generated Json types
	for( let type of attr.types )
	{
		switch( type.timtype )
		{
			case TypeBoolean: pcs.push( 'typeof( arg ) === "boolean"' ); break;
			case TypeNull: pcs.push( 'arg === null' ); break;
			case TypeNumber:
			case TypeInteger:
				pcs.push( 'typeof( arg ) === "number"' ); break;
			case TypeString: pcs.push( 'typeof( arg ) === "string"' ); break;
			case TypeUndefined: pcs.push( 'arg === undefined' ); break;
			case TypeTim:
			{
				if( !cSwitch ) cSwitch = $switch( 'arg.$type' ).$default( 'throw new Error( );' );
				let jsontype = type.spec.json;
/**/			if( CHECK && jsontype.indexOf( '/' ) >= 0 ) throw new Error( );
				let call = $( type.$varname, '.FromJson( arg )' );
				let first = true;
				const ja = type.spec.fromJsonArgs;
				for( let a of ja )
				{
					// the first one is 'json'
					if( first ) { first = false; continue; }
					call = call.$arg( a );
				}
				cSwitch =
					cSwitch
					.$case(
						$string( jsontype ),
						$block
						.$( attr.varRef, ' = ', call )
						.$break( )
					);
				break;
			}
			default: throw new Error( );
		}
	}
	const pcsl = pcs.length;
	if( pcsl === 0 ) return cSwitch;
	let cond = $expr( pcs[ 0 ] );
	for( let a = 1; a < pcsl; a++ ) cond = $or( cond, $expr( pcs[ a ] ) );
	const pcif = $( 'if( ', cond, ' )', attr.varRef, ' = arg;' );
	if( !cSwitch ) return pcif;
	return pcif.$elsewise( cSwitch );
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	const spec = this._spec;
	const attributes = spec.attributes;
	// the switch
	let nameSwitch =
		$switch( 'name' )
		.$case(
			'"$type"',
			$block
			.$( 'if( arg !==', $string( spec.json ), ') throw new Error( );' )
			.$break( )
		);

	switch( spec.collectionType )
	{
		case 'group':
			nameSwitch = nameSwitch.$case( '"group"', $block.$( 'jgroup = arg' ).$break( ) );
			break;
		case 'list':
			nameSwitch = nameSwitch.$case( '"list"', $block.$( 'jlist = arg' ).$break( ) );
			break;
		case 'set':
			nameSwitch = nameSwitch.$case( '"set"', $block.$( 'jset = arg' ).$break( ) );
			break;
		case 'table':
			break;
		case 'twig':
			nameSwitch =
				nameSwitch
				.$case( '"twig"', $block.$( 'jwig = arg' ).$break( ) )
				.$case( '"keys"', $block.$( 'keys = arg' ).$break( ) );
			break;
		default: throw new Error( );
	}

	for( let name of jsonList )
	{
		if( Self._fromJsonCreatorParserIgnores[ name ] ) continue;
		const attr = attributes.get( name );
		nameSwitch =
			nameSwitch
			.$case(
				$string( attr.name ),
				$block
				.$( this._fromJsonCreatorAttributeParser( attr ) )
				.$break( )
			);
	}

	return(
		$block
		.$( 'for( let name in json )',
			$block
			.$( 'const arg = json[ name ]' )
			.append( nameSwitch )
		)
	);
};

/*
| Ignored attributes in genFromJsonCreatorParser
*/
def.staticLazy._fromJsonCreatorParserIgnores =
	( ) => ( {
		group: true,
		keys: true,
		list: true,
		set: true,
		twig: true
	} );

/*
| Generates the fromJsonCreator's group processing.
*/
def.lazy._fromJsonCreatorGroupProcessing =
	function( )
{
	const spec = this._spec;
	const items = spec.items;
	let haveNull = false;
	// FUTURE dirty workaround
	if( items.size === 1 && items.trivial.timtype === TypeString )
	{
		return(
			$block
			.$( 'if( !jgroup ) throw new Error( );' )
			.$( 'group = jgroup' )
		);
	}
	// FUTURE dirty workaround 2
	if( items.size === 1 && items.trivial.timtype === TypeBoolean )
	{
		return(
			$block
			.$( 'if( !jgroup ) throw new Error( );' )
			.$( 'group = jgroup' )
		);
	}
	const result =
		$block
		.$( 'if( !jgroup ) throw new Error( );' )
		.$( 'group = { }' );
	let loopSwitch =
		$switch( 'jgroup[ k ].$type' )
		.$default( 'throw new Error( );' );
	// FUTURE allow more than one non-tim type
	let customDefault = false;
	for( let type of items )
	{
		switch( type.timtype )
		{
			case TypeBoolean:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.$default(
						'if( typeof( jgroup[ k ] ) === "boolean" ) group[ k ] = jgroup[ k ];',
						'else throw new Error( );'
					);
				continue;
			}
			case TypeNull: haveNull = true; continue;
			case TypeNumber:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.$default(
						$(
							'if( typeof( jgroup[ k ] ) === "number" )',
							'{ group[ k ] = jgroup[ k ]; }',
							'else',
							'{ throw new Error( ); }'
						)
					);
				continue;
			}
			case TypeString:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.$default(
						$(
							'if( typeof( jgroup[ k ] ) === "string" )',
							'{ group[ k ] = jgroup[ k ]; }',
							'else',
							'{ throw new Error( ); }'
						)
					);
				continue;
			}
		}

		const jsontype = type.spec.json;
		if( !jsontype ) throw new Error( );

		const ja = type.spec.fromJsonArgs;
		let call = $( type.$varname, '.FromJson( jgroup[ k ] )' );
		let first = true;
		for( let a of ja )
		{
			if( first ) { first = false; continue; }
			call = call.$arg( a );
		}
		loopSwitch =
			loopSwitch.$case(
				$string( jsontype ),
				$block.$( 'group[ k ] =', call ).$break( )
			);
	}

	let loopBody;
	if( !haveNull ) loopBody = loopSwitch;
	else
	{
		loopBody =
			$block
			.$if(
				'jgroup[ k ] === null',
				$block
				.$( 'group[ k ] = null' )
				.$continue( )
			)
			.$( loopSwitch );
	}
	return result.$( 'for( let k in jgroup )', loopBody, ';' );
};

/*
| Generates the fromJsonCreator's list processing.
*/
def.lazy._fromJsonCreatorListProcessing =
	function( )
{
	const spec = this._spec;
	const list = spec.items;

	// FIXME XXX dirty workaround
	if( list.size === 1 && list.trivial.timtype === TypeString )
	{
		return(
			$block
			.$( 'if( !jlist ) throw new Error( );' )
			.$( 'list = jlist' )
		);
	}

	let loopBody = $block;

	const result =
		$block
		.$( 'if( !jlist ) throw new Error( );' )
		.$( 'list = [ ]' );

	let loopSwitch =
		$switch( 'jlist[ r ].$type' )
		.$default( 'throw new Error( )' );

	for( let type of list )
	{
		switch( type )
		{
			case tsNull:
				loopBody = loopBody.$( 'if( jlist[ r ] === null ) { list[ r ] = null; continue; }' );
				continue;
			case tsUndefined:
				loopBody =
					loopBody.$( 'if( jlist[ r ] === undefined ) { list[ r ] = undefined; continue; }' );
				continue;
			case TypeBoolean.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "boolean" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
			case TypeNumber.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "number" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
			case TypeString.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "string" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
		}

		const jsontype = type.spec.json;
		if( !jsontype ) throw new Error( );
		const ja = type.spec.fromJsonArgs;
		let call = $( type.$varname, '.FromJson( jlist[ r ] )' );
		let first = true;
		for( let a of ja )
		{
			if( first ) { first = false; continue; }
			call = call.$arg( a );
		}

		loopSwitch =
			loopSwitch
			.$case(
				$string( jsontype ),
				$block.$( 'list[ r ] =', call ).$break( )
			);
	}

	loopBody =
		loopBody.length > 0
		? loopBody.append( loopSwitch )
		: loopSwitch;

	return result.$( 'for( let r = 0, rl = jlist.length; r < rl; ++r )', loopBody, ';' );
};


/*
| Generates the fromJsonCreator's set processing.
*/
def.lazy._fromJsonCreatorSetProcessing =
	function( )
{
	const spec = this._spec;
	const items = spec.items;

	// FUTURE dirty workaround
	if(
		items.size !== 1
		|| (
			items.trivial.timtype !== TypeNumber
			&& items.trivial.timtype !== TypeString
		)
	)
	{
		throw new Error( 'sets json conversation not yet fully implemented' );
	}

	return(
		$block
		.$( 'if( !jset ) throw new Error( );' )
		.$( 'set = new Set( jset )' )
	);
};

/*
| Generates the fromJsonCreator's twig processing.
*/
def.lazy._fromJsonCreatorTwigProcessing =
	function( )
{
	const spec = this._spec;
	const items = spec.items;
	let switchExpr = $switch( 'jval.$type' );
	for( let twigType of items )
	{
		const jsontype = twigType.spec.json;
		const ja = twigType.spec.fromJsonArgs;
		let call = $( twigType.$varname, '.FromJson( jval )' );
		let first = true;
		for( let a of ja )
		{
			if( first ) { first = false; continue; }
			call = call.$arg( a );
		}
		switchExpr =
			switchExpr.$case(
				$string( jsontype ),
				$block.$( 'twig[ key ] =', call ).$break( )
			);
	}
	// invalid twig type
	switchExpr = switchExpr.$default( 'throw new Error( );' );
	const loop =
		$block
		// json keys/twig mismatch
		.$( 'if( !jwig[ key ] ) throw new Error( );' )
		.$( 'jval = jwig[ key ]' )
		.append( switchExpr );
	return(
		$block
		.$( 'twig = { }' )
		// ranks/twig information missing
		.$( 'if( !jwig || !keys ) throw new Error( );' )
		.$( 'for( let key of keys )', loop )
	);
};

/*
| Generates the fromJsonCreator's return statement.
*/
def.lazy._fromJsonCreatorReturn =
	function( )
{
	const spec = this._spec;
	if( spec.singleton ) return $( 'return Self.singleton' );
	let call = $( 'Constructor( )' );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash':
				call = call.$arg( 'hash' );
				break;
			case 'groupDup':
			case 'listDup':
			case 'twigDup':
				call = call.$arg( 'true' );
				break;
			case 'group':
			case 'keys':
			case 'list':
			case 'set':
			case 'twig':
				call = call.$arg( name );
				break;
			default:
				call = call.$arg( name );
				break;
		}
	}
	let block = $block.$( 'const newtim = new', call );
	if( !spec.abstract ) block = block.$( 'imap.set( ihash, new WeakRef( newtim ) )' );
	return block.$( 'return newtim' );
};

/*
| Generates the fromJsonCreator.
*/
def.lazy._fromJsonCreator =
	function( )
{
	const spec = this._spec;
	// all attributes expected from json
	const jsonList = [ ];
	for( let attr of spec.attributes )
		if( attr.json ) jsonList.push( attr.name );
	if( spec.collectionType === 'twig' ) jsonList.push( 'twig', 'keys' );
	jsonList.sort( );

	// function contents
	let block =
		$block
		.$check(
			'if( arguments.length !== ' + spec.fromJsonArgs.length + ' ) throw new Error( );'
		)
		.$( this._fromJsonCreatorVariables )
		.$( this._fromJsonCreatorParser( jsonList ) )
		.$( this._creatorDefaults );

	switch( spec.collectionType )
	{
		case 'group': block = block.$( this._fromJsonCreatorGroupProcessing ); break;
		case 'list': block = block.$( this._fromJsonCreatorListProcessing ); break;
		case 'set': block = block.$( this._fromJsonCreatorSetProcessing ); break;
		case 'table': break;
		case 'twig': block = block.$( this._fromJsonCreatorTwigProcessing ); break;
		default: throw new Error( );
	}
	block =
		block
		.$( this._creatorChecks( true ) )
		.$( this._creatorIntern( true ) )
		.$( this._fromJsonCreatorReturn );
	let func = $func( block );
	for( let a of spec.fromJsonArgs ) func = func.$arg( a );
	return(
		$block
		.$comment( 'Creates a new object from json.' )
		.$( 'Self.FromJson =', func )
	);
};

/*
| Generates the interning map and caching queue.
*/
def.lazy._iMapAndCache =
	function( )
{
	const spec = this._spec;

	if( spec.singleton ) return;

	let block =
		$block
		.$comment( 'The interning map.' )
		.$( 'let imap = new Map( )' )
		.$comment( 'Collisions so far in imap.' )
		.$( 'let icollisions = 0' )
		.$comment( 'Remaps the interning map.' )
		.$( 'const iremap = ',
			$func(
				$block
				.$check( 'console.log( "remap" )' )
				.$( 'let omap = imap' )
				.$( 'icollisions = 0' )
				.$( 'imap = new Map( )' )
				.$( 'for( let wr of omap.values( ) )',
					$block
					.$( 'const e = wr.deref( )' )
					.$( 'if( !e ) continue;' )
					.$( 'let ihash = e.__hash' )
					.$while( 'imap.get( ihash )', 'ihash++' )
					.$( 'imap.set( ihash, wr )' )
				)
			)
		)
		.$comment( 'Caching queue.' )
		.$( 'let cSize = 1024' )
		.$( 'let cArray = new Array( cSize )' )
		.$( 'let cPos = 0' )
		.$comment( 'Timestamp since last turnaround' )
		.$( 'let cTurn = Date.now( )' )
		.$comment( 'Puts an entry in the caching queue.' )
		.$( 'const cPut = ',
			$func(
				$block
				.$( 'cArray[ cPos ] = tim;' )
				.$( 'cPos = ++cPos % cSize ' )
				.$if( '!cPos',
					$block
					.$( 'const now = Date.now( )' )
					.$if( 'now - cTurn < 180000',
						$block
						//.$check(
						//	'console.log(',
						//		'"increasing cache queue for",',
						//		'Self.__DEBUG_PATH__',
						//	')'
						//)
						.$( 'cPos = cSize' )
						.$( 'cSize *= 2' )
						.$( 'cArray.length = cSize' )
					)
					.$( 'cTurn = now' )
				)
			)
			.$arg( 'tim', 'tim to add' )
		);

	return block;
};

/*
| Generates the json type identifier.
*/
def.lazy._jsonType =
	function( )
{
	const spec = this._spec;
	const fja = spec.fromJsonArgs;
	let $fja = $array;
	for( let a of fja ) $fja = $fja.create( 'list:append', $string( a ) );
	if( spec.abstract )
	{
		return(
			$block
			.$comment( 'Json type identifier' )
			.$( 'Self.$type = ', $string( spec.json ) )
			.$( 'Self.$fromJsonArgs = ', $fja )
		);
	}
	return(
		$block
		.$comment( 'Json type identifier' )
		.$( 'Self.$type = prototype.$type =', $string( spec.json ) )
		.$( 'Self.$fromJsonArgs = prototype.$fromJsonArgs =', $fja )
	);
};

/*
| Generates code for setting the prototype
| lazy value named 'name' to 'func'.
|
| ~name: lazy value name as string
| ~func: lazy value function name
*/
def.proto._protoLazyValueSet =
	function( name, func )
{
	return(
		$block
		.$( 'tim._proto.lazyValue( prototype,', $string( name ), ', tim._proto.', func, ')' )
	);
};

/*
| Generates code for setting the prototype
| entry 'key' to 'value'
*/
def.proto._protoSet =
	function( key, value )
{
	return $( 'prototype.', key, ' = tim._proto.', value );
};

/*
| Generates the node include section.
*/
def.lazy._reflection =
	function( )
{
	return(
		$block
		.$comment( 'Type reflection.' )
		.$( 'prototype.timtype = Self' )
	);
};

/*
| Generates the requires.
*/
def.lazy._requires =
	function( )
{
	const spec = this._spec;
	let block = $block;
	let done = { };

	for( let type of spec.imports )
	{
		if( type.timtype !== TypeTim ) continue;

		const varname = type.varname;
		if( done[ varname ] ) continue;
		done[ varname ] = true;

		if( type.isPrimitive ) continue;
		block = block.$const( type.varname, type.require( spec.trace.back ) );
	}
	return block;
};

/*
| Generates the singleton decleration.
*/
def.lazy._singleton =
	function( )
{
	return(
		$block
		.$comment( 'Singleton.' )
		.$let( '_singleton' )
		.$(
			'tim._proto.lazyStaticValue( Self, "singleton", ',
			$func( 'return Self._create( );' ),
			')'
		)
	);
};

/*
| Generates a type check of a non set variable.
| It is true if the variable fails the check.
|
| ~aVar: name of the variable
| ~type: type to check for
*/
def.proto._singleTypeCheckFailCondition =
	function( aVar, type )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( aVar.timtype !== AstVar ) throw new Error( );
/**/}

	switch( type.timtype )
	{
		case TypeArbitrary: return $( aVar, '?.timtype === undefined' );
		case TypeBoolean: return $( 'typeof( ', aVar, ' ) !== "boolean"' );
		case TypeDate: return $( '!(', aVar, 'instanceof Date )' );
		case TypeFunction: return $( 'typeof( ', aVar, ' ) !== "function"' );
		case TypeInteger:
			return $(
				$( 'typeof( ', aVar, ' ) !== "number"' ),
				'|| Number.isNaN( ', aVar, ' )',
				'|| Math.floor( ', aVar, ' ) !== ', aVar
			);
		case TypeNull: return $( aVar, '!== null' );
		case TypeNumber:
			return $(
				'typeof( ', aVar, ' ) !== "number"',
				'|| Number.isNaN( ', aVar, ' )'
			);
		case TypeProtean: return $( 'typeof( ', aVar, ' ) !== "object"' );
		case TypeString: return $( 'typeof( ', aVar, ' ) !== "string"' );
		case TypeTim: return $( aVar, '.timtype !== ', type.$varname );
		case TypeUndefined: return $( aVar, '!== undefined' );
		default: throw new Error( );
	}
	// unreachable
};

/*
| Generates the timProto stuff.
*/
def.lazy._timProto =
	function( )
{
	const spec = this._spec;
	switch( spec.collectionType )
	{
		case 'group':
			return(
				$block
				.$comment(
					'Returns the group with another group added,',
					'overwriting collisions.'
				)
				.$( this._protoSet( 'addGroup', 'groupAddGroup' ) )
				.$comment( 'Gets one element from the group.' )
				.$( this._protoSet( 'get', 'groupGet' ) )
				.$comment( 'Returns the group with one element removed.' )
				.$( this._protoSet( 'remove', 'groupRemove' ) )
				.$comment( 'Returns the group with one element set.' )
				.$( this._protoSet( 'set', 'groupSet' ) )
				.$comment( 'Returns the size of the group.')
				.$( this._protoLazyValueSet( 'size', 'groupSize' ) )
				.$comment( 'Iterates over the group by sorted keys.' )
				.$(
					'prototype[ Symbol.iterator ] =',
					$generator(
						'for( let key of this.keys ) yield this.get( key );'
					)
				)
				.$comment( 'Creates an empty group of this type.' )
				.$( 'tim._proto.lazyStaticValue( Self, "Empty", ',
					$func( 'return Self.create( );' ),
					')'
				)
				.$( 'Self.Table =',
					$func( 'return Self.create( "group:init", group );')
					.$arg( 'group', 'the group' )
				)
			);
		case 'list':
			return(
				$block
				.$comment( 'Returns the list with an element appended.' )
				.$( this._protoSet( 'append', 'listAppend' ) )
				.$comment( 'Returns the list with another list appended.' )
				.$( this._protoSet( 'appendList', 'listAppendList' ) )
				.$comment( 'Returns an array clone of the list.' )
				.$( this._protoSet( 'clone', 'listClone' ) )
				.$comment( 'Returns the first element of the list.')
				.$( this._protoLazyValueSet( 'first', 'listFirst' ) )
				.$comment( 'Returns one element from the list.' )
				.$( this._protoSet( 'get', 'listGet' ) )
				.$comment( 'Returns the list with one element inserted.' )
				.$( this._protoSet( 'insert', 'listInsert' ) )
				.$comment( 'Returns the last element of the list.')
				.$( this._protoLazyValueSet( 'last', 'listLast' ) )
				.$comment( 'Returns the length of the list.')
				.$( this._protoLazyValueSet( 'length', 'listLength' ) )
				.$comment( 'Returns the list with one element removed.' )
				.$( this._protoSet( 'remove', 'listRemove' ) )
				.$comment( 'Returns the list with one element set.' )
				.$( this._protoSet( 'set', 'listSet' ) )
				.$comment( 'Returns a slice from the list.' )
				.$( this._protoSet( 'slice', 'listSlice' ) )
				.$comment( 'Returns a sorted list.' )
				.$( this._protoSet( 'sort', 'listSort' ) )
				// FUTURE maybe this can be simplified?
				.$comment( 'Forwards the iterator.' )
				.$(
					'prototype[ Symbol.iterator ] =',
					$func( 'return this._list[ Symbol.iterator ]( )' )
				)
				.$comment( 'Reverse iterates over the list.' )
				.$(
					'prototype.reverse =',
					$generator(
						'for( let a = this.length - 1; a >= 0; a-- ) yield this._list[ a ];'
					)
				)
				.$comment( 'Creates the list from an Array.' )
				.$( 'Self.Array =',
					$func( 'return Self.create( "list:init", array );')
					.$arg( 'array', 'the array' )
				)
				.$comment( 'Creates the list with direct elements.' )
				.$( 'Self.Elements =',
					$func(
						'return Self.create( "list:init", Array.prototype.slice.call( arguments) );'
					)
				)
				.$comment( 'Creates an empty list of this type.' )
				.$( 'tim._proto.lazyStaticValue( Self, "Empty", ',
					$func( 'return Self.create( );' ),
					')'
				)
			);
		case 'set':
			return(
				$block
				.$comment( 'Returns the set with one element added.' )
				.$( this._protoSet( 'add', 'setAdd' ) )
				.$comment( 'Returns the set with another set added.' )
				.$( this._protoSet( 'addSet', 'setAddSet' ) )
				.$comment( 'Returns a clone primitive.' )
				.$( 'prototype.clone = ', $func( 'return new Set( this._set );' ) )
				.$comment( 'Returns true if the set has an element.' )
				.$( this._protoSet( 'has', 'setHas' ) )
				.$comment( 'Returns the set with one element removed.' )
				.$( this._protoSet( 'remove', 'setRemove' ) )
				.$comment( 'Returns the size of the set.' )
				.$( this._protoLazyValueSet( 'size', 'setSize' ) )
				.$comment( 'Returns the one and only element or the set if size != 1.' )
				.$( this._protoLazyValueSet( 'trivial', 'setTrivial' ) )
				.$comment( 'Forwards the iterator.' )
				.$(
					'prototype[ Symbol.iterator ] =',
					$func( 'return this._set[ Symbol.iterator ]( )' )
				)
				.$comment( 'Creates the set with direct elements.' )
				.$( 'Self.Elements =',
					$func( 'return Self.create( "set:init", new Set( arguments ) );' )
				)
			);
		case 'table':
			return $block;
		case 'twig':
			return(
				$block
				.$comment( 'Returns the element at rank.' )
				.$( this._protoSet( 'atRank', 'twigAtRank' ) )
				.$comment( 'Returns the element by key.' )
				.$( this._protoSet( 'get', 'twigGet' ) )
				.$comment( 'Returns the key at a rank.' )
				.$( this._protoSet( 'getKey', 'twigGetKey' ) )
				.$comment( 'Returns the length of the twig.')
				.$( this._protoLazyValueSet( 'length', 'twigLength' ) )
				.$comment( 'Returns the rank of the key.' )
				.$( 'tim._proto.lazyFunction( prototype, "rankOf", tim._proto.twigRankOf )' )
				.$comment( 'Returns the twig with the element at key set.' )
				.$( this._protoSet( 'set', 'twigSet' ) )
				.$comment( 'Iterates over the twig.' )
				.$(
					'prototype[ Symbol.iterator ] =',
					$generator(
						'for( let a = 0, al = this.length; a < al; a++ ) yield this.atRank( a );'
					)
				)
				.$comment( 'Reverse iterates over the twig.' )
				.$(
					'prototype.reverse =',
					$generator(
						'for( let a = this.length - 1; a >= 0; a-- ) yield this.atRank( a );'
					)
				)
				.$comment( 'Creates an empty group of this type.' )
				.$( 'tim._proto.lazyStaticValue( Self, "Empty", ',
					$func( 'return Self.create( );' ),
					')'
				)
				// FIXME use ...agrs syntax
				.$comment( 'Grows the twig.' )
				.$( 'Self.Grow =',
					$func(
						$block
						.$( 'let ranks = [ ]' )
						.$( 'let twig = { }' )
						.$for( 'let a = 0, alen = arguments.length', 'a < alen', 'a+=2',
							$block
							.$( 'const key = arguments[ a ]' )
							.$( 'ranks.push( key )' )
							.$( 'twig[ key ] = arguments[ a + 1 ]' )
						)
						.$( 'return Self.create( "twig:init", twig, ranks )' )
					)
				)
			);
		default: throw new Error( );
	}
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	const spec = this._spec;
	const attributes = spec.attributes;

	let jsontype;
	if( typeof( spec.json ) === 'string' ) jsontype = spec.json;
	else jsontype = spec.json.json;
	let olit = $objLiteral( ).add( '$type', $string( jsontype ) );

	for( let name of attributes.keys )
	{
		const attr = attributes.get( name );
		if( !attr.json ) continue;
		olit = olit.add( name, 'this.', attr.assign );
	}

	switch( spec.collectionType )
	{
		case 'group': olit = olit.add( 'group', 'this._group' ); break;
		case 'list': olit = olit.add( 'list', 'this._list' ); break;
		case 'set': olit = olit.add( 'set', 'Array.from( this._set )' ); break;
		case 'table': break;
		case 'twig': olit = olit.add( 'keys', 'this.keys' ).add( 'twig', 'this._twig' ); break;
		default: throw new Error( );
	}

	let block = $block;
	if( spec.customJsonfy ) return block;

	// jsonfy code to build return value
	let hasSubJsonfy;
	let hasI2Space = false;
	switch( spec.collectionType )
	{
		case 'group':
		case 'list':
		case 'set':
		case 'twig':
			hasSubJsonfy = spec.items.composition === 'tims' || spec.items.composition === 'mixed';
			hasI2Space = true;
			break;
		case 'table':
			hasSubJsonfy = spec.attributes.hasSubJsonfy;
			break;
		default: throw new Error( );
	}

	let jr = $block.$( 'let i0space, i1space' );
	if( hasI2Space ) jr = jr.$( 'let i2space' );
	if( hasSubJsonfy )
	{
		// NOTE: never ouroboros: package since generated will always refer to tim:
		const basePackage = tim.packages.get( spec.trace.get( 1 ).key );
		const jsonfySpacing = TypeTim.FromString( 'tim:Jsonfy/Spacing', basePackage );
		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						jsonfySpacing.$varname, '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}
	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	switch( spec.collectionType )
	{
		case 'group':
		{
			jr = jr
				.$(
					'let r = "{" + i1space +',
					$string( '"$type"' ), '+ colon +', $string( '"' + jsontype + '",' ),
					'+ i1space +', $string( '"group"' ), '+ colon'
				)
				.$( 'r += "{";' )
				.$( 'const keys = this.keys;' )
				.$( 'const group = this._group;' )
				.$( 'let first = true;' );
			// for block
			let jfb = $block.$( 'const val = group[ key ]' );
			if( spec.items.has( tsUndefined ) )
				jfb = jfb.$( 'if( val === undefined ) continue;' );
			jfb = jfb
				.$( 'if( !first ){ r += ","; } else { first = false; }' )
				.$( 'r += i2space + ', $string( '"' ), '+ key + ', $string( '"' ), '+ colon' )
				.$( 'r += ', spec.items.jsonfyCode( 'val' ) );
			jr = jr.$forOfLet( 'key', 'keys', jfb );
			jr = jr.$( 'r += i1space + "}" + i0space + "}";' );
			break;
		}
		case 'list':
		{
			jr = jr
				.$(
					'let r = "{" + i1space +',
					$string( '"$type"' ), '+ colon +', $string( '"' + jsontype + '",' ),
					'+ i1space +', $string( '"list"' ),
					'+ colon'
				)
				.$( 'if( this.length === 0 ) { return r + "[ ]" + i0space + "}" }' )
				.$( 'r += "[" + i2space;' )
				.$( 'let first = true;' )
				.$forOfLet( 'val', 'this._list',
					$block
					.$( 'if( !first ){ r += "," + i2space; } else { first = false; }' )
					.$( 'r += ', spec.items.jsonfyCode( 'val' ) )
				)
				.$( 'r += i1space + "]" + i0space + "}";' );
			break;
		}
		case 'set':
		{
			jr = jr
				.$(
					'let r = "{" + i1space +',
					$string( '"$type"' ), '+ colon +', $string( '"' + jsontype + '",' ),
					'+ i1space +', $string( '"set"' ),
					'+ colon;'
				)
				.$( 'if( this.size === 0 ) { return r + "[ ]" + i0space + "}" }' )
				.$( 'const set = this._set;' )
				.$( 'const jstrs = [ ];' )
				.$forOfLet( 'val', 'set',
					$block
					.$( 'jstrs.push( ', spec.items.jsonfyCode( 'val' ), ')' )
				)
				.$( 'jstrs.sort( );' )
				.$( 'r += "[" + i2space;' )
				.$( 'r += jstrs.join( "," +', hasI2Space ? 'i2space' : 'i1space', ');' )
				.$( 'r += i1space + "]" + i0space + "}";' );
			break;
		}
		case 'table':
		{
			jr = jr.$(
				'let r = "{" + i1space +',
				$string( '"$type"' ), '+ colon +', $string( '"' + jsontype + '"' )
			);
			let keys = attributes.keys;
			let first = true;
			for( let name of keys )
			{
				const attr = attributes.get( name );
				if( !attr.json ) continue;
				if( first ){ first = false; jr = jr.$( 'let val = this.', name ); }
				else jr = jr.$( 'val = this.', name );
				const jrb =
					$block
					.$( 'r += "," + i1space +', $string( '"' + name + '"' ), '+ colon' )
					.$( 'r +=', attr.types.jsonfyCode( 'val' ) );
				if( attr.types.has( tsUndefined ) ) jr = jr.$( 'if( val !== undefined )', jrb );
				else jr = jr.$( jrb );
			}
			jr = jr.$( 'r += i0space + "}"' );
			break;
		}
		case 'twig':
		{
			jr = jr
				.$(
					'let r = "{" + i1space +',
					$string( '"$type"' ), '+ colon +', $string( '"' + jsontype + '"' )
				)
				.$( 'r+= "," + i1space +', $string( '"keys"' ), '+ colon + "[";' )
				.$(	'const keys = this.keys;' )
				.$(	'const twig = this._twig;' )
				.$for(
					'let a = 0, alen = keys.length', 'a < alen', 'a++',
					$block
					.$( 'r += i2space + ', $string( '"' ), '+ keys[ a ] +', $string( '"' ) )
					.$( 'if( a + 1 < alen ) r += ",";' )
				)
				.$(	'r += i1space + "]," + i1space +', $string( '"twig"' ), '+ colon + "{"' )
				.$( 'let first = true' )
				.$for(
					'let a = 0, alen = keys.length', 'a < alen', 'a++',
					$block
					.$( 'const key = keys[ a ]' )
					.$( 'const val = twig[ key ]' )
					.$( 'if( !first ){ r += ","; } else { first = false; }' )
					.$( 'r += i2space +', $string( '"' ), '+ key +', $string( '"' ), '+ colon' )
					.$(	'r +=', spec.items.jsonfyCode( $( 'val' ) ) )
				)
				.$( 'r += i1space + "}" + i0space + "}";' );
			break;
		}
		default: throw new Error( );
	}

	jr = jr.$( 'return r' );
	const fjr = $func( jr ).$arg( 'spacing' );
	return(
		block
		.$comment( 'Stable jsonfy' )
		.$( 'tim._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};

/*
| Generates a type check of a variable.
|
| ~aVar: the variable to check
| ~types: the type or TypeSet it has to match
*/
def.proto._typeCheckFailCondition =
	function( aVar, types )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( types.timtype !== TypeSet ) return this._singleTypeCheckFailCondition( aVar, types );
	if( types.size === 1 ) return this._singleTypeCheckFailCondition( aVar, types.trivial );
	const condArray = [ ];
	// first do the primitives
	for( let type of types )
	{
		if( type.timtype === TypeTim ) continue;
		const cond = this._singleTypeCheckFailCondition( aVar, type );
		if( cond ) condArray.push( cond );
	}
	// then do the tims
	for( let type of types )
	{
		if( type.timtype !== TypeTim ) continue;
		const cond = this._singleTypeCheckFailCondition( aVar, type );
		if( cond ) condArray.push( cond );
	}
	if( condArray.length === 0 ) return;
	if( condArray.length === 1 ) return condArray[ 0 ];
	return $and.apply( undefined, condArray );
};
