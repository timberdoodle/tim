/*
| Generates a timcode of a tim.
*/
'use strict';

const fs = require( 'fs' );

const Generator = tim.require( 'Generate/Self.js' );
const Formatter = tim.require( 'Format/Self.js' );
const Path = tim.require( 'Path/Self.js' );
const Spec = tim.require( 'Spec/Self.js' );

def.abstract = true;

/*
| Generates a timcode.
*/
def.static.driveSync =
	function( truss, pkgname, filename, timcodeFilename )
{
	console.log( 'timcode: ' + timcodeFilename );
	const filepath = Path.FromString( filename );
	const pkg = tim.packages.get( pkgname );
	const trace = pkg.trace.add( 'specs', filename );
	const spec = Spec.FromDef( truss.def, filepath, trace, truss.requires );
	// FromDef might have already created the timcode and made the truss ready
	if( truss.coupled ) return;
	const ast = Generator.generate( spec );
	let code = Formatter.format( ast );
	if( !code.endsWith( '\n' ) ) code += '\n';
	fs.writeFileSync( timcodeFilename, code );
};

/*
| Generates a timcode.
*/
def.static.driveAsync =
	async function( truss, pkgname, filename, timcodeFilename )
{
	console.log( 'timcode: ' + timcodeFilename );
	const filepath = Path.FromString( filename );
	const pkg = tim.packages.get( pkgname );
	const trace = pkg.trace.add( 'specs', filename );
	const spec = Spec.FromDef( truss.def, filepath, trace, truss.requires );
	// FromDef might have already created the timcode and made the truss ready
	if( truss.coupled ) return;
	const ast = Generator.generate( spec );
	let code = Formatter.format( ast );
	if( !code.endsWith( '\n' ) ) code += '\n';
	await fs.promises.writeFile( timcodeFilename, code );
};
