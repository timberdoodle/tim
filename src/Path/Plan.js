/*
| Plan of filesystem paths.
*/
'use strict';

def.abstract = true;

const Plan = tim.require( 'Plan/Self' );
const Subs = tim.require( 'Plan/Piece/Subs' );

// dirs
def.staticLazy.dirs = ( ) =>
	Plan.Build( {
		key: true,
		name: 'dirs',
		subs:
			Subs.Table( {
				dirs: ( ) => Self.dirs,
				files: ( ) => Self.files
			} ),
	} );

// files
def.staticLazy.files = ( ) =>
	Plan.Build( {
		key: true,
		name: 'files',
	} );

// '/' or './'
def.staticLazy.root = ( ) =>
	Plan.Build( {
		key: true,
		name: 'root',
		subs:
			Subs.Table( {
				dirs: ( ) => Self.dirs,
				files: ( ) => Self.files
			} ),
	} );
