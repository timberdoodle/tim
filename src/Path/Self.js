/*
| A filepath trace.
*/
'use strict';

def.list = [ 'Trace/Step/Key', 'Trace/Step/Root/Plain' ];

const Plan = tim.require( 'Path/Plan' );
const StepKey = tim.require( 'Trace/Step/Key' );

/*
| A path of absolute root.
*/
def.staticLazy.aroot = ( ) =>
	Self.Elements( StepKey.create( 'key', '/', 'name', 'root', 'plan', Plan.root ) );

/*
| Turns the trace into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	let s = '';
	for( let step of this )
	{
		s += step.key;
		if( step.name === 'dirs' ) s += '/';
	}
	return s;
};

/*
| Returns a path with a dir.
*/
def.proto.d =
def.proto.dir =
	function( name )
{
	const last = this.last;
	const lplan = last.plan;
	let sub = lplan.subs.get( 'dirs' );
	if( typeof( sub ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( this.name, name );

	// removes ending slash
	if( name.endsWith( '/' ) ) name = name.substr( 0, name.length - 1 );

	// name must not have another slash
	if( name.indexOf( '/' ) >= 0 ) throw new Error( );

	// shouldn't happen
	if( !sub.key ) throw new Error( );

	const t = this.append( StepKey.create( 'key', name, 'name', 'dirs', 'plan', sub ) );
	tim.aheadValue( t, 'back', this );
	return t;
};

/*
| Returns a path with a file.
*/
def.proto.f =
def.proto.file =
	function( name )
{
	const last = this.last;
	const lplan = last.plan;

	let sub = lplan.subs.get( 'dirs' );
	if( typeof( sub ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( this.name, name );

	// removes ending slash
	if( name.endsWith( '/' ) ) name = name.substr( 0, name.length - 1 );

	// name must not have another slash
	if( name.indexOf( '/' ) >= 0 ) throw new Error( );

	// shouldn't happen
	if( !sub.key ) throw new Error( );

	const t = this.append( StepKey.create( 'key', name, 'name', 'files', 'plan', sub ) );
	tim.aheadValue( t, 'back', this );
	return t;
};

/*
| Creates the path trace from a string.
*/
def.static.FromString =
	function( s )
{
	let p;
	if( s.substr( 0, 2 ) === './' ) { p = Self.rroot; s = s.substr( 2 ); }
	else if( s[ 0 ] === '/' ) { p = Self.aroot; s = s.substr( 1 ); }

	const ar = s.split( '/' );
	const alen = ar.length;
	for( let a = 0; a < alen - 1; a++ ) p = p.dir( ar[ a ] + '/' );

	const last = ar[ alen - 1 ];
	if( last !== '' ) p = p.file( ar[ alen - 1 ] );
	return p;
};

/*
| True if this path is absolute.
*/
def.lazy.isAbsolute = function( ) { return this.get( 0 ).key === '/'; };

/*
| True if this path is relative.
*/
def.lazy.isRelative = function( ) { return this.get( 0 ).key === './'; };

/*
| Returns the parent path.
*/
def.lazy.p =
def.lazy.parent =
	function( )
{
	return this._isRParents ? this.dir( '..' ) : this.back;
};

/*
| Resolves a relative path on an absolute dir.
*/
def.proto.resolve =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir.timtype !== Self ) throw new Error( );
/**/	if( !dir.isAbsolute ) throw new Error( );
/**/	if( dir.last.name !== 'dirs'  ) throw new Error( );
/**/	if( !this.isRelative ) throw new Error( );
/**/}

	for( let a = 1, al = this.length - 1; a < al; a++ )
	{
		dir = dir.dir( this.get( a ).key );
	}
	return dir.file( this.last.key );
};

def.proto.startsWith =
	function( path )
{
/**/if( CHECK && path.timtype !== Self ) throw new Error( );

	const plen = path.length;
	const tlen = this.length;

	if( plen > tlen ) return false;

	for( let a = 0; a < plen; a++ )
	{
		if( path.get( a ) !== this.get( a ) ) return false;
	}
	return true;
};

/*
| Appends a string to the path.
*/
def.proto.string =
	function( s )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( s ) !== 'string' ) throw new Error( );
/**/}

	let path = this;
	const pa = s.split( '/' );
	let a = 0, alen = pa.length - 1;
	for( ; a < alen; a++ )
	{
		const e = pa[ a ];
		if( e === '' ) continue;
		path = path.dir( e );
	}
	if( alen >= 0 )
	{
		const e = pa[ alen ];
		if( e !== '' ) path = path.file( e );
	}
	return path;
};

/*
| A path of absolute root.
*/
def.staticLazy.rroot = ( ) =>
	Self.Elements( StepKey.create( 'key', './', 'name', 'root', 'plan', Plan.root ) );

/*
| Returns a trace with a step back.
*/
def.lazy.back =
	function( )
{
	const len = this.length;
	if( len <= 1 ) throw new Error( );
	return this.slice( 0, len - 2 );
};

/*
| True if this is a relative path that has '../' components only.
*/
def.lazy._isRParents =
	function( )
{
	const len = this.length;
	if( this.get( 0 ).key !== './' ) return false;
	for( let a = 1; a < len; a++ )
	{
		if( this.get( a ).key !== '../' ) return false;
	}
	return true;
};
