/*
| Helper for spec traces
*/
'use strict';

def.abstract = true;

const Package = tim.require( 'Spec/Package' );
const Path = tim.require( 'Path/Self' );
const Trace = tim.require( 'Trace/Self' );
const TraceSpecPlan = tim.require( 'Spec/Trace/Plan' );

/*
| Creates the trace from an absolute file path
| and the matching package.
|
| ~afile: absolute file path (path trace)
| ~basePackage: package to be used as base
*/
def.static.FromAFile =
	function( afile, basePackage )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( afile.timtype !== Path ) throw new Error( );
/**/	if( afile.get( 0 ).key !== '/' ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/}

	const base = basePackage.dir;
	// checks if basePackage is indeed a base of afile
	if( !afile.startsWith( base ) ) throw new Error( );

	let t = Trace.root( TraceSpecPlan.root ).add( 'packages', basePackage.name );
	// TODO proper srcDir
	let key = '';
	for( let a = base.length + 1, alen = afile.length; a < alen; a++ )
	{
		if( key !== '' ) key += '/';
		key += afile.get( a ).key;
	}
	return t.add( 'specs', key );
};

/*
| Creates the trace from a string.
|
| ~string: string to create from
| ~basePackage:   base package
| ~leafType: 'spec' or 'types'
*/
def.static.FromString =
	function( string, basePackage, leafType )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( string ) !== 'string' ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/	if( typeof( leafType ) !== 'string' ) throw new Error( );
/**/}

	// no relative paths
	if( string[ 0 ] === '.' ) throw new Error( );
	if( string[ 0 ] === '~' ) throw new Error( );

	const ios = string.indexOf( ':' );
	if( ios >= 0 )
	{
		// another module
		const pkgName = string.substr( 0, ios );
		const pkg = tim.packages.get( pkgName );
		let imp = string.substr( ios + 1 );
		// TODO inconsistent .js extensions
		if( imp.endsWith( '.js' ) ) imp = imp.substr( 0, imp.length - 3 );
		let exp = pkg.exports && pkg.exports.get( imp );
		// TODO dirty workaround, for yes/no export mapping
		if( !exp ) exp = imp;
		let ext;
		switch( leafType )
		{
			case 'spec': ext = '.js'; break;
			case 'types': ext = '.txt'; break;
			default: throw new Error( );
		}
		if( !exp.endsWith( ext ) ) exp += ext;
		return pkg.trace.add( 'specs', exp );
	}
	else
	{
		// path from this module
		let ext;
		switch( leafType )
		{
			case 'spec': ext = '.js'; break;
			case 'types': ext = '.txt'; break;
			default: throw new Error( );
		}
		if( !string.endsWith( ext ) ) string += ext;
		return basePackage.trace.add( 'specs', string );
	}
};
