/*
| Plan of traces in tim's spec tree.
*/
'use strict';

def.abstract = true;

const Plan = tim.require( 'Plan/Self' );

def.staticLazy.root = ( ) =>
	Plan.Build( {
		name: 'root',
		subs:
		{
			packages:
			{
				key: true,
				subs:
				{
					specs: { key: true },
				},
			},
		},
	} );
