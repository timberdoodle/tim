/*
| Specifications of a tim package.
*/
'use strict';

def.attributes =
{
	// the absolute path of the root directory of the package
	dir: { type: 'Path/Self' },

	// tim exports of the module
	exports: { type: [ 'undefined', 'Spec/Exports' ] },

	// name for this tree
	name: { type: 'string' },

	// all the tim specs
	specs: { type: 'Spec/Group', defaultValue: 'require("Spec/Group").Empty' },

	// source directory (relative to dir)
	srcDir: { type: 'Path/Self' },

	// if true generates timcode for this spec tree.
	timcodeGen: { type: 'boolean' },

	// "cjs" or "esm"
	type: { type: 'string' },
};

const _core = tim._core;
const Trace = tim.require( 'Trace/Self' );
const TraceSpecPlan = tim.require( 'Spec/Trace/Plan' );

/*
| Adds a spec.
|
| ~spec: spec to be added
*/
def.proto.addSpec =
	function( spec )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const trace = spec.trace;
/**/if( CHECK && trace.get( 1 ).key !== this.name ) throw new Error( );

	const key = trace.get( 2 ).key;
	const ts = this.specs.get( key );

	// it is already in there?
	if( ts ) throw new Error( );

	return this.create( 'specs', this.specs.set( key, spec ) );
};

/*
| Imports a tim.
|
| Interface to be used by user.
*/
def.proto.import =
	async function( required )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( required ) !== 'string' ) throw new Error( );
/**/}

	if( required.indexOf( ':' ) >= 0 ) throw new Error( );
	if( !required.endsWith( '.js' ) ) required += '.js';

	const truss = _core.addTruss( this.name, required );
	await _core.loadAsync( truss );
	await _core.coupleAsync( truss );
	await tim._prepareAsync( truss );
	return truss.runtime;
};

/*
| Readies all tims of the package.
*/
def.proto.ready =
	async function( )
{
	for( let spec of this.specs )
	{
		await spec.ready( );
	}
};

/*
| Returns the absolute path to the timcode dir
*/
def.lazy.timcodeADir =
	function( )
{
	return this.dir.dir( 'timcode' );
};

/*
| Trace for this package.
*/
def.lazy.trace =
	function( )
{
	return Trace.root( TraceSpecPlan.root ).add( 'packages', this.name );
};
