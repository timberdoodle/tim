/*
| A group of packages.
| This is all the specs in the current running instance.
*/
'use strict';

// This has to be in node.
if( !NODE ) throw new Error( );

// not a group since they are
// ordered by root directory (for proper subdirectory containment)
def.twig = [ 'Spec/Package' ];

const fs = require( 'fs' );
const _core = tim._core;

const Exports = tim.require( 'Spec/Exports' );
const Path = tim.require( 'Path/Self' );
const Package = tim.require( 'Spec/Package' );
const Spec = tim.require( 'Spec/Self' );
const Trace = tim.require( 'Trace/Self' );

/*
| Adds a tim package.
|
| ~dir:        absolute directory of the package
| ~srcDir:     relative directory of source
| ~name:       name of the package
| ~metadata:   import.meta(ESM) or module(CJS)
| ~timcodeGen: if true generates tim codes
*/
def.proto.addPackage =
	async function( dir, srcDir, name, metadata, timcodeGen )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( dir.timtype !== Path ) throw new Error( );
/**/	if( srcDir.timtype !== Path ) throw new Error( );
/**/}

	let type;
	if( metadata )
	{
		if( metadata.require ) type = 'cjs';
		else if( metadata.url ) type = 'esm';
		else throw new Error( );

		await tim._core.addPackage( name, dir.asString, metadata, type, timcodeGen );
	}
	else
	{
		// tim itself, currently cjs
		type = 'cjs';
	}

	// package name already used?
	if( this.get( name ) ) throw new Error( );

	let exports, exportsStat;
	const exportsAFile = dir.file( 'exports.json' );

	try { exportsStat = await fs.promises.stat( exportsAFile.asString ); }
	catch( e ) { /* ignore */ }

	if( exportsStat ) exports = Exports.FromFile( exportsAFile.asString );

	const pkg =
		Package.create(
			'dir', dir,
			'exports', exports,
			'name', name,
			'timcodeGen', timcodeGen,
			'srcDir', srcDir,
			'type', type,
		);

	// sorts the entries by directory
	// so subdirs come before parentdir
	let ipos = 0;
	for( let t of this )
	{
		if( dir.asString > t.dir.asString ) break;
		else ipos++;
	}

	tim.packages = tim.packages.create( 'twig:insert', name, ipos, pkg );
	return pkg;
};

/*
| Creates and adds a spec to a package.
| Determines the correct package to add it to.
|
| ~spec: spec to add
*/
def.proto.addSpec =
	function( spec )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const pkg = this.getPackageOfDir( spec.afile.back );
	tim.packages = this.set( pkg.name, pkg.addSpec( spec ) );
};

/*
| Returns the tree initialization code
| to be used in the browser.
*/
def.proto.getBrowserInitCode =
	function( )
{
	let text = 'var _packages = {\n';
	for( let t = 0; t < tim.packages.length; t++ )
	{
		const pkg = tim.packages.atRank( t );
		// in case the name has a dot, single quotes are put around it
		const name = pkg.name;
		const sq = name.indexOf( '.' ) >= 0 ? '\'' : '';
		text += '\t' + sq + name + sq + ': {\n';
		const exports = pkg.exports;
		if( exports && exports.size )
		{
			text += '\t\texports : {\n';
			for( let key of exports.keys )
			{
				const sq = key.match( /[/.]/ ) ? '\'' : '';
				text += '\t\t\t' + sq + key + sq + ' : ';
				text += '\'' + exports.get( key ) + '\',\n';
			}
			text += '\t\t},\n';
		}
		text += '\t\tentries : {\n';
		for( let key of pkg.specs.keys )
		{
			// in case the name has a dot single quotes are put around it
			const sq = key.indexOf( '.' ) >= 0 ? '\'' : '';
			text += '\t\t\t' + sq + key + sq + ': { runtime: { }, },\n';
		}
		text += '\t\t},\n';
		text += '\t},\n';
	}
	text += '};\n';
	return text;
};

/*
| Returns the spec for a given path.
|
| ~path: the path
*/
def.proto.getByPath =
	function( path )
{
/**/if( CHECK )
/**/{
/**/	if( path.timtype !== Path ) throw new Error( );
/**/	if( !path.isAbsolute ) throw new Error( );
/**/}

	const pkg = tim.packages.getPackageOfDir( path.back );
	let s = '';
	// TODO path.chopRoot.asString
	for( let a = pkg.dir.length + 1, al = path.length; a < al; a++ )
	{
		if( s !== '' ) s += '/';
		s += path.get( a ).key;
	}
	return pkg.specs.get( s );
};


/*
| Gets the filename from a trace.
| TODO remove.
*/
def.proto.getFilename =
	function( trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( trace.timtype !== Trace ) throw new Error( );
/**/	if( this !== tim.packages ) throw new Error( );
/**/}

	const pkg = this.get( trace.get( 1 ).key );
	return pkg.dir.asString + 'src/' + trace.get( 2 ).key;
};

/*
| Returns the tim package that has the absolute dir.
*/
def.proto.getPackageOfDir =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir.timtype !== Path ) throw new Error( );
/**/}

	for( let pkg of this ) if( dir.startsWith( pkg.dir ) ) return pkg;
	// if nothing hits returns undefined
};

/*
| Gets an entry.
|
| ~trace: trace of the spec to get.
*/
def.proto.getSpec =
	function( trace )
{
/**/if( CHECK && trace.timtype !== Trace ) throw new Error( );

	const pkgname = trace.get( 1 ).key;
	const timname = trace.get( 2 ).key;
	return this.get( pkgname ).specs.get( timname );
};

/*
| Returns the spec.
| Loads it if it isn't already loaded.
|
| ~trace: trace of the spec to load/return
*/
def.proto.loadSpecAsync =
	async function( trace )
{
	const filename = tim.packages.getFilename( trace );

	let spec = tim.packages.getSpec( trace );
	if( spec ) return spec;

	let truss = _core.getTruss( trace.get( 1 ).key, trace.get( 2 ).key );
	if( !truss ) truss = _core.addTruss( trace.get( 1 ).key, trace.get( 2 ).key );
	if( !truss.ready )
	{
		await _core.loadAsync( truss );
		await _core.coupleAsync( truss );
		await tim._prepareAsync( truss );
	}
	spec = tim.packages.getSpec( trace );
	if( spec ) return spec;

	const afile = Path.FromString( filename );
	spec = Spec.FromDef( truss.def, afile, trace, truss.requires );
	tim.packages.addSpec( spec );
	return spec;
};

/*
| Returns the spec.
| Loads it if it isn't already loaded.
|
| ~trace: trace of the spec to load/return
*/
def.proto.loadSpecSync =
	function( trace )
{
	const filename = tim.packages.getFilename( trace );

	let spec = tim.packages.getSpec( trace );
	if( spec ) return spec;

	let truss = _core.getTruss( trace.get( 1 ).key, trace.get( 2 ).key );
	if( !truss ) truss = _core.addTruss( trace.get( 1 ).key, trace.get( 2 ).key );
	if( !truss.ready )
	{
		_core.loadSync( truss );
		_core.coupleSync( truss );
		tim._prepareSync( truss );
	}
	spec = tim.packages.getSpec( trace );
	if( spec ) return spec;

	const afile = Path.FromString( filename );
	spec = Spec.FromDef( truss.def, afile, trace, truss.requires );
	tim.packages.addSpec( spec );
	return spec;
};

/*
| Readies all imported tims.
*/
def.proto.ready =
	async function( )
{
	for( let pkg of this )
	{
		await pkg.ready( );
	}
};
