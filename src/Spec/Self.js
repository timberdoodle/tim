/*
| Specifications of a tim.
*/
'use strict';

def.attributes =
{
	// true if this tim cannot be instanced itself
	abstract: { type: 'boolean' },

	// absolute path to defining file name
	afile: { type: 'Path/Self' },

	// list of alike function and what to ignore
	alike: { type: [ 'undefined', 'protean' ] },

	// if this is a table the allowed attributes
	attributes: { type: [ 'Spec/Attribute/Group' ] },

	// true if an init checker is to be called
	check: { type: 'boolean' },

	// 'group', 'list', 'set', 'table' or 'twig'
	collectionType: { type: 'string' },

	// for now only a rename of the default creator possible
	creator: { type: 'string' },

	// true if this tim has a custom from JSON converter.
	customFromJson: { type: 'boolean' },

	// true if this tim has a custom jsonfy converter.
	customJsonfy: { type: 'boolean' },

	// if set extends this tim
	extend: { type: [ 'undefined', 'Type/Tim' ] },

	// if set extends this tim
	extendSpec: { type: [ 'undefined', 'Spec/Self' ] },

	// potentially extra/custom FromJson arguments
	fromJsonArgs: { type: [ 'undefined', 'string/list' ] },

	// true if this tim has lazy values
	hasLazy: { type: 'boolean' },

	// other tims this tim utilizes
	imports: { type: 'Type/Set' },

	// allowed items in case of group/list/set/twig
	items: { type: [ 'undefined', 'Type/Set' ] },

	// json name of this tim, or base spec to forward to
	json: { type: [ 'undefined', 'string', 'Spec/Self' ] },

	// stuff required in this tim implementation
	requires: { type: 'string/set' },

	// if set, actualizes a global variable to the last created tim
	setGlobal: { type: [ 'undefined', 'Ast/Var' ] },

	// true if this a singleton
	// (no attributes or group/list/set/twig, not abstract)
	singleton: { type: 'boolean' },

	// trace of the spec
	trace: { type: 'Trace/Self' },
};

const $ = tim.require( 'Ast/Shorthand' );
const AstString = tim.require( 'Ast/String' );
const Attribute = tim.require( 'Spec/Attribute/Self' );
const AttributeGroup = tim.require( 'Spec/Attribute/Group' );
const Call = tim.require( 'Ast/Call' );
const StringList = tim.require( 'string/list' );
const StringSet = tim.require( 'string/set' );
const TypeSet = tim.require( 'Type/Set' );
const TypeTim = tim.require( 'Type/Tim' );
const Validator = tim.require( 'Spec/Validator' );
const Var = tim.require( 'Ast/Var' );

const _core = tim._core;

/*
| Returns true if obj isn't empty.
*/
const isntEmpty =
	function( obj )
{
	for( let _ in obj ) return true;
	return false;
};

/*
| Creates a spec from the def protean.
|
| ~def:      the created def
| ~afile:    absolute file path
| ~trace:    trace of the spec
| ~requires: the collected requires
*/
def.static.FromDef =
	function( def, afile, trace, requires )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	Validator.check( def );
	let creator = def.create ? def.create[ 0 ] : 'create';
	const abstract = !!def.abstract;
	// in case of attributes, group, list, set or twig
	// it will be turned off again
	let singleton = true;
	let attributes = AttributeGroup.create( );
	let setGlobal;
	// foreign timtypes to be imported
	let imports = TypeSet.create( );
	let items;
	let extend, extendSpec;
	let fromJsonArgs;
	let collectionType;
	const basePackage = tim.packages.get( trace.get( 1 ).key );

	const customFromJson = def.static.FromJson !== undefined;

	if( def.extend )
	{
		extend = TypeTim.FromString( def.extend, basePackage );
		extendSpec = tim.packages.loadSpecSync( extend.trace );
		if( extendSpec.timtype !== Self ) throw new Error( );
	}

	// walker to transform default value
	// replaces require( 'path' ) with the import variable
	const transformDefaultValue =
		function( node )
	{
		if( node.timtype !== Call ) return node;
		const func = node.func;
		if( func.timtype !== Var ) return node;
		if( func.name !== 'require' ) return node;
		const args = node.args;
		if( args.length !== 1 )
		{
			throw new Error( 'require in defaultValue must have one argument' );
		}
		// require path argument
		const rpa = args.get( 0 );
		if( rpa.timtype !== AstString )
		{
			throw new Error( 'require argument in defaultValue must be string' );
		}
		const type = TypeTim.FromString( rpa.string, basePackage );
		imports = imports.add( type );
		return type.$varname;
	};

	for( let name in def.attributes || { } )
	{
		singleton = false;
		const attr = Attribute.FromDef( def, name, basePackage, extendSpec, transformDefaultValue );
		attributes = attributes.set( name, attr );
		if( !abstract ) imports = imports.addSet( attr.types );
		const jsonTypes = attr.jsonTypes;
		if( jsonTypes && !customFromJson )
		{
			for( let jt of jsonTypes ) imports = imports.add( jt );
		}
	}

	if( extendSpec )
	{
		const exAttributes = extendSpec.attributes;
		for( let name of exAttributes.keys )
		{
			if( attributes.get( name ) ) continue;
			const attr = exAttributes.get( name );
			if( !abstract )
			{
				const types = attr.types;
				if( types.timtype === TypeSet ) imports = imports.addSet( types );
				else imports = imports.add( types );
			}
			attributes = attributes.set( name, attr );
		}
	}

	if( def.global )
	{
		setGlobal = $.$var( def.global );
	}

	if( extendSpec )
	{
		collectionType = extendSpec.collectionType;
		items = extendSpec.items;
	}

	if( def.group )
	{
		collectionType = 'group';
		items = TypeSet.Array( def.group, basePackage );
	}
	else if( def.list )
	{
		collectionType = 'list';
		items = TypeSet.Array( def.list, basePackage );
	}
	else if( def.set )
	{
		collectionType = 'set';
		items = TypeSet.Array( def.set, basePackage );
	}
	else if( def.twig )
	{
		collectionType = 'twig';
		items = TypeSet.Array( def.twig, basePackage );
	}
	else
	{
		if( !collectionType )
		{
			collectionType = 'table';
		}
	}

	if( !abstract && items ) imports = imports.addSet( items );
	if( abstract || collectionType !== 'table' ) singleton = false;

	// if extending a non-singleton this can't be one either
	if(
		singleton
		&& extendSpec
		&& (
			extendSpec.attributes.size > 0
			|| extendSpec.collectionType !== 'table'
		)
	) singleton = false;

	const hasLazy =
		!!( extendSpec?.hasLazy )
		|| ( collectionType !== 'table' )
		|| ( !!def.json )
		|| isntEmpty( def.lazy )
		|| isntEmpty( def.lazyFunc );

	if( singleton && !def.singleton ) throw new Error( 'this is a singleton' );
	if( !singleton && def.singleton ) throw new Error( 'this is not a singleton' );
	if( def.abstract && def.singleton ) throw new Error( 'abstract and singleton are exclusive' );
	if( singleton && def.creator ) throw new Error( 'singletons cannot have creators' );

	if( singleton ) creator = '_create';

	const customJsonfy = def.lazyFunc.jsonfy !== undefined;
	let json = def.json;
	if( json )
	{
		// gets additional imports from json forwarders
		if( json.indexOf( '/' ) >= 0 )
		{
			const type = TypeTim.FromString( def.json, trace.back );
			json = tim.packages.loadSpecSync( type.trace );
		}

		if( def.fromJsonArgs )
		{
			if( def.fromJsonArgs[ 0 ] !== 'json' )
				throw new Error( 'first fromJsonArg has to be "json"' );
			fromJsonArgs = StringList.Array( def.fromJsonArgs );
		}
		else { fromJsonArgs = StringList.Elements( 'json' ); }

		if(
			!customJsonfy && (
				attributes.hasSubJsonfy
				|| ( items && ( items.composition === 'tims' || items.composition === 'mixed' ) )
			)
		)
		{
			// NOTE: never ouroboros: package since generated will always refer to tim:
			imports = imports.add( TypeTim.FromString( 'tim:Jsonfy/Spacing', basePackage ) );
		}
	}

	const spec =
		Self.create(
			'abstract', abstract,
			'alike', def.alike,
			'afile', afile,
			'attributes', attributes,
			'check', !!def.proto._check,
			'collectionType', collectionType,
			'creator', creator,
			'customFromJson', customFromJson,
			'customJsonfy', customJsonfy,
			'fromJsonArgs', fromJsonArgs,
			'extend', extend,
			'extendSpec', extendSpec,
			'setGlobal', setGlobal,
			'hasLazy', hasLazy,
			'imports', imports,
			'items', items,
			'json', json,
			'requires', StringSet.Protean( requires ),
			'singleton', singleton,
			'trace', trace,
		);

	spec._validate( def );
	return spec;
};

/*
| Realpath of the tim definer.
*/
def.lazy.filename =
	function( )
{
	return this.afile.asString;
};

/*
| Returns the preamble to be prepended
| to sources for browser mode.
|
| ~timcode: true if this is for timcode
| ~esm: true if if this is an esm module
*/
def.proto.getBrowserPreamble =
	function( timcode, esm )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	const trace = this.trace;

	/* eslint-disable quotes */
	if( !esm || timcode )
	{
		return(
			'var root;tim._load('
			+ "'" + trace.get( 1 ).key + "',"
			+ "'" + trace.get( 2 ).key + "',"
			+ ( timcode ? 'true' : 'false' ) + ','
			+ '( def, Self )=>{'
		);
	}
	else
	{
		return(
			'var root;'
			+ 'const _truss = tim._loadESM1('
				+ "'" + trace.get( 1 ).key + "',"
				+ "'" + trace.get( 2 ).key + "'"
			+ ');'
			+ 'const def = _truss.def;'
			+ 'const Self = _truss.runtime;'
		);
	}
	/* eslint-enable */
};

/*
| Returns the postamble to be append
| to sources for browser mode.
|
| ~timcode: true if this is for timcode
| ~esm: true if if this is an esm module
*/
def.proto.getBrowserPostamble =
	function( timcode, esm )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( !esm || timcode )
	{
		return( '} );' );
	}
	else
	{
		return(
			'\n'
			+ 'tim._loadESM2( _truss );\n'
			+ 'export default Self;\n'
		);
	}
};

/*
| Returns the absolute file path to timcode.
*/
def.proto.getTimcodeAFile =
	function( )
{
	const trace = this.trace;
	const pkg = tim.packages.get( trace.get( 1 ).key );
	return pkg.timcodeADir.file( trace.get( 2 ).key.replace( /\//g, '-' ) );
};

/*
| The base hash for this timtype
*/
def.lazy.hash =
	function( )
{
	return tim._hashArgs( 0, this.afile.asString );
};

/*
| Readies the spec.
*/
def.proto.ready =
	async function( )
{
	const trace = this.trace;
	const truss = _core.getTruss( trace.get( 1 ).key, trace.get( 2 ).key );
	await tim._readyTrussAsync( truss );
};

/*
| Attributes must not be named like these.
*/
def.static._validateAttributeBlacklist =
	Object.freeze( { create: true } );

/*
| Checks if a tim attribute definition looks ok.
|
| ~def: the tim definition
| ~name: the attribute name
*/
def.static._validateAttribute =
	function( def, name )
{
	if( Self._validateAttributeBlacklist[ name ] )
		throw new Error( 'attribute must not be named "' + name + '"' );
};

/*
| Throws an error if something doesn't seem right
| with the spec. This part of the validation is done
| after the spec is created.
|
| ~def: the definition
*/
def.proto._validate =
	function( def )
{
	const attr = def.attributes;
	if( attr )
	{
		for( let name in attr ) Self._validateAttribute( def, name );
	}

	if(
		def.proto.jsonfy !== undefined
		|| def.static.jsonfy !== undefined
		|| def.staticLazy.jsonfy !== undefined
		|| def.lazy.jsonfy !== undefined
	) throw new Error( '"jsonfy" must be lazyFunc' );

	if(
		def.proto.FromJson !== undefined
		|| def.lazy.FromJson !== undefined
		|| def.staticLazy.FromJson !== undefined
		|| def.lazyFunc.FromJson !== undefined
	) throw new Error( '"FromJson" must be static' );
};
