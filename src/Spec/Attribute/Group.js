/*
| An attribute description of a tim object.
*/
'use strict';

def.group = [ 'Spec/Attribute/Self' ];

/*
| True if at least one attribute group is to be jsonfied and has a tim type.
*/
def.lazy.hasSubJsonfy =
	function( )
{
	for( let attr of this )
	{
		if( !attr.json ) continue;
		for( let type of attr.types )
		{
			if( !type.isPrimitive ) return true;
		}
	}
	return false;
};
