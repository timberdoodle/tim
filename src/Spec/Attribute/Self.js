/*
| An attribute description of a tim object.
*/
'use strict';

def.attributes =
{
	// variable name to assign to
	assign: { type: 'string' },

	// default value
	defaultValue: { type: [ 'undefined', '< Ast/Type/Expr' ] },

	// create JSON converters
	json: { type: 'boolean', defaultValue : 'false' },

	// a mapping of types for forwarded json converters
	// TODO remove
	jsonTypes: { type: [ 'undefined', 'Type/Group' ] },

	// attribute name
	name: { type: 'string' },

	// attribute types
	types: { type: 'Type/Set' },

	// attribute variable used in generate
	varRef: { type: 'Ast/Var' },
};

const Shorthand = tim.require( 'Ast/Shorthand' );
const Package = tim.require( 'Spec/Package' );
const TypeAny = tim.require( 'Type/Any' );
const TypeGroup = tim.require( 'Type/Group' );
const TypeSet = tim.require( 'Type/Set' );
const TypeTim = tim.require( 'Type/Tim' );

/*
| The name as ast string.
*/
def.lazy.$name = function( ) { return Shorthand.$string( this.name ); };

/*
| Creates the attribute spec from the definition.
|
| ~def:                   the defintion to create from
| ~name:                  name of the attribute to create
| ~basePackage:               base package
| ~extendSpec:            if defined extends this spec
| ~transformDefaultValue: walker to transform default values
*/
def.static.FromDef =
	function( def, name, basePackage, extendSpec, transformDefaultValue )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( typeof( name ) !== 'string' ) throw new Error( );
/**/	if( basePackage.timtype !== Package ) throw new Error( );
/**/}

	const jAttr = def.attributes[ name ];
	const jType = jAttr.type;
	let types;

	// TODO combines this two in TypeSet
	if( !Array.isArray( jType ) )
	{
		const type = TypeAny.FromString( jType, basePackage );
		types = TypeSet.create( 'set:add', type );
	}
	else
	{
		types = TypeSet.Array( jType, basePackage );
	}

	// TODO remove this?
	const assign = name;
	const jdv = jAttr.defaultValue;

	let defaultValue;
	if( jdv ) defaultValue = Shorthand.$expr( jdv ).walk( transformDefaultValue );

	let jsonTypes;
	if( jAttr.json )
	{
		for( let type of types )
		{
			if( type.timtype !== TypeTim ) continue;
			const jsonType = type.spec.json;
			if( jsonType.indexOf( '/' ) < 0 ) continue;
			const jType = TypeTim.FromString( jsonType, type.trace.back );
			if( !jsonTypes ) jsonTypes = TypeGroup.create( );
			jsonTypes = jsonTypes.create( 'group:set', type.trace.getAFile( ).asString, jType );
		}
	}
	return(
		Self.create(
			'assign', assign,
			'defaultValue', defaultValue,
			'types', types,
			'json', !!jAttr.json,
			'jsonTypes', jsonTypes,
			'name', name,
			'varRef', Shorthand.$var( 'v_' + name )
		)
	);
};
