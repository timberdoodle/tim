/*
| A twig of spec's (an ordered group).
*/
'use strict';

def.twig = [ 'Spec/Self' ];

const Path = tim.require( 'Path/Self' );
const Spec = tim.require( 'Spec/Self' );
const TypeTim = tim.require( 'Type/Tim' );

/*
| Recursive helper for DependencyWalk
|
| twig: the twig being built
| spec: spec currently been inspected
| parents: a twig of parents already walked (the open circles)
*/
const dependencyWalkHelper =
	async function( twig, spec, parents )
{
	const filename = spec.filename;
	if( twig.get( spec.filename ) || parents.get( spec.filename ) )
	{
		return twig;
	}
	parents = parents.create( 'twig:add', filename, spec );
	const imports = spec.imports;

	if( imports )
	{
		for( let type of imports )
		{
			if( type.timtype !== TypeTim ) continue;
			const ts = await tim.packages.loadSpecAsync( type.trace );
			twig = await dependencyWalkHelper( twig, ts, parents );
		}
	}

	for( let filename of spec.requires )
	{
		const basePackage = tim.packages.get( spec.trace.get( 1 ).key );
		const type = TypeTim.FromString( filename, basePackage );
		const ts = await tim.packages.loadSpecAsync( type.trace );
		twig = await dependencyWalkHelper( twig, ts, parents );
	}

	if( spec.extend )
	{
		const type = spec.extend;
		const ts = await tim.packages.loadSpecAsync( type.trace );
		twig = await dependencyWalkHelper( twig, ts, parents );
	}

	return twig = twig.create( 'twig:add', filename, spec );
};

/*
| Creates the spec twig by dependency walking a spec
|
| ~entry: spec to start the walk with
*/
def.static.DependencyWalk =
	async function( entry )
{

/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( entry.timtype !== Spec ) throw new Error( );
/**/}

	let walk = await dependencyWalkHelper( Self.Empty, entry, Self.Empty );

	// sorts the walk by inheritance making sure extended
	// tims come first.
	let restart = false;

	do
	{
		restart = false;
		for( let a = 0, alen = walk.length; a < alen; a++ )
		{
			const filename = walk.getKey( a );
			const ts = tim.packages.getByPath( Path.FromString( filename ) );
			const extendSpec = ts.extendSpec;
			if( !extendSpec ) continue;
			const b = walk.rankOf( extendSpec.filename );

			// this shouldn't happen
			if( b < 0 ) throw new Error( );
			if( b > a )
			{
				walk =
					walk
					.create( 'twig:remove', extendSpec.filename )
					.create( 'twig:insert', extendSpec.filename, a, extendSpec );

				restart = true;
			}
		}
	} while( restart );

	return walk;
};
