/*
| A group of exports
*/
'use strict';

def.group = [ 'string' ];

const fs = require( 'fs' );

/*
| Creates it from a "exports.json" file
*/
def.static.FromFile =
	function( filename )
{
	const text = fs.readFileSync( filename );
	const group = JSON.parse( text );
	return Self.Table( group );
};
