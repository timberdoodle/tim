/*
| A list of numbers.
*/
'use strict';

def.list = [ 'number' ];
def.json = 'NumberList';

/*
| The StringList joined into one string.
|
| ~sep: seperator to join.
*/
def.lazyFunc.join = function( sep ) { return this._list.join( sep ); };
