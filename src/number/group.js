/*
| A group of numbers.
*/
'use strict';

def.group = [ 'number' ];
def.json = 'NumberGroup';
