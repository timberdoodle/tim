/*
| A set of numbers.
*/
'use strict';

def.set = [ 'number' ];
def.json = 'NumberSet';

const NumberList = tim.require( 'number/list' );

/*
| Creates the set from an array.
*/
def.static.Array =
	function( array )
{
	return Self.create( 'set:init', new Set( array ) );
};

/*
| Creates the set from a protean's keys.
*/
def.static.Protean =
	function( protean )
{
	return Self.create( 'set:init', new Set( Object.keys( protean ) ) );
};

/*
| The set as sorted list.
| TODO autogenerate this
*/
def.lazy.list =
	function( )
{
	const a = Array.from( this._set );
	a.sort( );
	return NumberList.Array( a );
};
