/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Plan_Piece_Subs = tim.require( 'Plan/Piece/Subs.js' );
const tt_tim_protean_group = tim.require( 'protean/group.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_at, v_key, v_name, v_subs, v_types )
{
	this.__hash = hash;
	this.at = v_at;
	this.key = v_key;
	this.name = v_name;
	this.subs = v_subs;
	this.types = v_types;
	Object.freeze( this );
/**/if( CHECK ) { this._check( ); }
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1595743668;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Plan/Piece/Self.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_at;
	let v_key;
	let v_name;
	let v_subs;
	let v_types;
	if( this !== Self )
	{
		v_at = this.at;
		v_key = this.key;
		v_name = this.name;
		v_subs = this.subs;
		v_types = this.types;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'at':
				if( arg !== pass ) { v_at = arg; }
				break;
			case 'key':
				if( arg !== pass ) { v_key = arg; }
				break;
			case 'name':
				if( arg !== pass ) { v_name = arg; }
				break;
			case 'subs':
				if( arg !== pass ) { v_subs = arg; }
				break;
			case 'types':
				if( arg !== pass ) { v_types = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_at === undefined ) { v_at = false; }
	if( v_key === undefined ) { v_key = false; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_at ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_key ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_subs !== undefined && v_subs.timtype !== tt_tim_Plan_Piece_Subs )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_types !== undefined && v_types.timtype !== tt_tim_protean_group )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 1595743668, v_at, v_key, v_name, v_subs, v_types );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_at === e.at && v_key === e.key && v_name === e.name && v_subs === e.subs
			&& v_types === e.types
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_at, v_key, v_name, v_subs, v_types );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
