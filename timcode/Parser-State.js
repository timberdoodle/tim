/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_Block = tim.require( 'Ast/Block.js' );
const tt_tim_Ast_Break = tim.require( 'Ast/Break.js' );
const tt_tim_Ast_Check = tim.require( 'Ast/Check.js' );
const tt_tim_Ast_Comment = tim.require( 'Ast/Comment.js' );
const tt_tim_Ast_Const = tim.require( 'Ast/Const.js' );
const tt_tim_Ast_Continue = tim.require( 'Ast/Continue.js' );
const tt_tim_Ast_DestructDecl = tim.require( 'Ast/DestructDecl.js' );
const tt_tim_Ast_DoWhile = tim.require( 'Ast/DoWhile.js' );
const tt_tim_Ast_For = tim.require( 'Ast/For.js' );
const tt_tim_Ast_ForIn = tim.require( 'Ast/ForIn.js' );
const tt_tim_Ast_ForOf = tim.require( 'Ast/ForOf.js' );
const tt_tim_Ast_If = tim.require( 'Ast/If.js' );
const tt_tim_Ast_Let = tim.require( 'Ast/Let.js' );
const tt_tim_Ast_Return = tim.require( 'Ast/Return.js' );
const tt_tim_Ast_Sep = tim.require( 'Ast/Sep.js' );
const tt_tim_Ast_Switch = tim.require( 'Ast/Switch.js' );
const tt_tim_Ast_Throw = tim.require( 'Ast/Throw.js' );
const tt_tim_Ast_Try = tim.require( 'Ast/Try.js' );
const tt_tim_Ast_VarDec = tim.require( 'Ast/VarDec.js' );
const tt_tim_Ast_While = tim.require( 'Ast/While.js' );
const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );
const tt_tim_Parser_TokenList = tim.require( 'Parser/TokenList.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_ast, v_pos, v_tokens )
{
	this.__lazy = { };
	this.__hash = hash;
	this.ast = v_ast;
	this.pos = v_pos;
	this.tokens = v_tokens;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1326380421;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Parser/State.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_ast;
	let v_pos;
	let v_tokens;
	if( this !== Self ) { v_ast = this.ast; v_pos = this.pos; v_tokens = this.tokens; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'ast':
				if( arg !== pass ) { v_ast = arg; }
				break;
			case 'pos':
				if( arg !== pass ) { v_pos = arg; }
				break;
			case 'tokens':
				if( arg !== pass ) { v_tokens = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if(
/**/		v_ast !== undefined && v_ast.timtype !== tt_tim_Ast_Block
/**/		&& v_ast.timtype !== tt_tim_Ast_Break
/**/		&& v_ast.timtype !== tt_tim_Ast_Check
/**/		&& v_ast.timtype !== tt_tim_Ast_Comment
/**/		&& v_ast.timtype !== tt_tim_Ast_Const
/**/		&& v_ast.timtype !== tt_tim_Ast_Continue
/**/		&& v_ast.timtype !== tt_tim_Ast_DestructDecl
/**/		&& v_ast.timtype !== tt_tim_Ast_DoWhile
/**/		&& v_ast.timtype !== tt_tim_Ast_For
/**/		&& v_ast.timtype !== tt_tim_Ast_ForIn
/**/		&& v_ast.timtype !== tt_tim_Ast_ForOf
/**/		&& v_ast.timtype !== tt_tim_Ast_If
/**/		&& v_ast.timtype !== tt_tim_Ast_Let
/**/		&& v_ast.timtype !== tt_tim_Ast_Return
/**/		&& v_ast.timtype !== tt_tim_Ast_Sep
/**/		&& v_ast.timtype !== tt_tim_Ast_Switch
/**/		&& v_ast.timtype !== tt_tim_Ast_Throw
/**/		&& v_ast.timtype !== tt_tim_Ast_Try
/**/		&& v_ast.timtype !== tt_tim_Ast_VarDec
/**/		&& v_ast.timtype !== tt_tim_Ast_While
/**/		&& v_ast.timtype !== tt_tim_Ast_And
/**/		&& v_ast.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_ast.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_ast.timtype !== tt_tim_Ast_Assign
/**/		&& v_ast.timtype !== tt_tim_Ast_Await
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_ast.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_Boolean
/**/		&& v_ast.timtype !== tt_tim_Ast_Call
/**/		&& v_ast.timtype !== tt_tim_Ast_Comma
/**/		&& v_ast.timtype !== tt_tim_Ast_Condition
/**/		&& v_ast.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_ast.timtype !== tt_tim_Ast_Delete
/**/		&& v_ast.timtype !== tt_tim_Ast_Differs
/**/		&& v_ast.timtype !== tt_tim_Ast_Divide
/**/		&& v_ast.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_Dot
/**/		&& v_ast.timtype !== tt_tim_Ast_Equals
/**/		&& v_ast.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_ast.timtype !== tt_tim_Ast_Generator
/**/		&& v_ast.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_ast.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_ast.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_ast.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_ast.timtype !== tt_tim_Ast_LessThan
/**/		&& v_ast.timtype !== tt_tim_Ast_Member
/**/		&& v_ast.timtype !== tt_tim_Ast_Minus
/**/		&& v_ast.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_Multiply
/**/		&& v_ast.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_Negate
/**/		&& v_ast.timtype !== tt_tim_Ast_New
/**/		&& v_ast.timtype !== tt_tim_Ast_Not
/**/		&& v_ast.timtype !== tt_tim_Ast_Null
/**/		&& v_ast.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_ast.timtype !== tt_tim_Ast_Number
/**/		&& v_ast.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_ast.timtype !== tt_tim_Ast_Or
/**/		&& v_ast.timtype !== tt_tim_Ast_Plus
/**/		&& v_ast.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_ast.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_ast.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_ast.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_ast.timtype !== tt_tim_Ast_Regex
/**/		&& v_ast.timtype !== tt_tim_Ast_Remainder
/**/		&& v_ast.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_ast.timtype !== tt_tim_Ast_String
/**/		&& v_ast.timtype !== tt_tim_Ast_Typeof
/**/		&& v_ast.timtype !== tt_tim_Ast_Undefined
/**/		&& v_ast.timtype !== tt_tim_Ast_Var
/**/		&& v_ast.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		typeof( v_pos ) !== 'number' || Number.isNaN( v_pos ) || Math.floor( v_pos ) !== v_pos
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_tokens.timtype !== tt_tim_Parser_TokenList ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 1326380421, v_ast, v_pos, v_tokens );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if( v_ast === e.ast && v_pos === e.pos && v_tokens === e.tokens ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_ast, v_pos, v_tokens );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
