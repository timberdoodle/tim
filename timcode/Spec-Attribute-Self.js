/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );
const tt_tim_Type_Group = tim.require( 'Type/Group.js' );
const tt_tim_Type_Set = tim.require( 'Type/Set.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_assign, v_defaultValue, v_json, v_jsonTypes, v_name, v_types, v_varRef )
{
	this.__lazy = { };
	this.__hash = hash;
	this.assign = v_assign;
	this.defaultValue = v_defaultValue;
	this.json = v_json;
	this.jsonTypes = v_jsonTypes;
	this.name = v_name;
	this.types = v_types;
	this.varRef = v_varRef;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -254252160;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Spec/Attribute/Self.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_assign;
	let v_defaultValue;
	let v_json;
	let v_jsonTypes;
	let v_name;
	let v_types;
	let v_varRef;
	if( this !== Self )
	{
		v_assign = this.assign;
		v_defaultValue = this.defaultValue;
		v_json = this.json;
		v_jsonTypes = this.jsonTypes;
		v_name = this.name;
		v_types = this.types;
		v_varRef = this.varRef;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'assign':
				if( arg !== pass ) { v_assign = arg; }
				break;
			case 'defaultValue':
				if( arg !== pass ) { v_defaultValue = arg; }
				break;
			case 'json':
				if( arg !== pass ) { v_json = arg; }
				break;
			case 'jsonTypes':
				if( arg !== pass ) { v_jsonTypes = arg; }
				break;
			case 'name':
				if( arg !== pass ) { v_name = arg; }
				break;
			case 'types':
				if( arg !== pass ) { v_types = arg; }
				break;
			case 'varRef':
				if( arg !== pass ) { v_varRef = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_json === undefined ) { v_json = false; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_assign ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_defaultValue !== undefined && v_defaultValue.timtype !== tt_tim_Ast_And
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Assign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Await
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Boolean
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Call
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Comma
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Condition
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Delete
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Differs
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Divide
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Dot
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Equals
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Generator
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_LessThan
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Member
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Minus
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Multiply
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Negate
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_New
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Not
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Null
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Number
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Or
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Plus
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Regex
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Remainder
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_String
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Typeof
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Undefined
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Var
/**/		&& v_defaultValue.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_json ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_jsonTypes !== undefined && v_jsonTypes.timtype !== tt_tim_Type_Group )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_types.timtype !== tt_tim_Type_Set ) { throw ( new Error( ) ); }
/**/	if( v_varRef.timtype !== tt_tim_Ast_Var ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash =
		tim._hashArgs(
			-254252160,
			v_assign,
			v_defaultValue,
			v_json,
			v_jsonTypes,
			v_name,
			v_types,
			v_varRef
		);
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_assign === e.assign && v_defaultValue === e.defaultValue && v_json === e.json
			&& v_jsonTypes === e.jsonTypes
			&& v_name === e.name
			&& v_types === e.types
			&& v_varRef === e.varRef
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim =
		new Constructor(
			hash,
			v_assign,
			v_defaultValue,
			v_json,
			v_jsonTypes,
			v_name,
			v_types,
			v_varRef
		);
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
