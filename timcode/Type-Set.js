/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Type_Arbitrary = tim.require( 'Type/Arbitrary.js' );
const tt_tim_Type_Boolean = tim.require( 'Type/Boolean.js' );
const tt_tim_Type_Date = tim.require( 'Type/Date.js' );
const tt_tim_Type_Function = tim.require( 'Type/Function.js' );
const tt_tim_Type_Integer = tim.require( 'Type/Integer.js' );
const tt_tim_Type_Null = tim.require( 'Type/Null.js' );
const tt_tim_Type_Number = tim.require( 'Type/Number.js' );
const tt_tim_Type_Protean = tim.require( 'Type/Protean.js' );
const tt_tim_Type_Undefined = tim.require( 'Type/Undefined.js' );
const tt_tim_Type_String = tim.require( 'Type/String.js' );
const tt_tim_Type_Tim = tim.require( 'Type/Tim.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, set )
{
	this.__lazy = { };
	this.__hash = hash;
	this._set = set;
	Object.freeze( this, set );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1063860830;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Type/Set.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let set;
	let setDup;
	if( this !== Self )
	{
		set = this._set;
		setDup = false;
	}
	else
	{
		set = new Set( );
		setDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'set:add':
				if( !setDup ) { set = new Set( set ); setDup = true; }
				set.add( arg, args[ a + 1 ] );
				break;
			case 'set:init':
/**/			if( CHECK ) { if( !( arg instanceof Set ) ) { throw ( new Error( ) ); } }
				set = arg;
				setDup = true;
				break;
			case 'set:remove':
				if( !setDup ) { set = new Set( set ); setDup = true; }
				set.delete( arg );
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let v of set )
/**/	{
/**/		if(
/**/			v.timtype !== tt_tim_Type_Arbitrary && v.timtype !== tt_tim_Type_Boolean
/**/			&& v.timtype !== tt_tim_Type_Date
/**/			&& v.timtype !== tt_tim_Type_Function
/**/			&& v.timtype !== tt_tim_Type_Integer
/**/			&& v.timtype !== tt_tim_Type_Null
/**/			&& v.timtype !== tt_tim_Type_Number
/**/			&& v.timtype !== tt_tim_Type_Protean
/**/			&& v.timtype !== tt_tim_Type_Undefined
/**/			&& v.timtype !== tt_tim_Type_String
/**/			&& v.timtype !== tt_tim_Type_Tim
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashSet( 1063860830, set );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const eset = e._set;
		if( set.size !== eset.size ) { continue; }
		let eq = true;
		for( let e of set ) { if( !eset.has( e ) ) { eq = false; break; } }
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, set );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the set with one element added.
*/
prototype.add = tim._proto.setAdd;

/*
| Returns the set with another set added.
*/
prototype.addSet = tim._proto.setAddSet;

/*
| Returns a clone primitive.
*/
prototype.clone = function( ) { return new Set( this._set ); };

/*
| Returns true if the set has an element.
*/
prototype.has = tim._proto.setHas;

/*
| Returns the set with one element removed.
*/
prototype.remove = tim._proto.setRemove;

/*
| Returns the size of the set.
*/
tim._proto.lazyValue( prototype, 'size', tim._proto.setSize );

/*
| Returns the one and only element or the set if size != 1.
*/
tim._proto.lazyValue( prototype, 'trivial', tim._proto.setTrivial );

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._set[ Symbol.iterator ]( ); };

/*
| Creates the set with direct elements.
*/
Self.Elements = function( ) { return Self.create( 'set:init', new Set( arguments ) ); };
