/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Path_Self = tim.require( 'Path/Self.js' );
const tt_tim_Spec_Exports = tim.require( 'Spec/Exports.js' );
const tt_tim_Spec_Group = tim.require( 'Spec/Group.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_dir, v_exports, v_name, v_specs, v_srcDir, v_timcodeGen, v_type )
{
	this.__lazy = { };
	this.__hash = hash;
	this.dir = v_dir;
	this.exports = v_exports;
	this.name = v_name;
	this.specs = v_specs;
	this.srcDir = v_srcDir;
	this.timcodeGen = v_timcodeGen;
	this.type = v_type;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 285960149;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Spec/Package.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_dir;
	let v_exports;
	let v_name;
	let v_specs;
	let v_srcDir;
	let v_timcodeGen;
	let v_type;
	if( this !== Self )
	{
		v_dir = this.dir;
		v_exports = this.exports;
		v_name = this.name;
		v_specs = this.specs;
		v_srcDir = this.srcDir;
		v_timcodeGen = this.timcodeGen;
		v_type = this.type;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'dir':
				if( arg !== pass ) { v_dir = arg; }
				break;
			case 'exports':
				if( arg !== pass ) { v_exports = arg; }
				break;
			case 'name':
				if( arg !== pass ) { v_name = arg; }
				break;
			case 'specs':
				if( arg !== pass ) { v_specs = arg; }
				break;
			case 'srcDir':
				if( arg !== pass ) { v_srcDir = arg; }
				break;
			case 'timcodeGen':
				if( arg !== pass ) { v_timcodeGen = arg; }
				break;
			case 'type':
				if( arg !== pass ) { v_type = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_specs === undefined ) { v_specs = tt_tim_Spec_Group.Empty; }
/**/if( CHECK )
/**/{
/**/	if( v_dir.timtype !== tt_tim_Path_Self ) { throw ( new Error( ) ); }
/**/	if( v_exports !== undefined && v_exports.timtype !== tt_tim_Spec_Exports )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_specs.timtype !== tt_tim_Spec_Group ) { throw ( new Error( ) ); }
/**/	if( v_srcDir.timtype !== tt_tim_Path_Self ) { throw ( new Error( ) ); }
/**/	if( typeof( v_timcodeGen ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_type ) !== 'string' ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash =
		tim._hashArgs(
			285960149,
			v_dir,
			v_exports,
			v_name,
			v_specs,
			v_srcDir,
			v_timcodeGen,
			v_type
		);
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_dir === e.dir && v_exports === e.exports && v_name === e.name && v_specs === e.specs
			&& v_srcDir === e.srcDir
			&& v_timcodeGen === e.timcodeGen
			&& v_type === e.type
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim =
		new Constructor( hash, v_dir, v_exports, v_name, v_specs, v_srcDir, v_timcodeGen, v_type );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
