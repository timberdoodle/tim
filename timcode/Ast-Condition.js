/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_condition, v_elsewise, v_then )
{
	this.__hash = hash;
	this.condition = v_condition;
	this.elsewise = v_elsewise;
	this.then = v_then;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -2139537263;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Condition.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_condition;
	let v_elsewise;
	let v_then;
	if( this !== Self )
	{
		v_condition = this.condition;
		v_elsewise = this.elsewise;
		v_then = this.then;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'condition':
				if( arg !== pass ) { v_condition = arg; }
				break;
			case 'elsewise':
				if( arg !== pass ) { v_elsewise = arg; }
				break;
			case 'then':
				if( arg !== pass ) { v_then = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if(
/**/		v_condition.timtype !== tt_tim_Ast_And
/**/		&& v_condition.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_condition.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_condition.timtype !== tt_tim_Ast_Assign
/**/		&& v_condition.timtype !== tt_tim_Ast_Await
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_condition.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_Boolean
/**/		&& v_condition.timtype !== tt_tim_Ast_Call
/**/		&& v_condition.timtype !== tt_tim_Ast_Comma
/**/		&& v_condition.timtype !== tt_tim_Ast_Condition
/**/		&& v_condition.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_condition.timtype !== tt_tim_Ast_Delete
/**/		&& v_condition.timtype !== tt_tim_Ast_Differs
/**/		&& v_condition.timtype !== tt_tim_Ast_Divide
/**/		&& v_condition.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_Dot
/**/		&& v_condition.timtype !== tt_tim_Ast_Equals
/**/		&& v_condition.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_condition.timtype !== tt_tim_Ast_Generator
/**/		&& v_condition.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_condition.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_condition.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_condition.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_condition.timtype !== tt_tim_Ast_LessThan
/**/		&& v_condition.timtype !== tt_tim_Ast_Member
/**/		&& v_condition.timtype !== tt_tim_Ast_Minus
/**/		&& v_condition.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_Multiply
/**/		&& v_condition.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_Negate
/**/		&& v_condition.timtype !== tt_tim_Ast_New
/**/		&& v_condition.timtype !== tt_tim_Ast_Not
/**/		&& v_condition.timtype !== tt_tim_Ast_Null
/**/		&& v_condition.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_condition.timtype !== tt_tim_Ast_Number
/**/		&& v_condition.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_condition.timtype !== tt_tim_Ast_Or
/**/		&& v_condition.timtype !== tt_tim_Ast_Plus
/**/		&& v_condition.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_condition.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_condition.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_condition.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_condition.timtype !== tt_tim_Ast_Regex
/**/		&& v_condition.timtype !== tt_tim_Ast_Remainder
/**/		&& v_condition.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_condition.timtype !== tt_tim_Ast_String
/**/		&& v_condition.timtype !== tt_tim_Ast_Typeof
/**/		&& v_condition.timtype !== tt_tim_Ast_Undefined
/**/		&& v_condition.timtype !== tt_tim_Ast_Var
/**/		&& v_condition.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_elsewise.timtype !== tt_tim_Ast_And
/**/		&& v_elsewise.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_elsewise.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Assign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Await
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_elsewise.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Boolean
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Call
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Comma
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Condition
/**/		&& v_elsewise.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Delete
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Differs
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Divide
/**/		&& v_elsewise.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Dot
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Equals
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Generator
/**/		&& v_elsewise.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_elsewise.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_elsewise.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_elsewise.timtype !== tt_tim_Ast_LessThan
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Member
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Minus
/**/		&& v_elsewise.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Multiply
/**/		&& v_elsewise.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Negate
/**/		&& v_elsewise.timtype !== tt_tim_Ast_New
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Not
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Null
/**/		&& v_elsewise.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Number
/**/		&& v_elsewise.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Or
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Plus
/**/		&& v_elsewise.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_elsewise.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_elsewise.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_elsewise.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Regex
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Remainder
/**/		&& v_elsewise.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_elsewise.timtype !== tt_tim_Ast_String
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Typeof
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Undefined
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Var
/**/		&& v_elsewise.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_then.timtype !== tt_tim_Ast_And && v_then.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_then.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_then.timtype !== tt_tim_Ast_Assign
/**/		&& v_then.timtype !== tt_tim_Ast_Await
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_then.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_then.timtype !== tt_tim_Ast_Boolean
/**/		&& v_then.timtype !== tt_tim_Ast_Call
/**/		&& v_then.timtype !== tt_tim_Ast_Comma
/**/		&& v_then.timtype !== tt_tim_Ast_Condition
/**/		&& v_then.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_then.timtype !== tt_tim_Ast_Delete
/**/		&& v_then.timtype !== tt_tim_Ast_Differs
/**/		&& v_then.timtype !== tt_tim_Ast_Divide
/**/		&& v_then.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_then.timtype !== tt_tim_Ast_Dot
/**/		&& v_then.timtype !== tt_tim_Ast_Equals
/**/		&& v_then.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_then.timtype !== tt_tim_Ast_Generator
/**/		&& v_then.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_then.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_then.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_then.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_then.timtype !== tt_tim_Ast_LessThan
/**/		&& v_then.timtype !== tt_tim_Ast_Member
/**/		&& v_then.timtype !== tt_tim_Ast_Minus
/**/		&& v_then.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_then.timtype !== tt_tim_Ast_Multiply
/**/		&& v_then.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_then.timtype !== tt_tim_Ast_Negate
/**/		&& v_then.timtype !== tt_tim_Ast_New
/**/		&& v_then.timtype !== tt_tim_Ast_Not
/**/		&& v_then.timtype !== tt_tim_Ast_Null
/**/		&& v_then.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_then.timtype !== tt_tim_Ast_Number
/**/		&& v_then.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_then.timtype !== tt_tim_Ast_Or
/**/		&& v_then.timtype !== tt_tim_Ast_Plus
/**/		&& v_then.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_then.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_then.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_then.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_then.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_then.timtype !== tt_tim_Ast_Regex
/**/		&& v_then.timtype !== tt_tim_Ast_Remainder
/**/		&& v_then.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_then.timtype !== tt_tim_Ast_String
/**/		&& v_then.timtype !== tt_tim_Ast_Typeof
/**/		&& v_then.timtype !== tt_tim_Ast_Undefined
/**/		&& v_then.timtype !== tt_tim_Ast_Var
/**/		&& v_then.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( -2139537263, v_condition, v_elsewise, v_then );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if( v_condition === e.condition && v_elsewise === e.elsewise && v_then === e.then )
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_condition, v_elsewise, v_then );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
