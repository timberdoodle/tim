/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_Func_Arg_Self = tim.require( 'Ast/Func/Arg/Self.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, list )
{
	this.__lazy = { };
	this.__hash = hash;
	this._list = list;
	Object.freeze( this, list );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1641909213;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Func/Arg/List.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let list;
	let listDup;
	if( this !== Self )
	{
		list = this._list;
		listDup = false;
	}
	else
	{
		list = [ ];
		listDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'list:init':
				if( Array.isArray( arg ) )
				{
					list = arg;
					listDup = true;
				}
				else
				{
					list = arg._list;
					listDup = false;
				}
				break;
			case 'list:append':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.push( arg );
				break;
			case 'list:insert':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 0, args[ ++a + 1 ] );
				break;
			case 'list:remove':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 1 );
				break;
			case 'list:set':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list[ arg ] = args[ ++a + 1 ];
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let o of list )
/**/	{
/**/		if( o.timtype !== tt_tim_Ast_Func_Arg_Self ) { throw ( new Error( ) ); }
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const len = list.length;
	const hash = tim._hashIterable( 1641909213, list );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const elist = e._list;
		if( len !== elist.length ) { continue; }
		let eq = true;
		for( let a = 0; a < len; a++ ) { if( list[ a ] !== elist[ a ] ) { eq = false; break; } }
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, list );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the list with an element appended.
*/
prototype.append = tim._proto.listAppend;

/*
| Returns the list with another list appended.
*/
prototype.appendList = tim._proto.listAppendList;

/*
| Returns an array clone of the list.
*/
prototype.clone = tim._proto.listClone;

/*
| Returns the first element of the list.
*/
tim._proto.lazyValue( prototype, 'first', tim._proto.listFirst );

/*
| Returns one element from the list.
*/
prototype.get = tim._proto.listGet;

/*
| Returns the list with one element inserted.
*/
prototype.insert = tim._proto.listInsert;

/*
| Returns the last element of the list.
*/
tim._proto.lazyValue( prototype, 'last', tim._proto.listLast );

/*
| Returns the length of the list.
*/
tim._proto.lazyValue( prototype, 'length', tim._proto.listLength );

/*
| Returns the list with one element removed.
*/
prototype.remove = tim._proto.listRemove;

/*
| Returns the list with one element set.
*/
prototype.set = tim._proto.listSet;

/*
| Returns a slice from the list.
*/
prototype.slice = tim._proto.listSlice;

/*
| Returns a sorted list.
*/
prototype.sort = tim._proto.listSort;

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._list[ Symbol.iterator ]( ); };

/*
| Reverse iterates over the list.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this._list[ a ]; } };

/*
| Creates the list from an Array.
*/
Self.Array = function( array ) { return Self.create( 'list:init', array ); };

/*
| Creates the list with direct elements.
*/
Self.Elements =
	function( ) { return Self.create( 'list:init', Array.prototype.slice.call( arguments ) ); };

/*
| Creates an empty list of this type.
*/
tim._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
