/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';


/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_line, v_lineNr, v_pos, v_type, v_value )
{
	this.__lazy = { };
	this.__hash = hash;
	this.line = v_line;
	this.lineNr = v_lineNr;
	this.pos = v_pos;
	this.type = v_type;
	this.value = v_value;
	Object.freeze( this );
/**/if( CHECK ) { this._check( ); }
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 310184942;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Lexer/Token/Self.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_line;
	let v_lineNr;
	let v_pos;
	let v_type;
	let v_value;
	if( this !== Self )
	{
		v_line = this.line;
		v_lineNr = this.lineNr;
		v_pos = this.pos;
		v_type = this.type;
		v_value = this.value;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'line':
				if( arg !== pass ) { v_line = arg; }
				break;
			case 'lineNr':
				if( arg !== pass ) { v_lineNr = arg; }
				break;
			case 'pos':
				if( arg !== pass ) { v_pos = arg; }
				break;
			case 'type':
				if( arg !== pass ) { v_type = arg; }
				break;
			case 'value':
				if( arg !== pass ) { v_value = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( typeof( v_line ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		typeof( v_lineNr ) !== 'number' || Number.isNaN( v_lineNr )
/**/		|| Math.floor( v_lineNr ) !== v_lineNr
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		typeof( v_pos ) !== 'number' || Number.isNaN( v_pos ) || Math.floor( v_pos ) !== v_pos
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_type ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_value !== undefined && ( typeof( v_value ) !== 'number' || Number.isNaN( v_value ) )
/**/		&& typeof( v_value ) !== 'boolean'
/**/		&& typeof( v_value ) !== 'string'
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 310184942, v_line, v_lineNr, v_pos, v_type, v_value );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_line === e.line && v_lineNr === e.lineNr && v_pos === e.pos && v_type === e.type
			&& v_value === e.value
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_line, v_lineNr, v_pos, v_type, v_value );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
