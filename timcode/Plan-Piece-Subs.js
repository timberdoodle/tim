/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Plan_Piece_Self = tim.require( 'Plan/Piece/Self.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, group, keys )
{
	this.__lazy = { };
	this.__hash = hash;
	this._group = group;
	this.keys = keys;
	Object.freeze( this, group, keys );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 851468392;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Plan/Piece/Subs.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let group;
	let groupDup;
	if( this !== Self )
	{
		group = this._group;
		groupDup = false;
	}
	else
	{
		group = { };
		groupDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'group:init':
				group = arg;
				groupDup = true;
				break;
			case 'group:set':
				if( !groupDup ) { group = tim.copy( group ); groupDup = true; }
				group[ arg ] = args[ ++a + 1 ];
				break;
			case 'group:remove':
				if( !groupDup ) { group = tim.copy( group ); groupDup = true; }
				delete group[ arg ];
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let k in group )
/**/	{
/**/		const o = group[ k ];
/**/		if( typeof( o ) !== 'function' && o.timtype !== tt_tim_Plan_Piece_Self )
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const keys = Object.freeze( Object.keys( group ).sort( ) );
	const hash = tim._hashKeys( 851468392, keys, group );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const ekeys = e.keys;
		const len = keys.length;
		const egroup = e._group;
		if( keys.length !== ekeys.length ) { continue; }
		let eq = true;
		for( let a = 0; a < len; a++ )
		{
			const key = keys[ a ];
			if( key !== ekeys[ a ] || group[ key ] !== egroup[ key ] ) { eq = false; break; }
		}
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, group, keys );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the group with another group added,
| overwriting collisions.
*/
prototype.addGroup = tim._proto.groupAddGroup;

/*
| Gets one element from the group.
*/
prototype.get = tim._proto.groupGet;

/*
| Returns the group with one element removed.
*/
prototype.remove = tim._proto.groupRemove;

/*
| Returns the group with one element set.
*/
prototype.set = tim._proto.groupSet;

/*
| Returns the size of the group.
*/
tim._proto.lazyValue( prototype, 'size', tim._proto.groupSize );

/*
| Iterates over the group by sorted keys.
*/
prototype[ Symbol.iterator ] =
	function*( ) { for( let key of this.keys ) { yield this.get( key ); } };

/*
| Creates an empty group of this type.
*/
tim._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
Self.Table = function( group ) { return Self.create( 'group:init', group ); };
