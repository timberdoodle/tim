/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';


/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_check, v_indent, v_inline )
{
	this.__lazy = { };
	this.__hash = hash;
	this.check = v_check;
	this.indent = v_indent;
	this.inline = v_inline;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1827149377;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Format/Context.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_check;
	let v_indent;
	let v_inline;
	if( this !== Self ) { v_check = this.check; v_indent = this.indent; v_inline = this.inline; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'check':
				if( arg !== pass ) { v_check = arg; }
				break;
			case 'indent':
				if( arg !== pass ) { v_indent = arg; }
				break;
			case 'inline':
				if( arg !== pass ) { v_inline = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_check === undefined ) { v_check = false; }
	if( v_indent === undefined ) { v_indent = 0; }
	if( v_inline === undefined ) { v_inline = false; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_check ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if(
/**/		typeof( v_indent ) !== 'number' || Number.isNaN( v_indent )
/**/		|| Math.floor( v_indent ) !== v_indent
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_inline ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 1827149377, v_check, v_indent, v_inline );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if( v_check === e.check && v_indent === e.indent && v_inline === e.inline ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_check, v_indent, v_inline );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
