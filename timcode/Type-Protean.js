/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';


/*
| Constructor.
*/
const Constructor = function( hash ) { this.__hash = hash; Object.freeze( this ); };

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -525139436;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Type/Protean.js'; }

/*
| Singleton.
*/
let _singleton;
tim._proto.lazyStaticValue( Self, 'singleton', function( ) { return Self._create( ); } );

/*
| Creates a new object.
*/
Self._create =
prototype._create =
	function( ...args )
{
	const hash = -525139436;
	if( !_singleton ) { _singleton = new Constructor( hash ); }
	return _singleton;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
