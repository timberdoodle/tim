/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Spec_Package = tim.require( 'Spec/Package.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, keys, twig )
{
	this.__lazy = { };
	this.__hash = hash;
	this._twig = twig;
	this.keys = keys;
	Object.freeze( this, twig, keys );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1867218166;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Spec/Packages.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let key;
	let keys;
	let rank;
	let twig;
	let twigDup;
	if( this !== Self )
	{
		twig = this._twig;
		keys = this.keys;
		twigDup = false;
	}
	else
	{
		twig = { };
		keys = [ ];
		twigDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'twig:add':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.push( key );
				break;
			case 'twig:init':
				twigDup = true;
				twig = arg;
				keys = args[ ++a + 1 ];
/**/			if( CHECK )
/**/			{
/**/				if( !Array.isArray( keys ) ) { throw ( new Error( ) ); }
/**/				if( Object.keys( twig ).length !== keys.length ) { throw ( new Error( ) ); }
/**/				for( let key of keys )
/**/				{
/**/					if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
/**/				}
/**/			}
				break;
			case 'twig:insert':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				rank = args[ a + 2 ];
				arg = args[ a + 3 ];
				a += 2;
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				if( rank < 0 || rank > keys.length ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.splice( rank, 0, key );
				break;
			case 'twig:remove':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				if( twig[ arg ] === undefined ) { throw ( new Error( ) ); }
				delete twig[ arg ];
				keys.splice( keys.indexOf( arg ), 1 );
				break;
			case 'twig:set+':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { keys.push( key ); }
				twig[ key ] = arg;
				break;
			case 'twig:set':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let key of keys )
/**/	{
/**/		const o = twig[ key ];
/**/		if( o.timtype !== tt_tim_Spec_Package ) { throw ( new Error( ) ); }
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashKeys( 1867218166, keys, twig );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const len = keys.length;
		const ekeys = e.keys;
		const etwig = e._twig;
		if( keys.length !== ekeys.length ) { continue; }
		let eq = true;
		for( let a = 0; a < len; a++ )
		{
			const key = keys[ a ];
			if( key !== ekeys[ a ] || twig[ key ] !== etwig[ key ] ) { eq = false; break; }
		}
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, keys, twig );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the element at rank.
*/
prototype.atRank = tim._proto.twigAtRank;

/*
| Returns the element by key.
*/
prototype.get = tim._proto.twigGet;

/*
| Returns the key at a rank.
*/
prototype.getKey = tim._proto.twigGetKey;

/*
| Returns the length of the twig.
*/
tim._proto.lazyValue( prototype, 'length', tim._proto.twigLength );

/*
| Returns the rank of the key.
*/
tim._proto.lazyFunction( prototype, 'rankOf', tim._proto.twigRankOf );

/*
| Returns the twig with the element at key set.
*/
prototype.set = tim._proto.twigSet;

/*
| Iterates over the twig.
*/
prototype[ Symbol.iterator ] =
	function*( ) { for( let a = 0, al = this.length; a < al; a++ ) { yield this.atRank( a ); } };

/*
| Reverse iterates over the twig.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this.atRank( a ); } };

/*
| Creates an empty group of this type.
*/
tim._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );

/*
| Grows the twig.
*/
Self.Grow =
	function( )
{
	let ranks = [ ];
	let twig = { };
	for( let a = 0, alen = arguments.length; a < alen; a += 2 )
	{
		const key = arguments[ a ];
		ranks.push( key );
		twig[ key ] = arguments[ a + 1 ];
	}
	return Self.create( 'twig:init', twig, ranks );
};
