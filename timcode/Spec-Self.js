/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Path_Self = tim.require( 'Path/Self.js' );
const tt_tim_Spec_Attribute_Group = tim.require( 'Spec/Attribute/Group.js' );
const tt_tim_Type_Tim = tim.require( 'Type/Tim.js' );
const tt_tim_Spec_Self = tim.require( 'Spec/Self.js' );
const tt_tim_string_list = tim.require( 'string/list.js' );
const tt_tim_Type_Set = tim.require( 'Type/Set.js' );
const tt_tim_string_set = tim.require( 'string/set.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Trace_Self = tim.require( 'Trace/Self.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function(
		hash,
		v_abstract,
		v_afile,
		v_alike,
		v_attributes,
		v_check,
		v_collectionType,
		v_creator,
		v_customFromJson,
		v_customJsonfy,
		v_extend,
		v_extendSpec,
		v_fromJsonArgs,
		v_hasLazy,
		v_imports,
		v_items,
		v_json,
		v_requires,
		v_setGlobal,
		v_singleton,
		v_trace
	)
{
	this.__lazy = { };
	this.__hash = hash;
	this.abstract = v_abstract;
	this.afile = v_afile;
	this.alike = v_alike;
	this.attributes = v_attributes;
	this.check = v_check;
	this.collectionType = v_collectionType;
	this.creator = v_creator;
	this.customFromJson = v_customFromJson;
	this.customJsonfy = v_customJsonfy;
	this.extend = v_extend;
	this.extendSpec = v_extendSpec;
	this.fromJsonArgs = v_fromJsonArgs;
	this.hasLazy = v_hasLazy;
	this.imports = v_imports;
	this.items = v_items;
	this.json = v_json;
	this.requires = v_requires;
	this.setGlobal = v_setGlobal;
	this.singleton = v_singleton;
	this.trace = v_trace;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1517091942;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Spec/Self.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_abstract;
	let v_afile;
	let v_alike;
	let v_attributes;
	let v_check;
	let v_collectionType;
	let v_creator;
	let v_customFromJson;
	let v_customJsonfy;
	let v_extend;
	let v_extendSpec;
	let v_fromJsonArgs;
	let v_hasLazy;
	let v_imports;
	let v_items;
	let v_json;
	let v_requires;
	let v_setGlobal;
	let v_singleton;
	let v_trace;
	if( this !== Self )
	{
		v_abstract = this.abstract;
		v_afile = this.afile;
		v_alike = this.alike;
		v_attributes = this.attributes;
		v_check = this.check;
		v_collectionType = this.collectionType;
		v_creator = this.creator;
		v_customFromJson = this.customFromJson;
		v_customJsonfy = this.customJsonfy;
		v_extend = this.extend;
		v_extendSpec = this.extendSpec;
		v_fromJsonArgs = this.fromJsonArgs;
		v_hasLazy = this.hasLazy;
		v_imports = this.imports;
		v_items = this.items;
		v_json = this.json;
		v_requires = this.requires;
		v_setGlobal = this.setGlobal;
		v_singleton = this.singleton;
		v_trace = this.trace;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'abstract':
				if( arg !== pass ) { v_abstract = arg; }
				break;
			case 'afile':
				if( arg !== pass ) { v_afile = arg; }
				break;
			case 'alike':
				if( arg !== pass ) { v_alike = arg; }
				break;
			case 'attributes':
				if( arg !== pass ) { v_attributes = arg; }
				break;
			case 'check':
				if( arg !== pass ) { v_check = arg; }
				break;
			case 'collectionType':
				if( arg !== pass ) { v_collectionType = arg; }
				break;
			case 'creator':
				if( arg !== pass ) { v_creator = arg; }
				break;
			case 'customFromJson':
				if( arg !== pass ) { v_customFromJson = arg; }
				break;
			case 'customJsonfy':
				if( arg !== pass ) { v_customJsonfy = arg; }
				break;
			case 'extend':
				if( arg !== pass ) { v_extend = arg; }
				break;
			case 'extendSpec':
				if( arg !== pass ) { v_extendSpec = arg; }
				break;
			case 'fromJsonArgs':
				if( arg !== pass ) { v_fromJsonArgs = arg; }
				break;
			case 'hasLazy':
				if( arg !== pass ) { v_hasLazy = arg; }
				break;
			case 'imports':
				if( arg !== pass ) { v_imports = arg; }
				break;
			case 'items':
				if( arg !== pass ) { v_items = arg; }
				break;
			case 'json':
				if( arg !== pass ) { v_json = arg; }
				break;
			case 'requires':
				if( arg !== pass ) { v_requires = arg; }
				break;
			case 'setGlobal':
				if( arg !== pass ) { v_setGlobal = arg; }
				break;
			case 'singleton':
				if( arg !== pass ) { v_singleton = arg; }
				break;
			case 'trace':
				if( arg !== pass ) { v_trace = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( typeof( v_abstract ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_afile.timtype !== tt_tim_Path_Self ) { throw ( new Error( ) ); }
/**/	if( v_alike !== undefined && typeof( v_alike ) !== 'object' ) { throw ( new Error( ) ); }
/**/	if( v_attributes.timtype !== tt_tim_Spec_Attribute_Group ) { throw ( new Error( ) ); }
/**/	if( typeof( v_check ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_collectionType ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_creator ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_customFromJson ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_customJsonfy ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_extend !== undefined && v_extend.timtype !== tt_tim_Type_Tim )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_extendSpec !== undefined && v_extendSpec.timtype !== tt_tim_Spec_Self )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_fromJsonArgs !== undefined && v_fromJsonArgs.timtype !== tt_tim_string_list )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_hasLazy ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_imports.timtype !== tt_tim_Type_Set ) { throw ( new Error( ) ); }
/**/	if( v_items !== undefined && v_items.timtype !== tt_tim_Type_Set )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_json !== undefined && typeof( v_json ) !== 'string'
/**/		&& v_json.timtype !== tt_tim_Spec_Self
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_requires.timtype !== tt_tim_string_set ) { throw ( new Error( ) ); }
/**/	if( v_setGlobal !== undefined && v_setGlobal.timtype !== tt_tim_Ast_Var )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_singleton ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_trace.timtype !== tt_tim_Trace_Self ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash =
		tim._hashArgs(
			1517091942,
			v_abstract,
			v_afile,
			v_alike,
			v_attributes,
			v_check,
			v_collectionType,
			v_creator,
			v_customFromJson,
			v_customJsonfy,
			v_extend,
			v_extendSpec,
			v_fromJsonArgs,
			v_hasLazy,
			v_imports,
			v_items,
			v_json,
			v_requires,
			v_setGlobal,
			v_singleton,
			v_trace
		);
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_abstract === e.abstract && v_afile === e.afile && v_alike === e.alike
			&& v_attributes === e.attributes
			&& v_check === e.check
			&& v_collectionType === e.collectionType
			&& v_creator === e.creator
			&& v_customFromJson === e.customFromJson
			&& v_customJsonfy === e.customJsonfy
			&& v_extend === e.extend
			&& v_extendSpec === e.extendSpec
			&& v_fromJsonArgs === e.fromJsonArgs
			&& v_hasLazy === e.hasLazy
			&& v_imports === e.imports
			&& v_items === e.items
			&& v_json === e.json
			&& v_requires === e.requires
			&& v_setGlobal === e.setGlobal
			&& v_singleton === e.singleton
			&& v_trace === e.trace
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim =
		new Constructor(
			hash,
			v_abstract,
			v_afile,
			v_alike,
			v_attributes,
			v_check,
			v_collectionType,
			v_creator,
			v_customFromJson,
			v_customJsonfy,
			v_extend,
			v_extendSpec,
			v_fromJsonArgs,
			v_hasLazy,
			v_imports,
			v_items,
			v_json,
			v_requires,
			v_setGlobal,
			v_singleton,
			v_trace
		);
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
