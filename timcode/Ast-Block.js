/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_Block = tim.require( 'Ast/Block.js' );
const tt_tim_Ast_Break = tim.require( 'Ast/Break.js' );
const tt_tim_Ast_Check = tim.require( 'Ast/Check.js' );
const tt_tim_Ast_Comment = tim.require( 'Ast/Comment.js' );
const tt_tim_Ast_Const = tim.require( 'Ast/Const.js' );
const tt_tim_Ast_Continue = tim.require( 'Ast/Continue.js' );
const tt_tim_Ast_DestructDecl = tim.require( 'Ast/DestructDecl.js' );
const tt_tim_Ast_DoWhile = tim.require( 'Ast/DoWhile.js' );
const tt_tim_Ast_For = tim.require( 'Ast/For.js' );
const tt_tim_Ast_ForIn = tim.require( 'Ast/ForIn.js' );
const tt_tim_Ast_ForOf = tim.require( 'Ast/ForOf.js' );
const tt_tim_Ast_If = tim.require( 'Ast/If.js' );
const tt_tim_Ast_Let = tim.require( 'Ast/Let.js' );
const tt_tim_Ast_Return = tim.require( 'Ast/Return.js' );
const tt_tim_Ast_Sep = tim.require( 'Ast/Sep.js' );
const tt_tim_Ast_Switch = tim.require( 'Ast/Switch.js' );
const tt_tim_Ast_Throw = tim.require( 'Ast/Throw.js' );
const tt_tim_Ast_Try = tim.require( 'Ast/Try.js' );
const tt_tim_Ast_VarDec = tim.require( 'Ast/VarDec.js' );
const tt_tim_Ast_While = tim.require( 'Ast/While.js' );
const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, list )
{
	this.__lazy = { };
	this.__hash = hash;
	this._list = list;
	Object.freeze( this, list );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 333344543;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Block.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let list;
	let listDup;
	if( this !== Self )
	{
		list = this._list;
		listDup = false;
	}
	else
	{
		list = [ ];
		listDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'list:init':
				if( Array.isArray( arg ) )
				{
					list = arg;
					listDup = true;
				}
				else
				{
					list = arg._list;
					listDup = false;
				}
				break;
			case 'list:append':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.push( arg );
				break;
			case 'list:insert':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 0, args[ ++a + 1 ] );
				break;
			case 'list:remove':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 1 );
				break;
			case 'list:set':
				if( !listDup ) { list = list.slice( ); listDup = true; }
				list[ arg ] = args[ ++a + 1 ];
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let o of list )
/**/	{
/**/		if(
/**/			o.timtype !== tt_tim_Ast_Block && o.timtype !== tt_tim_Ast_Break
/**/			&& o.timtype !== tt_tim_Ast_Check
/**/			&& o.timtype !== tt_tim_Ast_Comment
/**/			&& o.timtype !== tt_tim_Ast_Const
/**/			&& o.timtype !== tt_tim_Ast_Continue
/**/			&& o.timtype !== tt_tim_Ast_DestructDecl
/**/			&& o.timtype !== tt_tim_Ast_DoWhile
/**/			&& o.timtype !== tt_tim_Ast_For
/**/			&& o.timtype !== tt_tim_Ast_ForIn
/**/			&& o.timtype !== tt_tim_Ast_ForOf
/**/			&& o.timtype !== tt_tim_Ast_If
/**/			&& o.timtype !== tt_tim_Ast_Let
/**/			&& o.timtype !== tt_tim_Ast_Return
/**/			&& o.timtype !== tt_tim_Ast_Sep
/**/			&& o.timtype !== tt_tim_Ast_Switch
/**/			&& o.timtype !== tt_tim_Ast_Throw
/**/			&& o.timtype !== tt_tim_Ast_Try
/**/			&& o.timtype !== tt_tim_Ast_VarDec
/**/			&& o.timtype !== tt_tim_Ast_While
/**/			&& o.timtype !== tt_tim_Ast_And
/**/			&& o.timtype !== tt_tim_Ast_ArrayLiteral
/**/			&& o.timtype !== tt_tim_Ast_ArrowFunction
/**/			&& o.timtype !== tt_tim_Ast_Assign
/**/			&& o.timtype !== tt_tim_Ast_Await
/**/			&& o.timtype !== tt_tim_Ast_BitwiseAnd
/**/			&& o.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/			&& o.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseNot
/**/			&& o.timtype !== tt_tim_Ast_BitwiseOr
/**/			&& o.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/			&& o.timtype !== tt_tim_Ast_BitwiseRightShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseXor
/**/			&& o.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/			&& o.timtype !== tt_tim_Ast_Boolean
/**/			&& o.timtype !== tt_tim_Ast_Call
/**/			&& o.timtype !== tt_tim_Ast_Comma
/**/			&& o.timtype !== tt_tim_Ast_Condition
/**/			&& o.timtype !== tt_tim_Ast_ConditionalDot
/**/			&& o.timtype !== tt_tim_Ast_Delete
/**/			&& o.timtype !== tt_tim_Ast_Differs
/**/			&& o.timtype !== tt_tim_Ast_Divide
/**/			&& o.timtype !== tt_tim_Ast_DivideAssign
/**/			&& o.timtype !== tt_tim_Ast_Dot
/**/			&& o.timtype !== tt_tim_Ast_Equals
/**/			&& o.timtype !== tt_tim_Ast_Func_Self
/**/			&& o.timtype !== tt_tim_Ast_Generator
/**/			&& o.timtype !== tt_tim_Ast_GreaterOrEqual
/**/			&& o.timtype !== tt_tim_Ast_GreaterThan
/**/			&& o.timtype !== tt_tim_Ast_Instanceof
/**/			&& o.timtype !== tt_tim_Ast_LessOrEqual
/**/			&& o.timtype !== tt_tim_Ast_LessThan
/**/			&& o.timtype !== tt_tim_Ast_Member
/**/			&& o.timtype !== tt_tim_Ast_Minus
/**/			&& o.timtype !== tt_tim_Ast_MinusAssign
/**/			&& o.timtype !== tt_tim_Ast_Multiply
/**/			&& o.timtype !== tt_tim_Ast_MultiplyAssign
/**/			&& o.timtype !== tt_tim_Ast_Negate
/**/			&& o.timtype !== tt_tim_Ast_New
/**/			&& o.timtype !== tt_tim_Ast_Not
/**/			&& o.timtype !== tt_tim_Ast_Null
/**/			&& o.timtype !== tt_tim_Ast_NullishCoalescence
/**/			&& o.timtype !== tt_tim_Ast_Number
/**/			&& o.timtype !== tt_tim_Ast_ObjLiteral
/**/			&& o.timtype !== tt_tim_Ast_Or
/**/			&& o.timtype !== tt_tim_Ast_Plus
/**/			&& o.timtype !== tt_tim_Ast_PlusAssign
/**/			&& o.timtype !== tt_tim_Ast_PostDecrement
/**/			&& o.timtype !== tt_tim_Ast_PostIncrement
/**/			&& o.timtype !== tt_tim_Ast_PreDecrement
/**/			&& o.timtype !== tt_tim_Ast_PreIncrement
/**/			&& o.timtype !== tt_tim_Ast_Regex
/**/			&& o.timtype !== tt_tim_Ast_Remainder
/**/			&& o.timtype !== tt_tim_Ast_RemainderAssign
/**/			&& o.timtype !== tt_tim_Ast_String
/**/			&& o.timtype !== tt_tim_Ast_Typeof
/**/			&& o.timtype !== tt_tim_Ast_Undefined
/**/			&& o.timtype !== tt_tim_Ast_Var
/**/			&& o.timtype !== tt_tim_Ast_Yield
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const len = list.length;
	const hash = tim._hashIterable( 333344543, list );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const elist = e._list;
		if( len !== elist.length ) { continue; }
		let eq = true;
		for( let a = 0; a < len; a++ ) { if( list[ a ] !== elist[ a ] ) { eq = false; break; } }
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, list );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the list with an element appended.
*/
prototype.append = tim._proto.listAppend;

/*
| Returns the list with another list appended.
*/
prototype.appendList = tim._proto.listAppendList;

/*
| Returns an array clone of the list.
*/
prototype.clone = tim._proto.listClone;

/*
| Returns the first element of the list.
*/
tim._proto.lazyValue( prototype, 'first', tim._proto.listFirst );

/*
| Returns one element from the list.
*/
prototype.get = tim._proto.listGet;

/*
| Returns the list with one element inserted.
*/
prototype.insert = tim._proto.listInsert;

/*
| Returns the last element of the list.
*/
tim._proto.lazyValue( prototype, 'last', tim._proto.listLast );

/*
| Returns the length of the list.
*/
tim._proto.lazyValue( prototype, 'length', tim._proto.listLength );

/*
| Returns the list with one element removed.
*/
prototype.remove = tim._proto.listRemove;

/*
| Returns the list with one element set.
*/
prototype.set = tim._proto.listSet;

/*
| Returns a slice from the list.
*/
prototype.slice = tim._proto.listSlice;

/*
| Returns a sorted list.
*/
prototype.sort = tim._proto.listSort;

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._list[ Symbol.iterator ]( ); };

/*
| Reverse iterates over the list.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this._list[ a ]; } };

/*
| Creates the list from an Array.
*/
Self.Array = function( array ) { return Self.create( 'list:init', array ); };

/*
| Creates the list with direct elements.
*/
Self.Elements =
	function( ) { return Self.create( 'list:init', Array.prototype.slice.call( arguments ) ); };

/*
| Creates an empty list of this type.
*/
tim._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
