/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, keys, twig )
{
	this.__lazy = { };
	this.__hash = hash;
	this._twig = twig;
	this.keys = keys;
	Object.freeze( this, twig, keys );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 390948458;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/ObjLiteral.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let key;
	let keys;
	let rank;
	let twig;
	let twigDup;
	if( this !== Self )
	{
		twig = this._twig;
		keys = this.keys;
		twigDup = false;
	}
	else
	{
		twig = { };
		keys = [ ];
		twigDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'twig:add':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.push( key );
				break;
			case 'twig:init':
				twigDup = true;
				twig = arg;
				keys = args[ ++a + 1 ];
/**/			if( CHECK )
/**/			{
/**/				if( !Array.isArray( keys ) ) { throw ( new Error( ) ); }
/**/				if( Object.keys( twig ).length !== keys.length ) { throw ( new Error( ) ); }
/**/				for( let key of keys )
/**/				{
/**/					if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
/**/				}
/**/			}
				break;
			case 'twig:insert':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				rank = args[ a + 2 ];
				arg = args[ a + 3 ];
				a += 2;
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				if( rank < 0 || rank > keys.length ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.splice( rank, 0, key );
				break;
			case 'twig:remove':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				if( twig[ arg ] === undefined ) { throw ( new Error( ) ); }
				delete twig[ arg ];
				keys.splice( keys.indexOf( arg ), 1 );
				break;
			case 'twig:set+':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { keys.push( key ); }
				twig[ key ] = arg;
				break;
			case 'twig:set':
				if( twigDup !== true )
				{
					twig = tim.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let key of keys )
/**/	{
/**/		const o = twig[ key ];
/**/		if(
/**/			o.timtype !== tt_tim_Ast_And && o.timtype !== tt_tim_Ast_ArrayLiteral
/**/			&& o.timtype !== tt_tim_Ast_ArrowFunction
/**/			&& o.timtype !== tt_tim_Ast_Assign
/**/			&& o.timtype !== tt_tim_Ast_Await
/**/			&& o.timtype !== tt_tim_Ast_BitwiseAnd
/**/			&& o.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/			&& o.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseNot
/**/			&& o.timtype !== tt_tim_Ast_BitwiseOr
/**/			&& o.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/			&& o.timtype !== tt_tim_Ast_BitwiseRightShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/			&& o.timtype !== tt_tim_Ast_BitwiseXor
/**/			&& o.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/			&& o.timtype !== tt_tim_Ast_Boolean
/**/			&& o.timtype !== tt_tim_Ast_Call
/**/			&& o.timtype !== tt_tim_Ast_Comma
/**/			&& o.timtype !== tt_tim_Ast_Condition
/**/			&& o.timtype !== tt_tim_Ast_ConditionalDot
/**/			&& o.timtype !== tt_tim_Ast_Delete
/**/			&& o.timtype !== tt_tim_Ast_Differs
/**/			&& o.timtype !== tt_tim_Ast_Divide
/**/			&& o.timtype !== tt_tim_Ast_DivideAssign
/**/			&& o.timtype !== tt_tim_Ast_Dot
/**/			&& o.timtype !== tt_tim_Ast_Equals
/**/			&& o.timtype !== tt_tim_Ast_Func_Self
/**/			&& o.timtype !== tt_tim_Ast_Generator
/**/			&& o.timtype !== tt_tim_Ast_GreaterOrEqual
/**/			&& o.timtype !== tt_tim_Ast_GreaterThan
/**/			&& o.timtype !== tt_tim_Ast_Instanceof
/**/			&& o.timtype !== tt_tim_Ast_LessOrEqual
/**/			&& o.timtype !== tt_tim_Ast_LessThan
/**/			&& o.timtype !== tt_tim_Ast_Member
/**/			&& o.timtype !== tt_tim_Ast_Minus
/**/			&& o.timtype !== tt_tim_Ast_MinusAssign
/**/			&& o.timtype !== tt_tim_Ast_Multiply
/**/			&& o.timtype !== tt_tim_Ast_MultiplyAssign
/**/			&& o.timtype !== tt_tim_Ast_Negate
/**/			&& o.timtype !== tt_tim_Ast_New
/**/			&& o.timtype !== tt_tim_Ast_Not
/**/			&& o.timtype !== tt_tim_Ast_Null
/**/			&& o.timtype !== tt_tim_Ast_NullishCoalescence
/**/			&& o.timtype !== tt_tim_Ast_Number
/**/			&& o.timtype !== tt_tim_Ast_ObjLiteral
/**/			&& o.timtype !== tt_tim_Ast_Or
/**/			&& o.timtype !== tt_tim_Ast_Plus
/**/			&& o.timtype !== tt_tim_Ast_PlusAssign
/**/			&& o.timtype !== tt_tim_Ast_PostDecrement
/**/			&& o.timtype !== tt_tim_Ast_PostIncrement
/**/			&& o.timtype !== tt_tim_Ast_PreDecrement
/**/			&& o.timtype !== tt_tim_Ast_PreIncrement
/**/			&& o.timtype !== tt_tim_Ast_Regex
/**/			&& o.timtype !== tt_tim_Ast_Remainder
/**/			&& o.timtype !== tt_tim_Ast_RemainderAssign
/**/			&& o.timtype !== tt_tim_Ast_String
/**/			&& o.timtype !== tt_tim_Ast_Typeof
/**/			&& o.timtype !== tt_tim_Ast_Undefined
/**/			&& o.timtype !== tt_tim_Ast_Var
/**/			&& o.timtype !== tt_tim_Ast_Yield
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashKeys( 390948458, keys, twig );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		const len = keys.length;
		const ekeys = e.keys;
		const etwig = e._twig;
		if( keys.length !== ekeys.length ) { continue; }
		let eq = true;
		for( let a = 0; a < len; a++ )
		{
			const key = keys[ a ];
			if( key !== ekeys[ a ] || twig[ key ] !== etwig[ key ] ) { eq = false; break; }
		}
		if( eq ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, keys, twig );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;

/*
| Returns the element at rank.
*/
prototype.atRank = tim._proto.twigAtRank;

/*
| Returns the element by key.
*/
prototype.get = tim._proto.twigGet;

/*
| Returns the key at a rank.
*/
prototype.getKey = tim._proto.twigGetKey;

/*
| Returns the length of the twig.
*/
tim._proto.lazyValue( prototype, 'length', tim._proto.twigLength );

/*
| Returns the rank of the key.
*/
tim._proto.lazyFunction( prototype, 'rankOf', tim._proto.twigRankOf );

/*
| Returns the twig with the element at key set.
*/
prototype.set = tim._proto.twigSet;

/*
| Iterates over the twig.
*/
prototype[ Symbol.iterator ] =
	function*( ) { for( let a = 0, al = this.length; a < al; a++ ) { yield this.atRank( a ); } };

/*
| Reverse iterates over the twig.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this.atRank( a ); } };

/*
| Creates an empty group of this type.
*/
tim._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );

/*
| Grows the twig.
*/
Self.Grow =
	function( )
{
	let ranks = [ ];
	let twig = { };
	for( let a = 0, alen = arguments.length; a < alen; a += 2 )
	{
		const key = arguments[ a ];
		ranks.push( key );
		twig[ key ] = arguments[ a + 1 ];
	}
	return Self.create( 'twig:init', twig, ranks );
};
