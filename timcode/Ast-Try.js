/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_Block = tim.require( 'Ast/Block.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_catchStatement, v_exceptionVar, v_tryStatement )
{
	this.__hash = hash;
	this.catchStatement = v_catchStatement;
	this.exceptionVar = v_exceptionVar;
	this.tryStatement = v_tryStatement;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -614615759;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Try.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_catchStatement;
	let v_exceptionVar;
	let v_tryStatement;
	if( this !== Self )
	{
		v_catchStatement = this.catchStatement;
		v_exceptionVar = this.exceptionVar;
		v_tryStatement = this.tryStatement;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'catchStatement':
				if( arg !== pass ) { v_catchStatement = arg; }
				break;
			case 'exceptionVar':
				if( arg !== pass ) { v_exceptionVar = arg; }
				break;
			case 'tryStatement':
				if( arg !== pass ) { v_tryStatement = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v_catchStatement.timtype !== tt_tim_Ast_Block ) { throw ( new Error( ) ); }
/**/	if( v_exceptionVar.timtype !== tt_tim_Ast_Var ) { throw ( new Error( ) ); }
/**/	if( v_tryStatement.timtype !== tt_tim_Ast_Block ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( -614615759, v_catchStatement, v_exceptionVar, v_tryStatement );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_catchStatement === e.catchStatement && v_exceptionVar === e.exceptionVar
			&& v_tryStatement === e.tryStatement
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_catchStatement, v_exceptionVar, v_tryStatement );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
