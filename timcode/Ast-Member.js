/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_And = tim.require( 'Ast/And.js' );
const tt_tim_Ast_ArrayLiteral = tim.require( 'Ast/ArrayLiteral.js' );
const tt_tim_Ast_ArrowFunction = tim.require( 'Ast/ArrowFunction.js' );
const tt_tim_Ast_Assign = tim.require( 'Ast/Assign.js' );
const tt_tim_Ast_Await = tim.require( 'Ast/Await.js' );
const tt_tim_Ast_BitwiseAnd = tim.require( 'Ast/BitwiseAnd.js' );
const tt_tim_Ast_BitwiseAndAssign = tim.require( 'Ast/BitwiseAndAssign.js' );
const tt_tim_Ast_BitwiseLeftShift = tim.require( 'Ast/BitwiseLeftShift.js' );
const tt_tim_Ast_BitwiseNot = tim.require( 'Ast/BitwiseNot.js' );
const tt_tim_Ast_BitwiseOr = tim.require( 'Ast/BitwiseOr.js' );
const tt_tim_Ast_BitwiseOrAssign = tim.require( 'Ast/BitwiseOrAssign.js' );
const tt_tim_Ast_BitwiseRightShift = tim.require( 'Ast/BitwiseRightShift.js' );
const tt_tim_Ast_BitwiseUnsignedRightShift = tim.require( 'Ast/BitwiseUnsignedRightShift.js' );
const tt_tim_Ast_BitwiseXor = tim.require( 'Ast/BitwiseXor.js' );
const tt_tim_Ast_BitwiseXorAssign = tim.require( 'Ast/BitwiseXorAssign.js' );
const tt_tim_Ast_Boolean = tim.require( 'Ast/Boolean.js' );
const tt_tim_Ast_Call = tim.require( 'Ast/Call.js' );
const tt_tim_Ast_Comma = tim.require( 'Ast/Comma.js' );
const tt_tim_Ast_Condition = tim.require( 'Ast/Condition.js' );
const tt_tim_Ast_ConditionalDot = tim.require( 'Ast/ConditionalDot.js' );
const tt_tim_Ast_Delete = tim.require( 'Ast/Delete.js' );
const tt_tim_Ast_Differs = tim.require( 'Ast/Differs.js' );
const tt_tim_Ast_Divide = tim.require( 'Ast/Divide.js' );
const tt_tim_Ast_DivideAssign = tim.require( 'Ast/DivideAssign.js' );
const tt_tim_Ast_Dot = tim.require( 'Ast/Dot.js' );
const tt_tim_Ast_Equals = tim.require( 'Ast/Equals.js' );
const tt_tim_Ast_Func_Self = tim.require( 'Ast/Func/Self.js' );
const tt_tim_Ast_Generator = tim.require( 'Ast/Generator.js' );
const tt_tim_Ast_GreaterOrEqual = tim.require( 'Ast/GreaterOrEqual.js' );
const tt_tim_Ast_GreaterThan = tim.require( 'Ast/GreaterThan.js' );
const tt_tim_Ast_Instanceof = tim.require( 'Ast/Instanceof.js' );
const tt_tim_Ast_LessOrEqual = tim.require( 'Ast/LessOrEqual.js' );
const tt_tim_Ast_LessThan = tim.require( 'Ast/LessThan.js' );
const tt_tim_Ast_Member = tim.require( 'Ast/Member.js' );
const tt_tim_Ast_Minus = tim.require( 'Ast/Minus.js' );
const tt_tim_Ast_MinusAssign = tim.require( 'Ast/MinusAssign.js' );
const tt_tim_Ast_Multiply = tim.require( 'Ast/Multiply.js' );
const tt_tim_Ast_MultiplyAssign = tim.require( 'Ast/MultiplyAssign.js' );
const tt_tim_Ast_Negate = tim.require( 'Ast/Negate.js' );
const tt_tim_Ast_New = tim.require( 'Ast/New.js' );
const tt_tim_Ast_Not = tim.require( 'Ast/Not.js' );
const tt_tim_Ast_Null = tim.require( 'Ast/Null.js' );
const tt_tim_Ast_NullishCoalescence = tim.require( 'Ast/NullishCoalescence.js' );
const tt_tim_Ast_Number = tim.require( 'Ast/Number.js' );
const tt_tim_Ast_ObjLiteral = tim.require( 'Ast/ObjLiteral.js' );
const tt_tim_Ast_Or = tim.require( 'Ast/Or.js' );
const tt_tim_Ast_Plus = tim.require( 'Ast/Plus.js' );
const tt_tim_Ast_PlusAssign = tim.require( 'Ast/PlusAssign.js' );
const tt_tim_Ast_PostDecrement = tim.require( 'Ast/PostDecrement.js' );
const tt_tim_Ast_PostIncrement = tim.require( 'Ast/PostIncrement.js' );
const tt_tim_Ast_PreDecrement = tim.require( 'Ast/PreDecrement.js' );
const tt_tim_Ast_PreIncrement = tim.require( 'Ast/PreIncrement.js' );
const tt_tim_Ast_Regex = tim.require( 'Ast/Regex.js' );
const tt_tim_Ast_Remainder = tim.require( 'Ast/Remainder.js' );
const tt_tim_Ast_RemainderAssign = tim.require( 'Ast/RemainderAssign.js' );
const tt_tim_Ast_String = tim.require( 'Ast/String.js' );
const tt_tim_Ast_Typeof = tim.require( 'Ast/Typeof.js' );
const tt_tim_Ast_Undefined = tim.require( 'Ast/Undefined.js' );
const tt_tim_Ast_Var = tim.require( 'Ast/Var.js' );
const tt_tim_Ast_Yield = tim.require( 'Ast/Yield.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_expr, v_member )
{
	this.__hash = hash;
	this.expr = v_expr;
	this.member = v_member;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 48382125;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Member.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_expr;
	let v_member;
	if( this !== Self ) { v_expr = this.expr; v_member = this.member; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'expr':
				if( arg !== pass ) { v_expr = arg; }
				break;
			case 'member':
				if( arg !== pass ) { v_member = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if(
/**/		v_expr.timtype !== tt_tim_Ast_And && v_expr.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_expr.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_expr.timtype !== tt_tim_Ast_Assign
/**/		&& v_expr.timtype !== tt_tim_Ast_Await
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_expr.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_Boolean
/**/		&& v_expr.timtype !== tt_tim_Ast_Call
/**/		&& v_expr.timtype !== tt_tim_Ast_Comma
/**/		&& v_expr.timtype !== tt_tim_Ast_Condition
/**/		&& v_expr.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_expr.timtype !== tt_tim_Ast_Delete
/**/		&& v_expr.timtype !== tt_tim_Ast_Differs
/**/		&& v_expr.timtype !== tt_tim_Ast_Divide
/**/		&& v_expr.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_Dot
/**/		&& v_expr.timtype !== tt_tim_Ast_Equals
/**/		&& v_expr.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_expr.timtype !== tt_tim_Ast_Generator
/**/		&& v_expr.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_expr.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_expr.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_expr.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_expr.timtype !== tt_tim_Ast_LessThan
/**/		&& v_expr.timtype !== tt_tim_Ast_Member
/**/		&& v_expr.timtype !== tt_tim_Ast_Minus
/**/		&& v_expr.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_Multiply
/**/		&& v_expr.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_Negate
/**/		&& v_expr.timtype !== tt_tim_Ast_New
/**/		&& v_expr.timtype !== tt_tim_Ast_Not
/**/		&& v_expr.timtype !== tt_tim_Ast_Null
/**/		&& v_expr.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_expr.timtype !== tt_tim_Ast_Number
/**/		&& v_expr.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_expr.timtype !== tt_tim_Ast_Or
/**/		&& v_expr.timtype !== tt_tim_Ast_Plus
/**/		&& v_expr.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_expr.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_expr.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_expr.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_expr.timtype !== tt_tim_Ast_Regex
/**/		&& v_expr.timtype !== tt_tim_Ast_Remainder
/**/		&& v_expr.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_expr.timtype !== tt_tim_Ast_String
/**/		&& v_expr.timtype !== tt_tim_Ast_Typeof
/**/		&& v_expr.timtype !== tt_tim_Ast_Undefined
/**/		&& v_expr.timtype !== tt_tim_Ast_Var
/**/		&& v_expr.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_member.timtype !== tt_tim_Ast_And && v_member.timtype !== tt_tim_Ast_ArrayLiteral
/**/		&& v_member.timtype !== tt_tim_Ast_ArrowFunction
/**/		&& v_member.timtype !== tt_tim_Ast_Assign
/**/		&& v_member.timtype !== tt_tim_Ast_Await
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseAnd
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseAndAssign
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseLeftShift
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseNot
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseOr
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseOrAssign
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseRightShift
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseUnsignedRightShift
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseXor
/**/		&& v_member.timtype !== tt_tim_Ast_BitwiseXorAssign
/**/		&& v_member.timtype !== tt_tim_Ast_Boolean
/**/		&& v_member.timtype !== tt_tim_Ast_Call
/**/		&& v_member.timtype !== tt_tim_Ast_Comma
/**/		&& v_member.timtype !== tt_tim_Ast_Condition
/**/		&& v_member.timtype !== tt_tim_Ast_ConditionalDot
/**/		&& v_member.timtype !== tt_tim_Ast_Delete
/**/		&& v_member.timtype !== tt_tim_Ast_Differs
/**/		&& v_member.timtype !== tt_tim_Ast_Divide
/**/		&& v_member.timtype !== tt_tim_Ast_DivideAssign
/**/		&& v_member.timtype !== tt_tim_Ast_Dot
/**/		&& v_member.timtype !== tt_tim_Ast_Equals
/**/		&& v_member.timtype !== tt_tim_Ast_Func_Self
/**/		&& v_member.timtype !== tt_tim_Ast_Generator
/**/		&& v_member.timtype !== tt_tim_Ast_GreaterOrEqual
/**/		&& v_member.timtype !== tt_tim_Ast_GreaterThan
/**/		&& v_member.timtype !== tt_tim_Ast_Instanceof
/**/		&& v_member.timtype !== tt_tim_Ast_LessOrEqual
/**/		&& v_member.timtype !== tt_tim_Ast_LessThan
/**/		&& v_member.timtype !== tt_tim_Ast_Member
/**/		&& v_member.timtype !== tt_tim_Ast_Minus
/**/		&& v_member.timtype !== tt_tim_Ast_MinusAssign
/**/		&& v_member.timtype !== tt_tim_Ast_Multiply
/**/		&& v_member.timtype !== tt_tim_Ast_MultiplyAssign
/**/		&& v_member.timtype !== tt_tim_Ast_Negate
/**/		&& v_member.timtype !== tt_tim_Ast_New
/**/		&& v_member.timtype !== tt_tim_Ast_Not
/**/		&& v_member.timtype !== tt_tim_Ast_Null
/**/		&& v_member.timtype !== tt_tim_Ast_NullishCoalescence
/**/		&& v_member.timtype !== tt_tim_Ast_Number
/**/		&& v_member.timtype !== tt_tim_Ast_ObjLiteral
/**/		&& v_member.timtype !== tt_tim_Ast_Or
/**/		&& v_member.timtype !== tt_tim_Ast_Plus
/**/		&& v_member.timtype !== tt_tim_Ast_PlusAssign
/**/		&& v_member.timtype !== tt_tim_Ast_PostDecrement
/**/		&& v_member.timtype !== tt_tim_Ast_PostIncrement
/**/		&& v_member.timtype !== tt_tim_Ast_PreDecrement
/**/		&& v_member.timtype !== tt_tim_Ast_PreIncrement
/**/		&& v_member.timtype !== tt_tim_Ast_Regex
/**/		&& v_member.timtype !== tt_tim_Ast_Remainder
/**/		&& v_member.timtype !== tt_tim_Ast_RemainderAssign
/**/		&& v_member.timtype !== tt_tim_Ast_String
/**/		&& v_member.timtype !== tt_tim_Ast_Typeof
/**/		&& v_member.timtype !== tt_tim_Ast_Undefined
/**/		&& v_member.timtype !== tt_tim_Ast_Var
/**/		&& v_member.timtype !== tt_tim_Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 48382125, v_expr, v_member );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if( v_expr === e.expr && v_member === e.member ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_expr, v_member );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
