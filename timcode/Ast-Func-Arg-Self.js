/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';


/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_comment, v_name, v_rest )
{
	this.__hash = hash;
	this.comment = v_comment;
	this.name = v_name;
	this.rest = v_rest;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -715194943;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Func/Arg/Self.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_comment;
	let v_name;
	let v_rest;
	if( this !== Self ) { v_comment = this.comment; v_name = this.name; v_rest = this.rest; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'comment':
				if( arg !== pass ) { v_comment = arg; }
				break;
			case 'name':
				if( arg !== pass ) { v_name = arg; }
				break;
			case 'rest':
				if( arg !== pass ) { v_rest = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_rest === undefined ) { v_rest = false; }
/**/if( CHECK )
/**/{
/**/	if( v_comment !== undefined && typeof( v_comment ) !== 'string' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_name !== undefined && typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_rest ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( -715194943, v_comment, v_name, v_rest );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if( v_comment === e.comment && v_name === e.name && v_rest === e.rest ) { return e; }
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_comment, v_name, v_rest );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
