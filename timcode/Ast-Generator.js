/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';

const tt_tim_Ast_Func_Arg_List = tim.require( 'Ast/Func/Arg/List.js' );
const tt_tim_Ast_Block = tim.require( 'Ast/Block.js' );

/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_args, v_body, v_isAsync, v_name )
{
	this.__hash = hash;
	this.args = v_args;
	this.body = v_body;
	this.isAsync = v_isAsync;
	this.name = v_name;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1641682363;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Ast/Generator.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_args;
	let v_body;
	let v_isAsync;
	let v_name;
	if( this !== Self )
	{
		v_args = this.args;
		v_body = this.body;
		v_isAsync = this.isAsync;
		v_name = this.name;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'args':
				if( arg !== pass ) { v_args = arg; }
				break;
			case 'body':
				if( arg !== pass ) { v_body = arg; }
				break;
			case 'isAsync':
				if( arg !== pass ) { v_isAsync = arg; }
				break;
			case 'name':
				if( arg !== pass ) { v_name = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_args === undefined ) { v_args = tt_tim_Ast_Func_Arg_List.Empty; }
	if( v_isAsync === undefined ) { v_isAsync = false; }
/**/if( CHECK )
/**/{
/**/	if( v_args.timtype !== tt_tim_Ast_Func_Arg_List ) { throw ( new Error( ) ); }
/**/	if( v_body !== undefined && v_body.timtype !== tt_tim_Ast_Block )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_isAsync ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_name !== undefined && typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( 1641682363, v_args, v_body, v_isAsync, v_name );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_args === e.args && v_body === e.body && v_isAsync === e.isAsync && v_name === e.name
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_args, v_body, v_isAsync, v_name );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
