/*
| This is an auto generated file.
| Editing this might be rather futile.
*/
'use strict';


/*
| The interning map.
*/
let imap = new Map( );

/*
| Collisions so far in imap.
*/
let icollisions = 0;

/*
| Remaps the interning map.
*/
const iremap =
	function( )
{
/**/if( CHECK ) { console.log( 'remap' ); }
	let omap = imap;
	icollisions = 0;
	imap = new Map( );
	for( let wr of omap.values( ) )
	{
		const e = wr.deref( );
		if( !e ) { continue; }
		let ihash = e.__hash;
		while( imap.get( ihash ) ) { ihash++; }
		imap.set( ihash, wr );
	}
};

/*
| Caching queue.
*/
let cSize = 1024;
let cArray = new Array( cSize );
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = Date.now( );

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( tim )
{
	cArray[ cPos ] = tim;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < 180000 ) { cPos = cSize; cSize *= 2; cArray.length = cSize; }
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_associativity, v_astCreator, v_handler, v_prec )
{
	this.__hash = hash;
	this.associativity = v_associativity;
	this.astCreator = v_astCreator;
	this.handler = v_handler;
	this.prec = v_prec;
	Object.freeze( this );
/**/if( CHECK ) { this._check( ); }
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = tim.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -468580758;

/*
| Reflection for debugging.
*/
/**/if( CHECK ) { Self.__DEBUG_PATH__ = prototype.__DEBUG_PATH__ = 'tim/Parser/Spec.js'; }

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_associativity;
	let v_astCreator;
	let v_handler;
	let v_prec;
	if( this !== Self )
	{
		v_associativity = this.associativity;
		v_astCreator = this.astCreator;
		v_handler = this.handler;
		v_prec = this.prec;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'associativity':
				if( arg !== pass ) { v_associativity = arg; }
				break;
			case 'astCreator':
				if( arg !== pass ) { v_astCreator = arg; }
				break;
			case 'handler':
				if( arg !== pass ) { v_handler = arg; }
				break;
			case 'prec':
				if( arg !== pass ) { v_prec = arg; }
				break;
			default :
				throw ( new Error( args[ a ] ) );
		}
	}
	if( v_associativity === undefined ) { v_associativity = 'n/a'; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_associativity ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_astCreator !== undefined && typeof( v_astCreator ) !== 'object' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_handler ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_prec !== undefined && ( typeof( v_prec ) !== 'number' || Number.isNaN( v_prec ) ) )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	if( icollisions > 1024 ) { iremap( ); }
	const hash = tim._hashArgs( -468580758, v_associativity, v_astCreator, v_handler, v_prec );
	let ihash = hash;
	let collisions = 0;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ++ihash ), collisions++ )
	{
		const e = wr.deref( );
		if( !e ) { break; }
		if(
			v_associativity === e.associativity && v_astCreator === e.astCreator
			&& v_handler === e.handler
			&& v_prec === e.prec
		)
		{
			return e;
		}
	}
	if( collisions ) { icollisions += collisions; }
	const newtim = new Constructor( hash, v_associativity, v_astCreator, v_handler, v_prec );
	imap.set( ihash, new WeakRef( newtim ) );
	cPut( newtim );
	return newtim;
};

/*
| Type reflection.
*/
prototype.timtype = Self;
